<?php

use app\models\TstTestUser;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */
/** @var TstTestUser $trainee_test */
/* @var \app\models\db\ext\TraineeUser $te_user */
/* @var \app\models\db\ext\TrainerUser $tr_user */

$lprTestLink = Yii::$app->urlManager->createAbsoluteUrl(Url::to(['/te/tests']));

?>

Hi <?= $te_user->profile->name ?>,

You've been assigned a new test:

<?= $trainee_test['test_title'] ?>

You can access it by going to this link:
<?= $lprTestLink ?>

This test has <?= $trainee_test['no_of_qs'] ?> questions.
Once started, you have <?= \app\ro\exigotech\lpr\GmUtils::formatTimeRange($trainee_test['test_duration']) ?> to finish it.

Best regards,
<?= $tr_user->profile->name ?>

