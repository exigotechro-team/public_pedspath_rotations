### Install Redis

```bash
suodo su - 
yum install -y redis32u.x86_64
```

### Start Redis server
```bash
/etc/init.d/redis restart
redis-cli
```

```bash
$ set attitude:today "happy"
$ get attitude:today
```

### Configure automatic start of Redis server
```bash
chkconfig --list redis
chkconfig --add  redis
chkconfig --level 345 redis on
chkconfig --list redis
```

### Install yiisoft/yii2-redis

```bash
cd /var/www/lpr_yii2_basic/
composer require --prefer-dist yiisoft/yii2-redis
```

### Configure redis component

```php
    'components' => [
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],

        'session' => [
            'class' => 'yii\redis\Session',
        ],

//        'cache' => ['class' => 'yii\caching\FileCache',],
        'cache' => [
            'class' => 'yii\redis\Cache',
        ],
    ]
```

###Test

```php
Yii::$app->redis->set('mykey', 'some value');
echo Yii::$app->redis->get('mykey');
```
