```bash
#!/usr/bin/env bash
yum remove -y \
    php7-pear.noarch                 \
    php70.x86_64                     \
    php70-bcmath.x86_64              \
    php70-cli.x86_64                 \
    php70-common.x86_64              \
    php70-devel.x86_64               \
    php70-gd.x86_64                  \
    php70-intl.x86_64                \
    php70-json.x86_64                \
    php70-mbstring.x86_64            \
    php70-mcrypt.x86_64              \
    php70-mysqlnd.x86_64             \
    php70-opcache.x86_64             \
    php70-pdo.x86_64                 \
    php70-pecl-apcu.x86_64           \
    php70-pecl-apcu-devel.x86_64     \
    php70-pecl-igbinary.x86_64       \
    php70-pecl-igbinary-devel.x86_64 \
    php70-pecl-imagick.x86_64        \
    php70-pecl-imagick-devel.x86_64  \
    php70-pecl-memcache.x86_64       \
    php70-pecl-memcached.x86_64      \
    php70-pecl-oauth.x86_64          \
    php70-pecl-ssh2.x86_64           \
    php70-pecl-uuid.x86_64           \
    php70-pecl-yaml.x86_64           \
    php70-pgsql.x86_64               \
    php70-process.x86_64             \
    php70-pspell.x86_64              \
    php70-soap.x86_64                \
    php70-tidy.x86_64                \
    php70-xml.x86_64                 \
    php70-xmlrpc.x86_64              \
    php70-zip.x86_64 
```

```bash
yum install -y \
    php71w-pear.noarch                 \
    php71w.x86_64                     \
    php71w-bcmath.x86_64              \
    php71w-cli.x86_64                 \
    php71w-common.x86_64              \
    php71w-devel.x86_64               \
    php71w-gd.x86_64                  \
    php71w-intl.x86_64                \
    php71w-json.x86_64                \
    php71w-mbstring.x86_64            \
    php71w-mcrypt.x86_64              \
    php71w-mysqlnd.x86_64             \
    php71w-opcache.x86_64             \
    php71w-pdo.x86_64                 \
    php71w-pecl-apcu.x86_64           \
    php71w-pecl-apcu-devel.x86_64     \
    php71w-pecl-igbinary.x86_64       \
    php71w-pecl-igbinary-devel.x86_64 \
    php71w-pecl-imagick.x86_64        \
    php71w-pecl-imagick-devel.x86_64  \
    php71w-pecl-memcache.x86_64       \
    php71w-pecl-memcached.x86_64      \
    php71w-pecl-oauth.x86_64          \
    php71w-pecl-ssh2.x86_64           \
    php71w-pecl-uuid.x86_64           \
    php71w-pecl-yaml.x86_64           \
    php71w-pgsql.x86_64               \
    php71w-process.x86_64             \
    php71w-pspell.x86_64              \
    php71w-soap.x86_64                \
    php71w-tidy.x86_64                \
    php71w-xml.x86_64                 \
    php71w-xmlrpc.x86_64              \
    php71w-zip.x86_64 
```



