### Install node at system level 

```bash
cd /home/aarwa
curl --silent --location https://rpm.nodesource.com/setup_6.x | sudo bash -

sudo yum install -y nodejs yarn 

```



###Preparations – install nvm

Read from [creationix/nvm](https://github.com/creationix/nvm) to get the latest version number for nvm

```bash

cd /home/aarwa

# using curl
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash
# or using wget
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash


source ~/.nvm/nvm.sh

```

Install node:

```bash
nvm install node latest 
nvm install iojs latest 

```


### Read the tutorial at:

```bash
npm install -g bower
```


[bower/yii2-pjax](https://libraries.io/bower/yii2-pjax)

```bash
cd /var/www/lpr_yii2_basic
bower init
bower install yii2-pjax --save
bower install jquery-pjax  --save

```


