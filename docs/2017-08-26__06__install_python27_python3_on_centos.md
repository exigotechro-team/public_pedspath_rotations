### Read the tutorial at:

[how-to-install-latest-python-on-centos/](https://danieleriksson.net/2017/02/08/how-to-install-latest-python-on-centos/)

###Preparations – install prerequisites

```bash
# Start by making sure your system is up-to-date:
yum update
# Compilers and related tools:
yum groupinstall -y "development tools"
# Libraries needed during compilation to enable all features of Python:
yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel
# If you are on a clean "minimal" install of CentOS you also need the wget tool:
yum install -y wget
```

###Download, compile and install Python

```bash
# Python 2.7.13:
wget http://python.org/ftp/python/2.7.13/Python-2.7.13.tar.xz
tar xf Python-2.7.13.tar.xz
cd Python-2.7.13
./configure --prefix=/usr/local --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
make && make altinstall

# Python 3.6.2:
wget http://python.org/ftp/python/3.6.2/Python-3.6.2.tar.xz
tar xf Python-3.6.2.tar.xz
cd Python-3.6.2
./configure --prefix=/usr/local --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
make && make altinstall
```

You might also want to strip symbols from the shared library to reduce the memory footprint.

```bash
# Strip the Python 2.7 binary:
strip /usr/local/lib/libpython2.7.so.1.0
# Strip the Python 3.6 binary:
strip /usr/local/lib/libpython3.6m.so.1.0
```


###Install/upgrade pip, setuptools and wheel

```bash
# First get the script:
wget https://bootstrap.pypa.io/get-pip.py

# Then execute it using Python 2.7 and/or Python 3.6:
python2.7 get-pip.py
python3.6 get-pip.py

# With pip installed you can now do things like this:
pip2.7 install [packagename]
pip2.7 install --upgrade [packagename]
pip2.7 uninstall [packagename]
```

###What’s next?

```bash
pip2.7 install virtualenv

```

###Create your first isolated Python environment

```bash
# Install virtualenv for Python 2.7 and create a sandbox called my27project:
pip2.7 install virtualenv
virtualenv my27_aws_shell

# Use the built-in functionality in Python 3.6 to create a sandbox called my36project:
python3.6 -m venv my36_aws_shell

# Check the system Python interpreter version:
python --version
# This will show Python 2.6.6

# Activate the my27project sandbox:
source my27_aws_shell/bin/activate
# Check the Python version in the sandbox (it should be Python 2.7.13):
python --version

pip install boto aws aws-shell

# Deactivate the sandbox:
deactivate

# Activate the my36project sandbox:
source my36_aws_shell/bin/activate
# Check the Python version in the sandbox (it should be Python 3.6.2):
python --version

pip install boto3 aws aws-shell chalice

# Deactivate the sandbox:
deactivate
```
