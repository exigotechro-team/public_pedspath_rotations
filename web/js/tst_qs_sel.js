
var questions_selected_ids = [];

var tag_l1 = "_";
var tag_l2 = ":";


$(function(){
    //console.log(">>>>> tst_qs_sel.js loaded");
});


function markAsCheckedTheInitialSelectedQuestions()
{
    selQsLen = questions_selected_ids.length;

    for(var i=0; i<selQsLen; i++)
    {
        qid = questions_selected_ids[i];
        $('div#w0 * input[type=checkbox][class=simple][value="' + qid + '"]').prop('checked', true);
    }

    setStartEndSelQuestionsHiddenInputVals();

    $('div#w0 * input[type=checkbox][class=simple]').click(handleClickOnQuestionCheckbox);
}


function handleClickOnQuestionCheckbox()
{
    setEndSelQuestionsHiddenInputVal();

    //var thatParent = $(this).parent();
    //var isChecked = $(this).is(':checked');
    //console.log("click in checkbox ["+ $(this).attr('value') +"]: " + isChecked);
}

function setStartEndSelQuestionsHiddenInputVals()
{
    var sel_ids_dom = $('div#w0 * input[type=checkbox][class=simple]');
    var sel_ids_len = sel_ids_dom.length;
    var sel_ids = [];
    var sel_ids_str = '';

    for(var j =0; j<sel_ids_len; j++)
    {
        var dObj = $(sel_ids_dom[j]);
        if(dObj.is(':checked'))
        {
            var dObjVal = dObj.attr('value');
            sel_ids.push(dObjVal);
            sel_ids_str = sel_ids_str + ( sel_ids.length==1 ? '' : tag_l2 )+ dObjVal;
        }
    }

    $('input#selectquestionform-sel_ids_start').val(htmlEncode(sel_ids_str));
    $('input#selectquestionform-sel_ids_end').val(htmlEncode(sel_ids_str));

    //console.log(sel_ids);
}

function setEndSelQuestionsHiddenInputVal()
{
    var sel_ids = $('div#w0').yiiGridView('getSelectedRows');
    console.log("sel array ["+ sel_ids +"]");

    var sel_ids_len = sel_ids.length;
    var sel_ids_str = '';

    for(var j =0; j<sel_ids_len; j++)
    {
        var qid = sel_ids[j];
        sel_ids_str = sel_ids_str + ( j==0 ? '' : tag_l2 )+ qid;
    }

    $('input#selectquestionform-sel_ids_end').val(htmlEncode(sel_ids_str));
}


function htmlEncode(value){
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

function htmlDecode(value){
    return $.trim( $('<div/>').html(value).text() );
}
