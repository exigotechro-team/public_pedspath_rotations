$(function(){
   // console.log('-OK!-');
   $('#modalButton').click(function(){
       $('#modal').modal('show')
           .find('#modalContent')
           .load($(this).attr('value'));
       return false;
   });

    $('button[id^="modalEditButton_"]').click(function () {
        // console.log('OK2: ' + $(this).attr('value'));
        $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
        // console.log('OK2: loaded');
        return false;
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        // console.log('ok-add');
        if( $('#block').is(':checked') ) {
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        }
        $(this).removeData('bs.modal');
        //$(this).find('#modalContent').html('');
    });

    //$('#modalEditAnswer').on('hidden.bs.modal', function (e) {
    //    console.log('ok-edit');
    //    $(this).removeData('bs.modal');
    //    window.location.replace('/index.php?r=gm-admin');
    //    //$(this).find('#modalContent').html('');
    //});

});