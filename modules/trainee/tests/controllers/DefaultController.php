<?php

namespace app\modules\trainee\tests\controllers;

use app\models\db\ext\Organization;
use app\models\db\search\TstTestUserSearch;
use app\models\TstTestUser;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Default controller for the `tests` module
 */
class DefaultController extends Controller
{

    /** @var Organization $org */
    public $org;

    public $trainee_id;


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/modules/trainee/views/layouts/column2';
        $user_id = \Yii::$app->user->id;

        $this->view->params['user_id'] = $user_id;
        $this->view->params['action_tag'] = 2;

        $tuSearch = new TstTestUserSearch();
        $dataProvider = $tuSearch->searchAssignedTestsToTrainee($this->trainee_id);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->trainee_id = \Yii::$app->user->id;

        $org = Organization::GetOrgForTrainee(\Yii::$app->user->id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a trainee!");
        }

        return parent::beforeAction($action);
    }



}
