<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Assigned tests';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tests-default-index">
    <h2><?= $this->title ?></h2>
    <div class="row" style="margin-top: 1.5em;margin-bottom: 1em;">
        <div class="col-lg-9 col-md-9">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <?php
    try {
        echo \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list_item',
            'options' => [
                'tag' => 'div',
                'class' => 'list-wrapper',
                'id' => 'list-wrapper',
            ],
            'layout' => "{pager}\n{items}\n{summary}",
        ]);
    }
    catch (Exception $e) {
        echo sprintf("<pre>%s</pre>", $e->getMessage());
        if (true) {
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
        }
    }

    ?>

    <div class="row">

    </div>

</div>
