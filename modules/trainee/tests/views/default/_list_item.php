<?php

/* @var $model \app\models\TstTestUser */

use app\models\attrs\ExamStartStatus;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var int $examStartStatus */
$examStartStatus = $model->getExamCanBeginStatus();

$testTimeLeft = $model->getTestTimeRemaining();
$isTestCompleted = (strcmp(strtolower( $testTimeLeft), strtolower( "Test Completed")) === 0) ? true : false;

?>

<div class="row" style="margin-bottom: 1em;border-right: thin double #e4eef3;">
    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6" style="border-left: thin double #ff0000;margin-left: 1em;margin-bottom: 1em;">
        <span style="font-size: 14pt;font-weight: bold"><?= $model->test_title ?></span>
        <div style="margin-top: 10px;">
            <?php
            if ($model->isBeforeExamStartTime() && $model->hasStartedOn()) { ?>
                You can start this exam on: <b><?= $model->getStartDate() ?> </b><br/>
            <?php } ?>

            <p>Time to complete: <span class="label label-<?= ($isTestCompleted) ? 'success' : 'default'  ?>"><?= $testTimeLeft ?></span></p>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5" style="margin-left: 1em;margin-bottom: 1em;">

        <div style="width:120px">
            <?php
            if ($examStartStatus == ExamStartStatus::START_NOT_SET
                || $examStartStatus == ExamStartStatus::PRIOR_TO_START_TIME) {

                if (!($model->isBeforeExamStartTime() && $model->hasStartedOn())) {
                    echo Html::a('<span class="glyphicon glyphicon-blackboard"></span> Start test',
                        Url::to(['exam/intro', 'utid' => $model->id]),
                        [
                            'title' => 'Start test',
                            'class' => 'btn btn-xs btn-info btn-block',
                            'style' => '',
                            'data' => ['method' => 'POST',],
                        ]);
                }

            } elseif ($examStartStatus == ExamStartStatus::DURING_START_END_TIME) {

                echo Html::a('<span class="glyphicon glyphicon-time"></span> Continue test',
                    Url::to(['exam/continue', 'utid' => $model->id]),
                    [
                        'title' => 'Continue test',
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'style' => '',
                        'data' => ['method' => 'POST',],
                    ]);
            } elseif ($examStartStatus == ExamStartStatus::POST_END_TIME) {

                echo Html::a('<span class="glyphicon glyphicon-blackboard"></span> Review test',
                    Url::to(['exam/review', 'utid' => $model->id]),
                    [
                        'title' => 'Review test',
                        'class' => 'btn btn-xs btn-primary btn-block',
                        'style' => '',
                        'data' => ['method' => 'POST',],
                    ]);
            }
            ?>
        </div>
    </div>
</div>