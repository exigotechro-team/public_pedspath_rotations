<?php
/* @var $this yii\web\View */
/* @var $utid integer */
/* @var $model \app\models\TstTestUser */
/* @var $lpr_test \app\models\db\ext\LprTest */
/* @var $user_consent \app\models\db\TstUserConsent */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = ['label' => 'Assigned tests', 'url' => ['/te/tests']];
$this->title = 'Exam Intro';
$this->params['breadcrumbs'][] = $this->title;

/** @var int $examStartStatus */
$examStartStatus = $model->getExamCanBeginStatus();

$duration = intval($lpr_test->duration_sec);
$formatDuration = \app\ro\exigotech\lpr\GmUtils::formatTimeRange($duration);

$isAfirmativeConsent = false;

if(!empty($user_consent) && $user_consent->consent){
    $isAfirmativeConsent = true;
}

?>

<div class="row" style="margin-top: 0em;">
    <div class="panel panel-info">
        <div class="panel-body">
            <div class="col-sm-offset-2 col-sm-8 ">
                <?= $this->render('_notification') ?>
            </div>

            <?php $form = ActiveForm::begin([
                'action' => Url::to(['exam/start', 'utid' => $utid]),
                'method' => 'post',
            ]); ?>

            <div class="col-sm-offset-2 col-sm-8 ">
                <h4 style="margin-bottom: 1em;margin-top: 2em;">You are about to begin your test:</h4>
                <h3 style="background-color: #f8eec9;padding: 8px;">
                    <?= $model->test_title ?>
                </h3>

                <p style="margin-top: 2em;">
                    <?php
                    if ($model->isBeforeExamStartTime() && $model->hasStartedOn()) { ?>
                        You can start this exam on: <b><?= $model->getStartDate() ?> </b><br/>
                    <?php } ?>
                    Time available for test: <b><?= $formatDuration ?></b>.
                </p>

                <?php if (! $isAfirmativeConsent) { ?>
                <div class="panel panel-default pull-left" style="width: 100%;">
                    <div class="panel-body">
                        <p style="text-align: center;" class="pull-right">
                            <?= Html::checkbox('consent', true) ?>
                        </p>
                        <p>I agree to have the anonymized/de-identified test results included in educational research projects
                            conducted by the Department of Pathology at Lurie Children’s Hospital.
                        </p>

                    </div>
                </div>
                <?php } ?>
            </div>

            <div class="col-sm-offset-2 col-sm-8 ">
                <div style="margin-top: 1em;">
                    <?php

                    echo Html::a('<span class="glyphicon glyphicon-stop"></span> CANCEL',
                        Url::to(['/te/tests']),
                        [
                            'title' => 'Begin',
                            'class' => 'btn btn-xs btn-danger',
                            'style' => 'width:120px;',
                            'data' => ['method' => 'POST',],
                        ]);

                    ?>

                    &nbsp;&nbsp;&nbsp;

                    <?php

                    if (!($model->isBeforeExamStartTime() && $model->hasStartedOn())) {
                        echo Html::submitButton('<span class="glyphicon glyphicon-blackboard"></span> BEGIN',
                            [
                                'title' => 'Begin',
                                'class' => 'btn btn-xs btn-info',
                                'style' => 'width:120px;',
                                'data' => [/*'method' => 'POST',*/],
                                'name' => 'sbmtBtn',
                                'id' => 'sbmtBtnStart',
                                'value' => 'sbmtBtnStart',
                            ]);
                    }

                    ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
