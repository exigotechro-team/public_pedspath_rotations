<?php

namespace app\modules\trainee;
use app\models\rbac\roles\RbacRole;
use yii\base\ExitException;
use yii\base\InvalidRouteException;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;

/**
 * te module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\trainee\controllers';

    /**
     * @inheritdoc
     * @throws ForbiddenHttpException
     */
    public function init()
    {
        parent::init();

        if (\Yii::$app->user->isGuest) {
            $response = \Yii::$app->getResponse();
            $response->data = "";
            $response->redirect(\Yii::$app->urlManager->createUrl("/user/login"))->send();

            try {
                \Yii::$app->end();
            } catch (ExitException $e) {
            }
        }

        if (!empty(\Yii::$app->user) && !\Yii::$app->user->can(RbacRole::TRAINEE)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        // custom initialization code goes here
        $this->modules = [
            'tests' => [
                'class' => 'app\modules\trainee\tests\Module',
            ],
        ];

    }
}
