<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$org_title = "";

/** @var \app\models\db\ext\Organization $org */
$org = null;

if(isset($this->params['organization']))
{
    $org = $this->params['organization'];
    $org_title .= $org->short_label;
}

$this->title = 'Trainee Home Page';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="te-default-index">
    <h3 style="margin-bottom: 1em;"><?= $this->title ?></h3>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <ul>
                        <li><a href="/te/tests">Assigned Tests</a></li>
                        <li><a href="/te/consent">Consent Form</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <ul>
                        <li><a href="<?= Url::to(['/user/settings/profile']) ?>">Profile</a></li>
                        <li><a href="<?= Url::to(['/user/settings/account']) ?>">Account</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
