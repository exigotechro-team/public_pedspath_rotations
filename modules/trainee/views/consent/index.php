<?php

/* @var $this yii\web\View */
/* @var $user_consent \app\models\db\TstUserConsent */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;

$org_title = "";

/** @var \app\models\db\ext\Organization $org */
$org = null;

if(isset($this->params['organization']))
{
    $org = $this->params['organization'];
    $org_title .= $org->short_label;
}

$this->title = 'Consent Form';
$this->params['breadcrumbs'][] = $this->title;


$isAfirmativeConsent = false;

if(!empty($user_consent) && $user_consent->consent){
    $isAfirmativeConsent = true;
}

if (!true) {
    print "\n<pre>" . VarDumper::dumpAsString([
            'user_consent' => $user_consent->toArray(),
            'isAfirmativeConsent' => $isAfirmativeConsent,
        ]) . "</pre>\n";
    exit;
}

?>

<div class="te-default-index">
    <h2 style="margin-bottom: 1em;"><?= $this->title ?></h2>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div style="padding: 1em;">
            <div class="panel panel-default pull-left" style="width: 100%;">
                <div class="panel-body">
                    <p style="text-align: center;margin:1pt;padding:5pt;background-color: #9acfea;" class="pull-right">
                        <?= Html::checkbox('consent', ($isAfirmativeConsent) ? true : false,
                            [
                                'disabled' => ($isAfirmativeConsent) ? true : false,
                            ])
                        ?>
                    </p>
                    <p>I agree to have the anonymized/de-identified test results included in educational research
                        projects conducted by the Department of Pathology at Lurie Children’s Hospital.
                    </p>
                </div>
            </div>
            <?php

            if(!$isAfirmativeConsent){
                echo Html::submitButton('<span class="glyphicon glyphicon-floppy-save"></span> Save',
                    [
                        'title' => 'Begin',
                        'class' => 'btn btn-xs btn-info',
                        'style' => 'width:120px;',
                        'data' => [/*'method' => 'POST',*/],
                        'name' => 'sbmtBtn',
                        'id' => 'sbmtBtnSave',
                        'value' => 'sbmtBtnSave',
                    ]);
            }
            ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
