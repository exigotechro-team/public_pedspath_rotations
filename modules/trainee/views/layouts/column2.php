<?php use yii\bootstrap\Nav;

/* @var $content string */

use yii\helpers\Url;



$user_id = $this->params['user_id'];
$tag = intval($this->params['action_tag']);

$this->beginContent('@app/views/layouts/main.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <?= $this->render('_notification') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3" style="margin-top:1em;">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul id="w3" class="nav-pills nav-stacked nav">
                            <li <?= ($tag == 1) ? "class=\"active\"" : "" ?>>
                                <a href="/te/">Home</a>
                            </li>
                            <li <?= ($tag == 2) ? "class=\"active\"" : "" ?>>
                                <a href="/te/tests">Assigned Tests</a>
                            </li>
                            <li <?= ($tag == 3) ? "class=\"active\"" : "" ?>>
                                <a href="/te/consent">Consent Form</a>
                            </li>
                            <hr/>
                            <li><a class="text-danger" href="/user/settings/profile">Profile</a></li>
                            <li><a class="text-danger" href="/user/settings/account">Account</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="content" class="col-sm-8" style=";margin-top:1em;">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div><!-- content -->
        </div>
    </div>
<?php $this->endContent(); ?>