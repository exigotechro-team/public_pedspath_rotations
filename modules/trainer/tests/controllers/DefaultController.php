<?php

namespace app\modules\trainer\tests\controllers;

use app\models\db\ext\Organization;
use app\models\db\search\LedgerSearch;
use app\models\db\search\TraineeUserSearch;
use app\models\db\search\TstTestUserSearch;
use app\models\TstTestUser;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Default controller for the `tests` module
 */
class DefaultController extends Controller
{
    /** @var Organization $org */
    public $org;

    public $trainer_id;



    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        /**
         * $this->trainer_id
         * get all tests for trainees that belong to this trainer
         */

        $tuSearch = new TstTestUserSearch();
        $dataProvider = $tuSearch->searchAssignedTestsByTrainer($this->trainer_id);
        $trainees_tests = $tuSearch->getAssignedTestsByTrainerQuery($this->trainer_id)->all();

        $trainees = [];
        $assigned_tests = [];

        foreach ($trainees_tests as $trainee_tests){
            /** @var \app\models\TstTestUser $trainee_tests */
            $trainees[$trainee_tests->user_id] = $trainee_tests->user_name;

            if(!array_key_exists($trainee_tests->user_id, $assigned_tests)){
                $assigned_tests[$trainee_tests->user_id] = [];
            }
            $assigned_tests[$trainee_tests->user_id][$trainee_tests->id] = $trainee_tests;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'trainees_tests' => $trainees_tests,
            'trainees' => $trainees,
            'assigned_tests' => $assigned_tests,
        ]);
    }


    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->trainer_id = \Yii::$app->user->id;

        $org = Organization::GetOrgForTrainer($this->trainer_id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a trainer!");
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $tid
     * @throws Exception
     */
    private function validateTestAssignedToOrg($tid)
    {
        $searchModel = new LedgerSearch();
        if(!$searchModel->checkTestIsAssignedToOrg($tid, $this->org->id)){
            throw new Exception(sprintf("Test %s is not assigned to this organization %s", $tid, $this->org->id));
        };
    }

    /**
     * @param $uid
     * @throws Exception
     */
    private function validateTrainee($uid)
    {
        $traineeSearch = new TraineeUserSearch();
        if(!$traineeSearch->checkTraineeExistsForTrainer($uid, $this->trainer_id)){
            throw new Exception(sprintf("Trainee %s is not assigned to this trainer %s", $uid, $this->trainer_id));
        };
    }




}
