<?php

namespace app\modules\trainer\tests\controllers;

use app\models\attrs\ExamStartStatus;
use app\models\db\ext\LprQuestion;
use app\models\db\ext\LprTest;
use app\models\db\ext\Organization;
use app\models\db\ext\TstAnswer;
use app\models\db\search\TstTestQuestionOrder;
use app\models\db\search\TstTestUserSearch;
use app\models\form\TestTakeQuestionForm;
use app\models\LprQMediaContent;
use app\models\search\LprQuestionSearch;
use app\models\TstTestUser;
use app\models\TstUserAnswer;
use app\ro\exigotech\lpr\GmUtils;
use DusanKasan\Knapsack\Collection;
use yii\base\Exception;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;

class ExamController extends \yii\web\Controller
{
    /** @var Organization $org */
    public $org;

    public $trainee_id;

    public $trainer_id;

    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->trainer_id = \Yii::$app->user->id;

        $org = Organization::GetOrgForTrainer($this->trainer_id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a trainer!");
        }

        return parent::beforeAction($action);
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param int $utid
     * @param int $te_uid
     * @return string
     */
    public function actionReview($utid, $te_uid)
    {
        $this->trainee_id = $te_uid;
        $this->trainer_id = \Yii::$app->user->id;

        /** @var TstTestUserSearch $tuSearch */
        $tuSearch = new TstTestUserSearch();

        /** @var TstTestUser $ttu_trainee */
        $ttu_trainee = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($ttu_trainee)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        /** @var int $examStartStatus */
        $examStartStatus = $ttu_trainee->getExamCanBeginStatus();

        // if test is NOT DURING set time, then redirect
        if (false && $examStartStatus != ExamStartStatus::POST_END_TIME)
        {
            $msg = "You are not allowed to take the test at this moment!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]));
        }

        /** @var LprTest $lprTest */
        $lprTest = LprTest::find()->where(['id' => $ttu_trainee->test_id])->one();

        if(empty($lprTest)){
            return $this->redirectWithAlertOperation(
                sprintf("No test was found w. id: %s!", $ttu_trainee->test_id),
                'alert-danger',
                Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]));
        }

        /** @var LprQuestionSearch $searchModel */
        $searchModel = new LprQuestionSearch();

        $totalCount = $searchModel->getTestQuestionsCount($ttu_trainee->test_id);

        /** @var ArrayDataProvider $testStatsDataProvider */
        $testStatsDataProvider = $tuSearch->getStatsForUserTest($utid);

        $data = $testStatsDataProvider->allModels;

        $q_total = Collection::from($data)->reduce(
            function ($tmp, $val) {
                $tmp += $val['no_of_q'];
                return $tmp; }, 0);

        $q_correct_total = Collection::from($data)->reduce(
            function ($tmp, $val) {
                $tmp += $val['correct'];
                return $tmp; }, 0);

        $q_correct_perc = (intval($q_total) > 0) ? (intval($q_correct_total) / intval($q_total)) * 100 : 0;
        $q_correct_perc = sprintf("%.01f%%", $q_correct_perc);

        $total = [[
            'category' => 'Total',
            'total' => $q_total,
            'correct' => $q_correct_total,
            'correct_percentage' => $q_correct_perc,
        ]];

        $totalDataProvider = new ArrayDataProvider(['allModels' => $total,]);

        return $this->render('review',[
            'lprTestModel'  => $lprTest,
            'te_uid' => $te_uid,
            'totalCount'    => $totalCount,
            'testStatsDataProvider' => $testStatsDataProvider,
            'totalDataProvider' => $totalDataProvider,
        ]);
    }


    /**
     * @param int $utid
     * @param int $te_uid
     * @return string|\yii\web\Response
     */
    public function actionIntro($utid, $te_uid)
    {
        $this->trainee_id = $te_uid;

        $tuSearch = new TstTestUserSearch();
        /** @var TstTestUser $trainee_test */
        $trainee_test = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($trainee_test)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]));
        }

        /** @var int $examStartStatus */
        $examStartStatus = $trainee_test->getExamCanBeginStatus();

        // if test is started or finished, then redirect
        if ($examStartStatus != ExamStartStatus::START_NOT_SET
            && $examStartStatus != ExamStartStatus::PRIOR_TO_START_TIME)
        {
            $msg = "Test is already started or finished!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]));
        }

        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $trainee_test->test_id]);

        return $this->render('intro', [
            'model' => $trainee_test,
            'utid' => $utid,
            'te_uid' => $te_uid,
            'lpr_test' => $lprTest ]);
    }

    /**
     * @param int $utid
     * @param int $te_uid
     * @return string|\yii\web\Response
     */
    public function actionStart($utid, $te_uid)
    {
        $this->trainee_id = $te_uid;

        $tuSearch = new TstTestUserSearch();
        /** @var TstTestUser $trainee_test */
        $trainee_test = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($trainee_test)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        /** @var int $examStartStatus */
        $examStartStatus = $trainee_test->getExamCanBeginStatus();

        // if test is started or finished, then redirect
        if ($examStartStatus != ExamStartStatus::START_NOT_SET
            && $examStartStatus != ExamStartStatus::PRIOR_TO_START_TIME)
        {
            $msg = "Test is already started or finished!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        if ($examStartStatus == ExamStartStatus::START_NOT_SET) {
            $trainee_test->configureStartAndEndExamTimes();
        }

//        return $this->redirect(['exam/take', 'utid' => $utid, 'q' => 1 ]);
        return $this->redirect(['exam/take', 'utid' => $utid, 'te_uid' => $te_uid]);
    }

    /**
     * @param int $utid
     * @param int $te_uid
     * @param int $rv review mode
     * @return string|\yii\web\Response
     */
    public function actionContinue($utid, $te_uid, $rv = 0)
    {
        $this->trainee_id = $te_uid;

        $tuSearch = new TstTestUserSearch();
        /** @var TstTestUser $trainee_test */
        $trainee_test = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($trainee_test)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        /** @var int $examStartStatus */
        $examStartStatus = $trainee_test->getExamCanBeginStatus();

        // if test is NOT DURING set time, then redirect
        if ($examStartStatus != ExamStartStatus::DURING_START_END_TIME)
        {
            if (!true) {
                $msg = "You are not allowed to take the test at this moment!";
                return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
            }
        }

        return $this->redirect(['exam/take', 'utid' => $utid, 'te_uid' => $te_uid, 'rv' => $rv]);
    }


    /**
     * @param int $utid LprTest ID
     * @param int $te_uid
     * @param int $rv review mode
     * @return \yii\web\Response|string
     */
    public function actionTake($utid, $te_uid, $rv=0)
    {
        $this->trainee_id = $te_uid;
        $rv = intval($rv);
        $isReviewMode = ($rv == 1) ? true : false;

        /** @var TstTestUserSearch $tuSearch */
        $tuSearch = new TstTestUserSearch();

        /** @var TstTestUser $ttu_trainee */
        $ttu_trainee = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($ttu_trainee)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        /** @var int $examStartStatus */
        $examStartStatus = $ttu_trainee->getExamCanBeginStatus();

        // if test is NOT DURING set time, then redirect
        if ($examStartStatus != ExamStartStatus::DURING_START_END_TIME)
        {
            if (!true) {
                $msg = "You are not allowed to take the test at this moment!";
                return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]));
            }
        }

        /** @var LprTest $lprTest */
        $lprTest = LprTest::find()->where(['id' => $ttu_trainee->test_id])->one();

        if(empty($lprTest)){
            return $this->redirectWithAlertOperation(
                sprintf("No test was found w. id: %s!", $ttu_trainee->test_id),
                'alert-danger',
                Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]));
        }

        $post = \Yii::$app->request->post();

        $isPostRequest = (!empty($post)) ? true : false;

        if (!$isPostRequest) {
            // handle start/continue exam
            return $this->takeHandleStartTest($ttu_trainee, $lprTest, $rv);
        }
        else
        {
            /** @var TestTakeQuestionForm $currFormModel */
            $curr_FormModel = new TestTakeQuestionForm();

            if (!($curr_FormModel->load($post) && $curr_FormModel->validate())) {
                return $this->redirectWithAlertOperation("Invalid test data!",
                    "alert-danger",
                    ['take', 'utid' => $utid, 'te_uid' => $te_uid]);
            }

            if(!$curr_FormModel->verifyControlHash()){
                return $this->redirectWithAlertOperation("Invalid test - question association!",
                    "alert-danger",
                    ['take', 'utid' => $utid, 'te_uid' => $te_uid]);
            }

            $submitBtnVal = array_key_exists('submitBtn', $post) ?  $post['submitBtn'] : '';

            if ($submitBtnVal == 'exit') {
                return $this->takeHandleSaveExit($ttu_trainee, $lprTest, $curr_FormModel); }

            if ($submitBtnVal == 'jump') {

                $pos_qid = (array_key_exists('jump_to_q', $post)) ? intval($post['jump_to_q']) : 0;

                return $this->takeHandleJumpToQuestion($ttu_trainee, $lprTest, $pos_qid, $rv);
            }

            if (in_array($submitBtnVal, ['next', 'prev'])) {
                $navDir = ($submitBtnVal == 'prev') ? -1 : 1;
                $curr_FormModel->isReviewMode = $isReviewMode;
                return $this->takeHandleNextPrevQuestion($ttu_trainee, $lprTest, $curr_FormModel, $navDir);
            }
        }

        $msg = "There was an error processing your submission!";
        return $this->redirectWithAlertOperation($msg, 'alert-danger', Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
    }


    /**
     * @param int $utid
     * @param int $te_uid
     * @return string|\yii\web\Response
     */
    public function actionReset($utid, $te_uid)
    {
        $this->trainee_id = $te_uid;
        $this->trainer_id = \Yii::$app->user->id;

        /** @var TstTestUserSearch $tuSearch */
        $tuSearch = new TstTestUserSearch();

        /** @var TstTestUser $ttu_trainee */
        $ttu_trainee = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($ttu_trainee)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger',
                Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        /** @var int $examStartStatus */
        $examStartStatus = $ttu_trainee->getExamCanBeginStatus();

        // if test is NOT DURING or POST set time, then redirect
        if (!($examStartStatus == ExamStartStatus::DURING_START_END_TIME
            || $examStartStatus == ExamStartStatus::POST_END_TIME))
        {
            $msg = "Test cannot be reset before start!";
            return $this->redirectWithAlertOperation($msg,'alert-danger',
                Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }

        if (true) {
            $ttu_trainee->resetTestForTrainee();
        }

        if (!true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'utid' => $utid,
                    'te_uid' => $te_uid,
                    'ttu_trainee' => $ttu_trainee->toArray(),
                    'examStartStatus' => $examStartStatus,
                ]) . "</pre>\n";
            exit;
        }

        return $this->redirect(Url::to(['/tr/trainees/tests', 'id'=>$te_uid]));
    }

    /**
     * @param int $utid
     * @param int $te_uid
     * @return string|\yii\web\Response
     */
    public function actionExtend($utid, $te_uid)
    {
        $this->trainee_id = $te_uid;
        $this->trainer_id = \Yii::$app->user->id;

        /** @var TstTestUserSearch $tuSearch */
        $tuSearch = new TstTestUserSearch();

        /** @var TstTestUser $ttu_trainee */
        $ttu_trainee = $tuSearch->getAssignedTestsToTraineeQuery($this->trainee_id, $utid)->one();

        if (empty($ttu_trainee)) {
            $msg = "No such test associated with your account!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger',
                Url::to(['/tr/trainees/tests', 'id' => $this->trainee_id]) );
        }


        /** @var int $examStartStatus */
        $examStartStatus = $ttu_trainee->getExamCanBeginStatus();

        if ($examStartStatus == ExamStartStatus::POST_END_TIME && !$ttu_trainee->is_test_completed) {
            $curr_time = time() + 24*60*60;
            $tz = new \DateTimeZone('UTC');
            $curr_Date = new \DateTime(gmdate("Y-m-d\TH:i:s\Z", $curr_time), $tz);
            $ttu_trainee->ended_on = $curr_Date->format('Y-m-d H:i:s');
            $ttu_trainee->save();
        }

        return $this->redirect(Url::to(['/tr/trainees/tests', 'id'=>$te_uid]));
    }




    // support functions


    /**
     * @param TstTestUser $ttu_trainee
     * @param LprTest $lprTest
     * @param int $rv review mode
     * @return string
     */
    private function takeHandleStartTest($ttu_trainee, $lprTest, $rv = 0)
    {
        $isReviewMode = ($rv == 1) ? true : false;

        /** @var LprQuestionSearch $searchModel */
        $searchModel = new LprQuestionSearch();
        $totalCount = $searchModel->getTestQuestionsCount($ttu_trainee->test_id);

        if(!$isReviewMode){
            $curr_pos = TstTestUser::GetMaxQuestionRank($ttu_trainee->id);
            $next_pos = $curr_pos + 1;
        }else{
            $next_pos = 1;
        }

        // handle $next_pos > $totalCount => exam already finished
        if($next_pos > $totalCount){
            $this->setAlertOperation("Error: Test not available!", "alert-info");
            return $this->render('take-done', [
                'lprTestModel'  => $lprTest,
                'totalCount'    => $totalCount,
                'utid' => $ttu_trainee->id,
                'te_uid' => $ttu_trainee->user_id,
                'test_completed' => true,
                'msg' => "Test has been completed!",
            ]);
        }

        return $this->provideNextQuestion($ttu_trainee, $lprTest, $next_pos, $totalCount, $rv);
    }


    /**
     * @param TstTestUser $ttu_trainee
     * @param LprTest $lprTest
     * @param int $next_pos
     * @param int $rv review mode
     * @return string
     */
    private function takeHandleJumpToQuestion($ttu_trainee, $lprTest, $next_pos, $rv = 0)
    {
        if (empty($next_pos)) {
            return $this->redirectWithAlertOperation("Invalid question to jump to!",
                "alert-danger", [
                    'take',
                    'utid' => $ttu_trainee->id,
                    'te_uid' => $ttu_trainee->user_id, ]);
        }

        /** @var LprQuestionSearch $searchModel */
        $searchModel = new LprQuestionSearch();

        $totalCount = $searchModel->getTestQuestionsCount($ttu_trainee->test_id);

        if($next_pos > $totalCount || $next_pos <= 0){
            $this->setAlertOperation("Jumping to an out-of-range test position!", "alert-success");
            return $this->render('take-done', [
                'lprTestModel'  => $lprTest,
                'msg' => "Jumping to an out-of-range test position!",
                'test_completed' => false,
                'utid' => $ttu_trainee->id,
                'te_uid' => $ttu_trainee->user_id, ]);
        }

        return $this->provideNextQuestion($ttu_trainee, $lprTest, $next_pos, $totalCount, $rv);
    }

    /**
     * @param TstTestUser $ttu_trainee
     * @param LprTest $lprTest
     * @param TestTakeQuestionForm $curr_FormModel
     * @param int $navDir
     * @return string
     *
     */
    private function takeHandleNextPrevQuestion($ttu_trainee, $lprTest, $curr_FormModel, $navDir)
    {
        /** @var LprQuestionSearch $searchModel */
        $searchModel = new LprQuestionSearch();

        $curr_pos = $curr_FormModel->curr_pos;
//        $curr_pos = $searchModel->getCurrPosForTestQuestion($curr_FormModel->t_id, $curr_FormModel->q_id);

        $next_pos = $curr_pos+$navDir;

        $totalCount = $searchModel->getTestQuestionsCount($ttu_trainee->test_id);

        if($navDir == 1)/*NEXT*/{
            $this->takeSaveAnswerForQuestion($curr_FormModel, $ttu_trainee, $totalCount); }

        if($next_pos > $totalCount || $next_pos <= 0){
            $this->setAlertOperation("You reached the end of the test!", "alert-success");
            return $this->render('take-done', [
                'lprTestModel'  => $lprTest,
                'msg' => "You reached the end of the test!",
                'utid' => $ttu_trainee->id,
                'te_uid' => $ttu_trainee->user_id,
                'test_completed' => ($next_pos <= 0) ? false : true, ]);
        }

        $rv = ($curr_FormModel->isReviewMode) ? 1 : 0;

        return $this->provideNextQuestion($ttu_trainee, $lprTest, $next_pos, $totalCount, $rv);
    }

    /**
     * @param TestTakeQuestionForm $curr_FormModel
     * @param TstTestUser $ttu_trainee
     * @param int $totalCount
     * @return \yii\web\Response
     */
    private function takeSaveAnswerForQuestion($curr_FormModel, $ttu_trainee, $totalCount=null)
    {
        $qid = $curr_FormModel->q_id;
        $tid = $curr_FormModel->t_id;
        $q_answr = (!empty($curr_FormModel->q_answr) && intval($curr_FormModel->q_answr) > 0)
            ? intval($curr_FormModel->q_answr)
            : null;

        $curr_pos = intval($curr_FormModel->curr_pos);

        /** @var LprQuestionSearch $searchModel */
        $searchModel = new LprQuestionSearch();

        if (!empty($q_answr)) {
            $validation_status = $searchModel->validateQAnswerForTest($q_answr, $qid, $tid);

            if ($validation_status == 0) {
                // we have a validation error -> return to the same question w. a message
                return $this->redirectWithAlertOperation("Invalid answer submission!",
                    "alert-danger",[
                        'take',
                        'utid'  => $ttu_trainee->id,
                        'te_uid'=> $ttu_trainee->user_id ]);
            }

            /** @var TstUserAnswer $tua_orig */
            $tua_orig = TstUserAnswer::findOne([
                'ttu_id' => $ttu_trainee->id,
                'question_id' => $qid
            ]);

            if (empty($tua_orig)) {

                /** @var TstUserAnswer $tua */
                $tua = TstUserAnswer::SaveAnswerForQuestion([
                    'ttu_id' => $ttu_trainee->id,
                    'question_id' => $curr_FormModel->q_id,
                    'question_order' => $curr_FormModel->curr_pos,
                    'answer_id' => $curr_FormModel->q_answr,
                    'is_correct' => TstAnswer::GetAnswerIsCorrect($curr_FormModel->q_answr),
                ]);

                if($curr_pos == $totalCount){
                    // we reached the end of the test
                    $ttu_trainee->setEndedOn();
                    $ttu_trainee->save();
                }
            } // end if empty $tua_orig
            else {
                /** @var \app\models\pojos\UserTestProperties $trnrTstProps */
                $trnrTstProps = $ttu_trainee->usr_tst_trainer_props;

                if ($trnrTstProps->canChangeAnswer) {
                    $tua_orig->answer_id = $q_answr;
                    $tua_orig->save();
                }
            }

        } // !empty $q_answr

        return null;
    }


    /**
     * @param TstTestUser $ttu_trainee
     * @param LprTest $lprTest
     * @param TestTakeQuestionForm $curr_FormModel
     * @return string
     */
    private function takeHandleSaveExit($ttu_trainee, $lprTest, $curr_FormModel)
    {
        $totalCount = (new LprQuestionSearch())->getTestQuestionsCount($ttu_trainee->test_id);

        // save answer if needed
        $this->takeSaveAnswerForQuestion($curr_FormModel, $ttu_trainee, $totalCount);

        $this->setAlertOperation("You can continue your test at any time!", "alert-info");
        return $this->render('take-done', [
            'lprTestModel'  => $lprTest,
            'msg' => "You can continue your test at any time!",
            'utid' => $ttu_trainee->id,
            'te_uid' => $ttu_trainee->user_id,
            'test_completed' => false,
        ]);
    }


    /**
     * @param TstTestUser $ttu_trainee
     * @param LprTest $lprTest
     * @param int $next_pos
     * @param int $totalCount
     * @param int $rv review mode
     * @return string
     */
    private function provideNextQuestion($ttu_trainee, $lprTest, $next_pos, $totalCount, $rv = 0)
    {
        /** @var LprQuestionSearch $searchModel */
        $searchModel = new LprQuestionSearch();

        /** @var LprQuestion $lprQuestion */
        $lprQuestion = $searchModel->getTestQuestionForPosition($ttu_trainee->test_id, $next_pos);

        if(empty($lprQuestion)){
            $this->setAlertOperation("There was an error retrieving your next question!", "alert-danger");
            return $this->render('take-done', [
                'lprTestModel'  => $lprTest,
                'utid' => $ttu_trainee->id,
                'te_uid' => $ttu_trainee->user_id,
                'test_completed' => false,
            ]);
        }


        /** @var TestTakeQuestionForm $next_FormModel */
        $next_FormModel = new TestTakeQuestionForm();

        $next_FormModel->t_id = $lprTest->id;
        $next_FormModel->q_id = $lprQuestion->id;
        $next_FormModel->setDisplayTimestamp();
        $next_FormModel->curr_pos = $next_pos;

        $isReviewMode = (intval($rv) == 1) ? true : false;
        if($isReviewMode){
            $next_FormModel->isReviewMode = $isReviewMode;
            $next_FormModel->allowSaveAndExit = false;
        }

        /** @var TstUserAnswer $tua2 */
        $tua2 = TstUserAnswer::findOne([
            'ttu_id' => $ttu_trainee->id,
            'question_id' => $lprQuestion->id
        ]);

        if(!empty($tua2)){
            $next_FormModel->q_answr = $tua2->answer_id;
        }else{
            $next_FormModel->q_answr = null;
        }

        $next_FormModel->setControlHash();

        /** @var TstAnswer[] $qAnswers */
        $qAnswers = $lprQuestion->getTstAnswers()->orderBy([TstAnswer::COL_DISPLAY_ORDER => SORT_ASC])->all();

        /** @var array|LprQMediaContent[] $qMediaItems */
        $qMediaItems = LprQMediaContent::GetQuestionMediaItems($lprQuestion->id);

        // get questions assigned to test
        /** @var TstTestQuestionOrder[] $testQuestions */
        $testQuestions = $searchModel->getQuestionsAssignedToTest($ttu_trainee->test_id)->all();

        return $this->render('take', [
            'lprTestModel'  => $lprTest,
            'pos'           => $next_pos,
            'totalCount'    => $totalCount,
            'lprQuestion'   => $lprQuestion,
            'qAnswers'      => $qAnswers,
            'qMediaItems'   => $qMediaItems,
            'formModel'     => $next_FormModel,
            'testQuestions' => $testQuestions,
//            'te_uid'        => $ttu_trainee->user_id,
            'ttuModel'      => $ttu_trainee,
        ]);
    }


    /**
     * @param null $msg
     * @param string $msg_type
     * @param array $redirUrlObj
     * @return \yii\web\Response
     */
    public function redirectWithAlertOperation($msg = null, $msg_type = 'alert-danger', $redirUrlObj = ['index'])
    {
        $msg = (empty($msg)) ? 'Error #' . rand(1000, 2000) : $msg;

        \Yii::$app->session->setFlash('alert', $msg);
        \Yii::$app->session->setFlash('alert-type', $msg_type);

        return $this->redirect($redirUrlObj);
    }


    /**
     * @param null $msg
     * @param string $msg_type
     */
    public function setAlertOperation($msg = null, $msg_type = 'alert-danger')
    {
        $msg = (empty($msg)) ? 'Error #' . rand(1000, 2000) : $msg;

        \Yii::$app->session->setFlash('alert', $msg);
        \Yii::$app->session->setFlash('alert-type', $msg_type);
    }

}