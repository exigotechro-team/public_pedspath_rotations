<?php
/* @var $this yii\web\View */
/* @var $utid integer */
/* @var $model \app\models\TstTestUser */
/* @var $lpr_test \app\models\db\ext\LprTest */

use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = [
    'label' => 'Assigned tests',
    'url' => Url::to(['/tr/trainees/tests', 'id' => $model->user_id])
];
$this->title = 'Exam Intro';
$this->params['breadcrumbs'][] = $this->title;

/** @var int $examStartStatus */
$examStartStatus = $model->getExamCanBeginStatus();

$duration = intval($lpr_test->duration_sec);
$formatDuration = \app\ro\exigotech\lpr\GmUtils::formatTimeRange($duration);

?>

<div class="row" style="margin-top: 1.5em;">
    <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10 ">
        <?= $this->render('_notification') ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-offset-2 col-sm-8">
        <h4 style="margin-bottom: 1em;margin-top: 2em;">You are about to begin your test:</h4>
        <h3 style="background-color: #f8eec9;padding: 8px;">
            <?= $model->test_title ?>
        </h3>
        <p style="margin-top: 2em;">
            Time available for test: <b><?= $formatDuration ?></b>.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-sm-offset-2 col-sm-2">
        <div style="margin-top: 1em;">
            <?php

            echo Html::a('<span class="glyphicon glyphicon-stop"></span> CANCEL',
                Url::to(['/tr/trainees/tests', 'id'=>$model->user_id]),
                [
                    'title' => 'Begin',
                    'class' => 'btn btn-xs btn-danger btn-block',
                    'style' => '',
                    'data' => ['method' => 'POST',],
                ]);

            ?>
        </div>
    </div>

    <div class="col-sm-offset-1 col-sm-2">
        <div style="margin-top: 1em;">
        <?php

        echo Html::a('<span class="glyphicon glyphicon-blackboard"></span> BEGIN',
            Url::to(['exam/start', 'utid' => $utid, 'te_uid'=>$model->user_id]),
            [
                'title' => 'Begin',
                'class' => 'btn btn-xs btn-info btn-block',
                'style' => '',
                'data' => ['method' => 'POST',],
            ]);

        ?>
        </div>
    </div>


</div>
