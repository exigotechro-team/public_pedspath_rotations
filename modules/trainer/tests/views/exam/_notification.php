<?php
/*
 * alert-info
 * alert-success
 * alert-danger
 * */
if((\Yii::$app->session->hasFlash('alert')))
{
    try {
        echo yii\bootstrap\Alert::widget([
            'options' => ['class' => Yii::$app->session->getFlash('alert-type')],
            'body' => Yii::$app->session->getFlash('alert')
        ]);
    } catch (Exception $e) {
        echo sprintf("<pre>%s</pre>", $e->getMessage());
        echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
    }
}