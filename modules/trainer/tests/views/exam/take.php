<?php
/* @var $this yii\web\View */
/* @var $lprTestModel \app\models\db\ext\LprTest */
/* @var $pos int */
/* @var $totalCount int */
/* @var $lprQuestion \app\models\db\ext\LprQuestion */
/* @var $qAnswers \app\models\TstAnswer[]|array */
/* @var $qMediaItems \app\models\db\ext\TstMediaQuestion[]|array */
/* @var $form yii\widgets\ActiveForm */
/* @var $formModel \app\models\form\TestTakeQuestionForm */
/* @var $testQuestions TstTestQuestionOrder[] */
/* @var $ttuModel \app\models\TstTestUser */

use app\assets\ImageViewAsset;
use app\models\db\search\TstTestQuestionOrder;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$te_uid = $ttuModel->user_id;

$this->params['breadcrumbs'][] = [
    'label' => 'Available tests',
    'url' => Url::to(['/tr/trainees/tests', 'id' => $te_uid])
];
$this->title = sprintf('Take test (id: %s)', $lprTestModel->id);
$this->params['breadcrumbs'][] = $this->title;

ImageViewAsset::register($this);

$lbls = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];

$percent = intval(($pos/$totalCount)*100);

$q_is_answered = false;
if(!empty($formModel->q_answr)){
    $q_is_answered = true;
}

/** @var \app\models\pojos\UserTestProperties $usrTstProps */
$usrTstProps = $ttuModel->usr_tst_properties;

/** @var \app\models\pojos\UserTestProperties $trnrTstProps */
$trnrTstProps = $ttuModel->usr_tst_trainer_props;

?>

<div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <h3>Test: <?= $lprTestModel->title ?></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <?= $this->render('_notification') ?>
        </div>
    </div>


    <?php $form = ActiveForm::begin(); ?>

    <div>
        <?php
        try {
            echo Progress::widget([
                'percent' => $percent,
                'label' => sprintf('Q# %s / %s (%s%%)', $pos, $totalCount, $percent),
                'barOptions' => ['class' => 'progress-bar-info'],
                // 'options' => ['class' => 'progress-striped']
            ]);
        } catch (Exception $e) {
        }
        ?>
    </div>

    <div class="row" style="margin-top: 2em;">
        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p style="font-weight: bold;font-size: 1.2em;"><?= Html::encode($lprQuestion->description ) ?></p>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 2em;">
                    <?php
                    /** @var \app\models\TstAnswer $qanswer */
                    foreach ($qAnswers as $qanswer) {
                        $answr_match = false;
                        if ($q_is_answered && $qanswer->id == $formModel->q_answr) {
                            $answr_match = true;
                        }
                    ?>
                        <div class="row" style="margin-top: .5em;margin-bottom: .5em;padding-left: 1em;padding-top: .5em;">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" style="background-color: <?php $clr = ($answr_match) ? "#9acfea" : ""; echo $clr; ?>;">
                                <?php
                                echo Html::activeRadio($formModel, 'q_answr', [
                                    'value' => $qanswer->id,
                                    'uncheck' => null,
                                    'label' => $lbls[$qanswer->display_order - 1],
                                    'disabled' => ($q_is_answered && !$trnrTstProps->canChangeAnswer) ? true : null,
                                ]);

                                ?>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-9">
                                <span>
                                    <?= $qanswer->body ?>

                                    <?php if ($trnrTstProps->canShowDebug /*$formModel->allowDebugInfo*/) { ?>
                                        (id: <?= $qanswer->id ?>)

                                        <?php if ($qanswer->is_correct) { ?>
                                            <span style="font-weight: bold;color:red;">*</span>
                                        <?php }
                                    } ?>
                                </span>
                            </div>
                        </div>

                    <?php } // end foreach ?>

                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
            <div class="row" id="links">
                <?php
                /** @var \app\models\LprMediaContent $qMediaItem */
                foreach ($qMediaItems as $qMediaItem) { ?>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                        <div class="thumbnail">
                            <a title="<?= Html::encode($qMediaItem->label) ?>"
                               href="/media/<?= $qMediaItem->media_uri ?>" data-gallery>
                                <?= Html::img('/media/' . $qMediaItem->media_thumb_uri, ['width' => 144, 'height' => 144, 'style' => 'padding:1em;']) ?>
                            </a>
                        </div>

                        <?php if ($trnrTstProps->canShowDebug /*$formModel->allowDebugInfo*/) { ?>
                            <div class="caption" style="padding:0.4em;">
                                <p><?= $qMediaItem->description ?></p>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 2em;">
        <div class="col-lg-4 col-md-5 col-sm-5 col-xs-6">
            <?php
            if ($trnrTstProps->canGoPrevious /*$formModel->allowGoPrevious*/) {
                if ($pos > 1) {
                    echo Html::submitButton('Previous',
                        [
                            'class' => 'btn btn-primary',
                            'name' => 'submitBtn',
                            'id' => 'submitBtnPrev',
                            'value' => 'prev'
                        ]);
                }
            }
            ?>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-5 col-xs-6">
            <?php
            if ($pos <= $totalCount) {
                echo Html::submitButton(($pos < $totalCount) ? 'Next' : 'Finish',
                    [
                        'class' => 'btn btn-primary pull-right',
                        'name' => 'submitBtn',
                        'id' => 'submitBtnNext',
                        'value' => 'next'
                    ]);
            }

            if ($trnrTstProps->canSaveAndExit /*$formModel->allowSaveAndExit*/) {
                echo Html::submitButton('Save &amp; Exit',
                    [
                        'class' => 'btn btn-info pull-right',
                        'name' => 'submitBtn',
                        'id' => 'submitBtnExit',
                        'value' => 'exit',
                        'style' => 'margin-right:2em;'
                    ]);
            }
            ?>
        </div>
    </div>

    <?php if($trnrTstProps->canJump /*$formModel->allowGoJump*/){?>
    <div style="background-color: #a6e1ec; margin-top: 2em;padding: 1em;">
        Jump to
        &nbsp;
        <?php
        $list2 = \DusanKasan\Knapsack\Collection::from($testQuestions)
            ->map(function ($value) {
                /** @var TstTestQuestionOrder $value */
//                return [$value->question_id => sprintf("%d:  %s", $value->test_order, $value->title)];
                return [$value->test_order => sprintf("%d:  %s", $value->test_order, $value->title)];
            })->reduce(function ($tmp, $val) {
                foreach ($val as $k => $v) {
                    $tmp[$k] = $v;
                }
                return $tmp;
            }, [], true
            )->toArray();

        //            $list = ArrayHelper::map($testQuestions, 'question_id', 'title');
        echo Html::dropDownList('jump_to_q', null, $list2, ['prompt' => 'question...',]);
        ?>
        &nbsp;
        <?=
        Html::submitButton('now',
            [
                'class' => 'btn btn-info btn-xs',
                'name' => 'submitBtn',
                'id' => 'submitBtnJump',
                'value' => 'jump'
            ]);
        ?>
    </div>
    <?php } ?>

    <?= $form->field($formModel, 't_id')->hiddenInput()->label(''); ?>
    <?= $form->field($formModel, 'q_id')->hiddenInput()->label(''); ?>
    <?= $form->field($formModel, 'display_timestamp')->hiddenInput()->label(''); ?>
    <?= $form->field($formModel, 'curr_pos')->hiddenInput()->label(''); ?>
    <?= $form->field($formModel, 'data_hash')->hiddenInput()->label(''); ?>

    <?php ActiveForm::end(); ?>


    <?php if($trnrTstProps->canShowDebug /*$formModel->allowDebugInfo*/){?>
    <div style="display: inherit;background-color: #a6e1ec;padding: 8px;">
        Question Tag: <?= $lprQuestion->title ?> <br/>
        Q_ID: <?= $lprQuestion->id; ?> /
        QID(f): <?= $formModel->q_id; ?><br/>
        T_ID: <?= $lprTestModel->id; ?> /
        TID(f): <?= $formModel->t_id; ?><br/>
        TIMESTAMP(f): <?= $formModel->display_timestamp; ?><br/>
        DATA_HASH(f): <?= $formModel->data_hash; ?><br/>
    </div>
    <?php } ?>


</div>


<?php $this->beginBlock('bootstrap_image_gallery'); ?>
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery" data-use-bootstrap-modal="false">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev btn-xs">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next btn-xs">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endBlock(); ?>


<?php

$disable_btns = sprintf('disable_btns = %s', ($q_is_answered) ? 'false' : 'true');

$script = <<< EOD

$disable_btns

$(document).ready(function() {

    if(disable_btns){
        // $("button#submitBtnPrev").attr("disabled","disabled");
        $("button#submitBtnNext").attr("disabled","disabled");
        $("button#submitBtnExit").attr("disabled","disabled");
    }
    
    $('input[type=radio]').change(function(){
        $("button#submitBtnPrev").attr("disabled",false);
        $("button#submitBtnNext").attr("disabled",false);
        $("button#submitBtnExit").attr("disabled",false);
    });
    
});

EOD;

$this->registerJs($script, \yii\web\View::POS_END);

$this->registerJsFile('blueimp/js/jquery.blueimp-gallery.min.js', [
    'position' => \yii\web\View::POS_END,
    'depends'   => 'yii\web\JqueryAsset',
]);
$this->registerJsFile('bimgg/js/bootstrap-image-gallery.min.js', [
    'position' => \yii\web\View::POS_END,
    'depends'   => 'yii\web\JqueryAsset',
]);

?>



