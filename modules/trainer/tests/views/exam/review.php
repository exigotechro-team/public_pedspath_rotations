<?php
/* @var $this yii\web\View */
/* @var $lprTestModel \app\models\db\ext\LprTest */
/* @var $totalCount int */
/* @var $testStatsDataProvider \yii\data\ArrayDataProvider */
/* @var $totalDataProvider \yii\data\ArrayDataProvider */
/* @var $te_uid int */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = [
    'label' => 'Available tests',
    'url' => Url::to(['/tr/trainees/tests', 'id' => $te_uid])
];
$this->title = sprintf('Review Test (id: %s)', $lprTestModel->id);
$this->params['breadcrumbs'][] = $this->title;

?>

<div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <h3>Test: <?= $lprTestModel->title ?></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <div class="row" style="margin-top:2em;">
        <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
            <h3>Results:</h3>
            <?php

            try {
                echo GridView::widget([
                    'dataProvider' => $testStatsDataProvider,
                    'layout'=>"{items}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute'=>'label',
                            'label' => 'category',
                        ],
                        [
                            'attribute'=>'no_of_q',
                            'label' => '# questions',
                        ],
                        [
                            'attribute'=>'correct',
                            'label' => '# correct',
                        ],
                        [
                            'attribute'=>'child_id',
                            'label' => '% correct',
                            'contentOptions' =>['class' => 'table_class','style'=>'display:block;'],
                            'content'=>function($data){
                                $val = intval($data['correct'])/intval($data['no_of_q']) * 100;
                                $str_val = sprintf("%.01f %%", $val);

                                return $str_val;
                            }
                        ],
                    ],
                    'tableOptions' =>['class' => 'table table-striped table-bordered'],
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
    </div>

    <div class="row" style="margin-top:2em;">
        <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
            <?php

            try {
                echo GridView::widget([
                    'dataProvider' => $totalDataProvider,
                    'layout'=>"{items}",
                    'columns' => [
                        [
                            'attribute'=>'category',
                            'label' => '',
                        ],
                        [
                            'attribute'=>'total',
                            'label' => '# questions',
                        ],
                        [
                            'attribute'=>'correct',
                            'label' => '# correct',
                        ],
                        [
                            'attribute'=>'correct_percentage',
                            'label' => '% correct',
                        ],
                    ],
                    'tableOptions' =>['class' => 'table table-striped table-bordered'],
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
    </div>

    <div class="row" style="margin-top:2em;">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <?= Html::a(Html::button('Return to tests', ['class' => 'btn btn-primary btn-md']),
                Url::to(['/tr/trainees/tests', 'id' => $te_uid]), [ 'class' => '' ]); ?>
        </div>
    </div>

</div>