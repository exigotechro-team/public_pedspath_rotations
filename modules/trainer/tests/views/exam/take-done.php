<?php
/* @var $this yii\web\View */
/* @var $lprTestModel \app\models\db\ext\LprTest */
/* @var $msg string */
/* @var $utid int|string */
/* @var $test_completed bool */
/* @var $te_uid int */

use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = [
    'label' => 'Available tests',
    'url' => Url::to(['/tr/trainees/tests', 'id' => $te_uid])
];
$this->title = sprintf('Test ID: %s', $lprTestModel->id);
$this->params['breadcrumbs'][] = $this->title;

?>

<div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <h3><?= $lprTestModel->title ?> (preview)</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <p style="font-size: 12pt;font-weight: bolder;margin-top: 2em;">
                <?= $msg ?> <br/>
            </p>

            <?php if($test_completed){?>
            <p>
                <?=
                Html::a(Html::button('See test results', ['class' => 'btn btn-primary btn-md']),
                    ['/tr/tests/exam/review', 'utid' => $utid, 'te_uid' => $te_uid],
                    ['class' => '']); ?>
            </p>

            <?php } ?>

            <p style="font-size: 12pt;font-weight: bolder;margin-top: 2em;">
                <?= Html::a('Return to tests', ['/tr/trainees/tests', 'id' => $te_uid], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
    </div>




</div>



