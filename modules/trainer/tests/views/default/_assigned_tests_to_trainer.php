<?php

/* @var $model \app\models\TstTestUser */

use yii\helpers\Url;

/* @var $trainee_id int */
/* @var $trainee_name string */
/* @var $tests_for_trainee \app\models\TstTestUser[] */

?>

<div class="row">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1"
         style="margin-left: 1em;margin-bottom: 1em;margin-top: 1em;padding: 1px; background-color: #a6e1ec;">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-10">
                <h3 style="margin-top: 10px;padding-left: 10px;"><?= $trainee_name ?> <span style="font-size: small">(id: <?= $trainee_id ?>)</span></h3>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right" style="">
                <a style="margin-top:9px;" href="<?= Url::to(['/tr/my-tests/assign', 'id' => $trainee_id, 'ret' => 'tas']) ?>" class="btn btn-xs btn-info">Assign Test</a>
            </div>
        </div>
    </div>

    <?php
    foreach ($tests_for_trainee as $tests) {
        /* @var $tests \app\models\TstTestUser */
        ?>

        <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-lg-8 col-md-8 col-sm-8 col-xs-8"
             style="margin-bottom: 1em;padding: 1px; background-color: white;">
            <span style="font-size: 14pt;font-weight: bold"><?= $tests->test_title ?></span>
            <div style="margin-top: 10px;">
                <p>
                    Status: <?= ($tests->started_on != '0000-00-00 00:00:00') ? $tests->started_on : "not started" ?>
                </p>
            </div>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"
             style="margin-bottom: 1em;padding: 1px; background-color: white;">
            <a href="<?= \yii\helpers\Url::to(['/tr/my-tests/delete', 'tu_id' => $tests->id, 'ret' => "tas" ]); ?>" class="btn btn-xs btn-danger pull-right">
                <span title='delete' class="glyphicon glyphicon-trash"></span>
            </a>
        </div>

    <?php } // end 2nd foreach
    ?>

</div>