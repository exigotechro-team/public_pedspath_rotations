<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trainees_tests \app\models\TstTestUser[] */
/* @var $trainees array */
/* @var $assigned_tests array */

$this->params['breadcrumbs'][] = ['label' => 'Available tests', 'url' => ['/tr/my-tests']];
$this->params['breadcrumbs'][] = ['label' => 'Available trainees', 'url' => ['/tr/trainees']];

$this->title = 'Tests assigned to trainees';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tests-default-index">
    <h1><?= $this->title ?></h1>
    <div class="row" style="margin-top: 1.5em;">
        <div class="col-lg-12">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <div class="row" style="margin-left: 1em;">
        <?php if (count($trainees) < 1) { ?>
            <div style="margin-bottom: 2em;">
                <h3>You have assigned no tests so far!</h3>
            </div>
        <?php } ?>

        <div>
            <a href="<?= Url::to(['/tr/my-tests/assign']) ?>" class="btn btn-sm btn-info">Assign Test to a Trainee</a>
            <a href="<?= Url::to(['/tr/trainees']) ?>" class="btn btn-sm btn-info">Manage Trainees</a>
        </div>
    </div>


    <?php
    foreach ($trainees as $trainee_id => $trainee_name) {
        echo $this->render('_assigned_tests_to_trainer', [
            'trainee_id' => $trainee_id,
            'trainee_name' => $trainee_name,
            'tests_for_trainee' => $assigned_tests[$trainee_id]
        ]);
    } ?>
</div>