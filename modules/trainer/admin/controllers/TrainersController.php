<?php

namespace app\modules\trainer\admin\controllers;

use app\models\db\ext\Organization;
use app\models\db\ext\OrganizationRepository;
use app\models\db\ext\TrainerUser;
use app\models\db\ext\UserGroupsRepository;
use app\models\db\search\PgCoordUserSearch;
use app\models\db\search\TrainerUserSearch;
use app\models\db\ext\UserGroups;
use app\models\rbac\roles\RbacRole;
use dektrium\user\events\UserEvent;
use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use dektrium\user\models\User;
use dektrium\user\traits\EventTrait;
use dektrium\user\models\Profile;
use Yii;
use app\models\db\ext\TrainersGroup;
use app\models\db\search\TrainersGroupSearch;
use yii\base\ExitException;
use yii\base\Model;
use yii\base\Module as Module2;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * TrainersController implements the CRUD actions for Trainers model.
 */
class TrainersController extends Controller
{
    use EventTrait;

    /**
     * Event is triggered before creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CREATE = 'beforeCreate';

    /**
     * Event is triggered after creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * Event is triggered before updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';

    /**
     * Event is triggered after updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';

    /**
     * Event is triggered before deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';

    /**
     * Event is triggered after deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_DELETE = 'afterDelete';

    /** @var Finder */
    protected $finder;

    /** @var Organization $org */
    public $org;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'update', 'create', 'delete', 'block'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['Trainer'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'update', 'delete', 'block'],
                        'roles' => ['adminTrainers'],
                        'roleParams' => function () {
                            return [
                                'pgc_uid' => Yii::$app->user->id,
                                'trainer_uid' => Yii::$app->request->get('id'),
                                'org_id' => $this->org->id,
                            ];
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all Trainers models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');

        /** @var PgCoordUserSearch $searchModel */
        $searchModel  = \Yii::createObject(PgCoordUserSearch::className());
        $dataProvider = $searchModel->searchTrainersUnderProgramCoord(\Yii::$app->request->get(), $this->org->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrainerUser model.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionView($id)
    {
//        if(!$this->checkIfUserIsAllowed($id)){
//            throw new ForbiddenHttpException("You are not allowed to perform this action."); }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Trainers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var TrainerUser $user */
        $user = \Yii::createObject([
            'class'    => TrainerUser::className(),
            'scenario' => 'createTrainer',
        ]);

        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $this->trigger(self::EVENT_BEFORE_CREATE, $event);

        if ($user->load(\Yii::$app->request->post()))
        {
            $user->create();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));
            $this->trigger(self::EVENT_AFTER_CREATE, $event);

            UserGroupsRepository::AddTrainerToOrganization($this->org->id, $user->id);

            $auth = Yii::$app->authManager;
            $userRole = $auth->getRole(RbacRole::TRAINER);
            $auth->assign($userRole, $user->id);

            $profile = $user->profile;

            if ($profile == null) {
                $profile = \Yii::createObject(Profile::className());
                $profile->link('user', $user);
            }

            $profile->load(\Yii::$app->request->post());
            $profile->save();

            return $this->redirect(['update', 'id' => $user->id]);
        }

        return $this->render('create', [
            'model' => $user,
            'profile' => new Profile(),
        ]);
    }

    /**
     * Updates an existing Trainers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
//        if(!$this->checkIfUserIsAllowed($id)){
//            throw new ForbiddenHttpException("You are not allowed to perform this action."); }

        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);

        $user->scenario = 'update';
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $profile = $user->profile;

        if ($profile == null) {
            $profile = \Yii::createObject(Profile::className());
            $profile->link('user', $user);
        }

        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);
        if ( $user->load(\Yii::$app->request->post()) )
        {
            $user->save();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);

            $profile->load(\Yii::$app->request->post());
            $profile->save();

            return $this->refresh();
        }

        return $this->render('update', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * Deletes an existing Trainers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {

        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'You can not remove your own account'));
        }
        else {
            $trainerUser = $this->findModel($id);
//            $event = $this->getUserEvent($trainerUser);
//            $this->trigger(self::EVENT_BEFORE_DELETE, $event);

            /** @var boolean $hasTrainees */
            $hasTrainees = UserGroupsRepository::GetTrainerHasTrainees($trainerUser->id);

            /** @var boolean $isProgCoord */
            $isProgCoord = UserGroupsRepository::CheckIsProgCoord($trainerUser->id);

            if ($hasTrainees && !$isProgCoord) {
                return $this->redirect(['block', 'id' => $trainerUser->id]);
            } elseif ($isProgCoord) {
                \Yii::$app->getSession()->setFlash('info', \Yii::t('user', 'Program Coordinator cannot be deleted or blocked'));
            } else {
                UserGroupsRepository::RemoveTrainerFromOrganization($this->org->id, $id);

                $auth = Yii::$app->authManager;
                $auth->revokeAll($trainerUser->id);

                $trainerUser->delete();

                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Trainer has been removed from organization'));
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return TrainerUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var TrainerUser $user */
        $user = $this->finder->findUserById($id);
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }

        return $user;
    }

    /**
     * Performs AJAX validation.
     *
     * @param array|Model $model
     *
     * @throws ExitException
     */
    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && !\Yii::$app->request->isPjax) {
            if ($model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }
    }

    /**
     * Blocks the user.
     *
     * @param int $id
     *
     * @return Response
     */
    public function actionBlock($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'You can not block your own account'));
        } else {
            $user  = $this->findModel($id);
//            $event = $this->getUserEvent($user);
            if ($user->getIsBlocked()) {
//                $this->trigger(self::EVENT_BEFORE_UNBLOCK, $event);
                $user->unblock();
//                $this->trigger(self::EVENT_AFTER_UNBLOCK, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been unblocked'));
            } else {
//                $this->trigger(self::EVENT_BEFORE_BLOCK, $event);
                $user->block();
//                $this->trigger(self::EVENT_AFTER_BLOCK, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been blocked'));
            }
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }


    public function beforeAction($action)
    {
        /** @var Organization $org */
        $org = OrganizationRepository::GetActiveOrganizationForProgAdmin(\Yii::$app->user->id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a program coordinator!");
        }

        return parent::beforeAction($action); // TODO: Change the auto-generated stub
    }


} // end class