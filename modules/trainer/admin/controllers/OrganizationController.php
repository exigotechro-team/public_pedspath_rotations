<?php

namespace app\modules\trainer\admin\controllers;

use app\models\db\ext\TraineeUser;
use app\models\db\ext\TrainersGroup;
use app\models\db\ext\UserGroupsRepository;
use app\models\db\User;
use app\models\db\UserGroups;
use app\models\LprTest;
use app\models\rbac\roles\RbacRole;
use function DusanKasan\Knapsack\toArray;
use Yii;
use app\models\db\ext\Organization;
use app\models\db\search\OrganizationSearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrganizationController implements the CRUD actions for Organization model.
 */
class OrganizationController extends Controller
{

    /** @var Organization $org */
    public $org;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'update', 'create', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['Program Coordinator'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'update', 'delete'],
                        'roles' => ['manageOrganization'],
                        'roleParams' => function() {
                            return ['organization' => Organization::findOne(Yii::$app->request->get('id'))];
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Organization models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;

        $searchModel = new OrganizationSearch();

        $dataProvider = null;
        if(Yii::$app->user->can(RbacRole::ADMIN)){
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }else{
            $dataProvider = $searchModel->searchByProgAdmin(Yii::$app->request->queryParams, $user_id); }

        $canCreate = false;

        if(Yii::$app->user->can(RbacRole::ADMIN)
            || Organization::GetOrgCountForUser($user_id) < 1)
        {
            $canCreate = true;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'canCreate' => $canCreate,
        ]);
    }

    /**
     * Displays a single Organization model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Organization model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        $user_id = Yii::$app->user->id;

        $canCreate = false;

        if(Yii::$app->user->can(RbacRole::ADMIN)
            || Organization::GetOrgCountForUser($user_id) < 1)
        {
            $canCreate = true;
        }

        if(!$canCreate)
        {
            $msg = <<<HTML_MSG
            You are not allowed to perform this action.
            Not more than one Organization per Program Coordinator account!
HTML_MSG;
            throw new ForbiddenHttpException($msg);
        }


        $model = new Organization();

        if ($model->load(Yii::$app->request->post()))
        {
            $model->prog_admin_id = $user_id;
            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            $model->is_active = true;

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Organization model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var Organization $model */
        $model = $this->findModel($id);

        $old_prog_admin_id = (!empty($model->prog_admin_id)) ? intval($model->prog_admin_id) : -1;


        if ($model->load(Yii::$app->request->post()))
        {
            // not ADMIN, but PROGRAM_COORDINATOR
            if (!Yii::$app->user->can(RbacRole::ADMIN)) {
                $model->prog_admin_id = \Yii::$app->user->id; }

            // ADMIN
            if(Yii::$app->user->can(RbacRole::ADMIN))
            {
                $admin_id =\Yii::$app->user->id;
                $new_prog_admin_id = (!empty($model->prog_admin_id)) ? intval($model->prog_admin_id) : -1;
                UserGroupsRepository::ReplaceUserFromGroup($admin_id , $new_prog_admin_id, $old_prog_admin_id);
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->id]);
        }
        else
        {
            $setProgAdmins = false;
            $progAdmins = [];

            if(Yii::$app->user->can(RbacRole::ADMIN))
            {
                $setProgAdmins = true;

                /** @var array $progAdmins */
                $progAdmins = ArrayHelper::map(
                    User::GetAllUserForRole(RbacRole::PROGRAM_COORDINATOR, true),
                    'user_id', 'username'
                );
                $progAdmins[Yii::$app->user->id] = RbacRole::ADMIN;
            }

            if(!true){
                print "\n<pre>" . VarDumper::dumpAsString([
                        'progAdmins' => $progAdmins,
                    ]) . "</pre>\n"; exit;
            }

            return $this->render('update', [
                'model' => $model,
                'progAdmins' => $progAdmins,
                'setProgAdmins' => $setProgAdmins,
            ]);
        }
    }

    /**
     * Deletes an existing Organization model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        /** @var Organization $model */
        $model = $this->findModel($id);

//        UserGroupsRepository::RemoveProgAdminFromGroup($model->prog_admin_id);

        $model->is_active = 0;

//        $model->delete();
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Organization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Organization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        $org = Organization::findOne(['prog_admin_id' => \Yii::$app->user->id]);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a program coordinator!");
        }

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }


}
