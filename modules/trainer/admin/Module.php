<?php

namespace app\modules\trainer\admin;
use app\models\rbac\permissions\RbacPermission;
use app\models\rbac\roles\RbacRole;
use yii\web\ForbiddenHttpException;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\trainer\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!empty(\Yii::$app->user) && !\Yii::$app->user->can(RbacRole::PROGRAM_COORDINATOR)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action.');
        }

        // custom initialization code goes here

    }
}
