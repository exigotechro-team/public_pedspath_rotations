<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$org_title = "";

/** @var \app\models\db\ext\Organization $org */
$org = null;

if(isset($this->params['organization']))
{
    $org = $this->params['organization'];
    $org_title .= $org->short_label;
}

$this->title = 'Program Coordinator';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="admin-default-index">
    <h3 style="margin-bottom: 1em;"><?= $org_title ?> / <?= $this->title ?>
    </h3>

    <div class="row">
        <div class="col-lg-4 col-md-8 col-sm-8">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4>Program Coordinator Tools</h4>
                    <ul>
                        <li><a href="/tr/admin/organization">Organization</a></li>
                        <li><a href="/tr/admin/trainers">All Trainers (for the organization)</a></li>
                        <li><a href="/tr/admin/all-trainees">All Trainees (from all trainers)</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-8 col-sm-8">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4>Trainer Tools</h4>
                    <ul>
                        <li><a href="/tr/trainees">Trainees (mine)</a></li>
                        <li><a href="/tr/my-tests">Tests</a></li>
                        <li style="list-style: none"><br/></li>
                        <li><a href="/tr/my-tests/assign">Assign Tests</a></li>
                        <li style="list-style: none"><br/></li>
                        <li><a href="/tr/tests">Manage Assignments</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-8 col-sm-8">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4>Account Admin Tools</h4>
                    <ul>
                        <li><a href="/user/settings/profile">My Profile</a></li>
                        <li><a href="/user/settings/account">My Account</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

</div>