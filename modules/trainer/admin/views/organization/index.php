<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\db\search\OrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $canCreate boolean */

$this->title = 'Organizations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p><?php if($canCreate) {
        echo Html::a('Create Organization', ['create'], ['class' => 'btn btn-success']); } ?></p>

    <?php

    $columns = [
        ['class' => 'yii\grid\SerialColumn'],

//            'institution_name',
//            'department_name',
//            'short_label',

        [
            'label' => 'Organization',
            'format'=>'raw',
            'value' => function ($model) {
                /** @var \app\models\db\ext\Organization $org */
                $org = $model;

                return sprintf("%s<br/>%s<br/>%s<br/>",
                    $org->institution_name,
                    $org->department_name,
                    $org->short_label);
            }
        ],

//            'id',
//            'prog_admin_id',
//            'created_at',
//             'updated_at',
//             'is_active',
//             'properties:ntext',

        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'buttons' => [

                //delete button
                'delete' => function ($url, $model) {
                    /** @var \app\models\db\ext\Organization $org */
                    $org = $model;

                    return !$org->is_active ? '' :
                        Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => '',
                            'data' => [
                                'confirm' => 'Are you absolutely sure? You will lose access to this group.',
                                'method' => 'POST',
                            ],
                        ]);
                },

                //update button
                'update' => function ($url, $model) {
                    /** @var \app\models\db\ext\Organization $org */
                    $org = $model;

                    return !$org->is_active ? '' :
                        Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => '',
                        ]);
                },

                //view button
                'view' => function ($url, $model) {
                    /** @var \app\models\db\ext\Organization $org */
                    $org = $model;

                    return ! $org->is_active ? '' :
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => '',
                        ]);
                },
            ],
        ],
    ];

    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            //        'filterModel' => $searchModel,
            'columns' => $columns,
        ]);
    } catch (Exception $e) {
        if (true) {
            print "\n<pre>" . $e->getMessage() . "</pre>\n";
        }
    } ?>
</div>
