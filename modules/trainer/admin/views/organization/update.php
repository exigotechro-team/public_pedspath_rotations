<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\ext\Organization */
/* @var $form yii\widgets\ActiveForm */
/* @var $progAdmins array */
/* @var $setProgAdmins boolean */


$slug = !empty($model->short_label)? $model->short_label : $model->id;

$this->title = 'Update Organization: ' . $slug;
$this->params['breadcrumbs'][] = ['label' => 'Organizations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $slug, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="organization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="organization-form col-lg-6">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'institution_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'department_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'short_label')->textInput(['maxlength' => true]) ?>

        <?php
        if($setProgAdmins)
        {
            echo $form->field($model, 'prog_admin_id')->widget(Select2::className(), [
                'data' => $progAdmins,
                'language' => 'en',
                'size' => kartik\select2\Select2::MEDIUM,

                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Select program admin...'
                ],

                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => 220,
                ],
            ])->label('Program Coordinator');
        }
        ?>

        <!-- ?= $form->field($model, 'prog_admin_id')->textInput() ? //-->

        <!-- ?= $form->field($model, 'created_at')->textInput() ? //-->

        <!-- ?= $form->field($model, 'updated_at')->textInput() ? //-->

        <?= $form->field($model, 'is_active')->checkbox() ?>

        <!-- ?= $form->field($model, 'properties')->textarea(['rows' => 6]) ? //-->

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
