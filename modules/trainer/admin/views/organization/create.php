<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\ext\Organization */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Create Organization';
$this->params['breadcrumbs'][] = ['label' => 'Organizations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-create col-lg-6">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="organization-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'institution_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'department_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'short_label')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'is_active')->checkbox() ?>

        <!-- ?= $form->field($model, 'created_at')->textInput() ? //-->

        <!-- ?= $form->field($model, 'updated_at')->textInput() ? //-->


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
