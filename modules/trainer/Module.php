<?php

namespace app\modules\trainer;
use app\models\rbac\permissions\RbacPermission;
use app\models\rbac\roles\RbacRole;
use yii\web\ForbiddenHttpException;

/**
 * tr module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\trainer\controllers';

    /**
     * @inheritdoc
     * @throws ForbiddenHttpException
     */
    public function init()
    {
        parent::init();

        if (!empty(\Yii::$app->user) && !\Yii::$app->user->can(RbacRole::TRAINER)) {
            throw new ForbiddenHttpException('You are not allowed to perform this action. X#125');
        }

        // custom initialization code goes here

        $this->modules = [
            'admin' => [
                'class' => 'app\modules\trainer\admin\Module',
            ],
            'tests' => [
                'class' => 'app\modules\trainer\tests\Module',
            ],
        ];
    }
}
