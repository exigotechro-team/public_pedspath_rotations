<?php
/* @var $this yii\web\View */
/* @var $model_trainee \app\models\pojos\UserTestProperties */
/* @var $model_trainer \app\models\pojos\UserTestProperties */
/* @var $ttuModel \app\models\TstTestUser */
/* @var $user \app\models\db\ext\TraineeUser*/
/* @var $lprTest \app\models\db\ext\LprTest*/
/* @var $ret string */
/* @var $active_tab string */

use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Trainee test settings';

$trainee_title = (!empty($user->profile->name)) ? $user->profile->name : $user->username;
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $trainee_title, 'url' => ['/tr/trainees/tests', 'id' => $ttuModel->user_id]];
$this->params['breadcrumbs'][] = sprintf("%s (id:%s)", $this->title, $ttuModel->id);

?>

<div class="row">

    <div class="col-sm-offset-1 col-sm-10" style="margin-left: 1em;margin-bottom: 1em;">
        <h3><?= $lprTest->title ?></h3>
    </div>

    <div class="col-sm-offset-1 col-sm-10" style="display: none;">
        <?= $this->render('_notification') ?>
    </div>

    <div class="col-sm-offset-1 col-sm-10" style="margin-left: 1em;margin-bottom: 1em;padding-left: 2em;">
        <ul class="nav nav-tabs" style="font-weight: bold;" >
            <li class="<?php echo ($active_tab === 'te') ? "active" : ""; ?>"><a data-toggle="tab" href="#trainee">Trainee</a></li>
            <li class="<?php echo ($active_tab === 'tr') ? "active" : ""; ?>"><a data-toggle="tab" href="#trainer">Trainer</a></li>
        </ul>

        <div class="tab-content">


            <div id="trainee" class="tab-pane fade<?php echo ($active_tab === 'te') ? " in active" : ""; ?>">
                <h3 style="font-size: 14pt;">Trainee-level Test Settings:</h3>

                <div class="col-sm-offset-1 col-sm-10" style="margin: 1em;padding-left: 2em;">
                    <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-sm-9',
                            ],
                        ],
                    ]); ?>

                    <?php echo Html::hiddenInput('ret', $ret); ?>
                    <?php echo Html::hiddenInput('for', 'te'); ?>

                    <?php foreach ([
                                       'canJump',
                                       'canGoPrevious',
                                       'canGoNext',
                                       'canShowDebug',
//                                       'canRndQuestions',
//                                       'canRndAnswers',
                                       'canChangeAnswer',
                                       'canSaveAndExit'
                                   ] as $prop_name) { ?>
                        <p><?php echo Html::activeCheckbox($model_trainee, $prop_name); ?></p>
                    <?php } ?>

                    <div style="margin-top: 2em;">
                        <?= Html::submitButton('&nbsp;&nbsp;Save&nbsp;&nbsp;', [
                            'class' => 'btn btn-sm btn-primary',
                            'name' => 'sbmtBtn',
                            'id' => 'sbmtBtnSave',
                            'value' => 'sbmtBtnSave',
                        ]) ?>
                        &nbsp;&nbsp;
                        <?= Html::submitButton('&nbsp;&nbsp;Done&nbsp;&nbsp;', [
                            'class' => 'btn btn-sm btn-info',
                            'name' => 'sbmtBtn',
                            'id' => 'sbmtBtnDone',
                            'value' => 'sbmtBtnDone',
                        ]) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>


            </div>

            <div id="trainer" class="tab-pane fade<?php echo ($active_tab === 'tr') ? " in active" : ""; ?>">
                <h3 style="font-size: 14pt;">Trainer-level Test Settings:</h3>

                <div class="col-sm-offset-1 col-sm-10" style="margin: 1em;padding-left: 2em;">
                    <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-sm-9',
                            ],
                        ],
                    ]); ?>

                    <?php echo Html::hiddenInput('ret', $ret); ?>
                    <?php echo Html::hiddenInput('for', 'tr'); ?>

                    <?php
                    foreach ([
                                 'canJump',
                                 'canGoPrevious',
                                 'canGoNext',
                                 'canShowDebug',
//                                 'canRndQuestions',
//                                 'canRndAnswers',
                                 'canChangeAnswer',
                                 'canSaveAndExit'
                             ] as $prop_name) { ?>
                        <p><?php echo Html::activeCheckbox($model_trainer, $prop_name); ?></p>
                    <?php } ?>

                    <div style="margin-top: 2em;">
                        <?= Html::submitButton('&nbsp;&nbsp;Save&nbsp;&nbsp;', [
                            'class' => 'btn btn-sm btn-primary',
                            'name' => 'sbmtBtn',
                            'id' => 'sbmtBtnSave',
                            'value' => 'sbmtBtnSave',
                        ]) ?>
                        &nbsp;&nbsp;
                        <?= Html::submitButton('&nbsp;&nbsp;Done&nbsp;&nbsp;', [
                            'class' => 'btn btn-sm btn-info',
                            'name' => 'sbmtBtn',
                            'id' => 'sbmtBtnDone',
                            'value' => 'sbmtBtnDone',
                        ]) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

            </div>

        </div>
    </div>


</div>

