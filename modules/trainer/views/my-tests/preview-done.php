<?php
/* @var $this yii\web\View */
/* @var $lprTestModel \app\models\db\ext\LprTest */
/* @var $totalCount int */
/* @var $msg string */

use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => 'Available tests', 'url' => ['/tr/my-tests']];
$this->title = sprintf('Test ID: %s', $lprTestModel->id);
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <h3><?= $lprTestModel->title ?> (preview)</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <p style="font-size: 12pt;font-weight: bolder;margin-top: 2em;">
                <?= $msg ?> <br/>
            </p>
            <p style="font-size: 12pt;font-weight: bolder;margin-top: 2em;">
                <?= Html::a('Return to list of available tests', ['index'], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
    </div>




</div>



