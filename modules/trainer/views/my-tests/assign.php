<?php
/* @var $this yii\web\View */
/* @var UserTestAssign $model */
/* @var null|string $ret */

use app\models\form\UserTestAssign;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->params['breadcrumbs'][] = ['label' => 'Available tests', 'url' => ['/tr/my-tests']];
$this->params['breadcrumbs'][] = ['label' => 'Available trainees', 'url' => ['/tr/trainees']];

$this->title = 'Assign test';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row" style="margin:0 2em;">
    <div class="col-sm-offset-1">
        <h2>Assign test to trainee</h2>
    </div>

    <div class="row" style="margin-top: 1.5em;">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <?php
        $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'action' => ['finish-assign'],
            'method' => 'post',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-9',
                ],
            ],
        ]);

        echo Html::hiddenInput('ret', $ret);
    ?>

    <div class="row">
        <div class="col-sm-10">
            <?= $form->field($model, 'tid')->widget(Select2::className(), [
                'data' => $model->tests,
                'language' => 'en',
                'size' => kartik\select2\Select2::MEDIUM,

                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Select test...'
                ],

                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => 320,
                ],
            ])->label('Test'); ?>

            <?= $form->field($model, 'uid')->widget(Select2::className(), [
                'data' => $model->trainees,
                'language' => 'en',
                'size' => kartik\select2\Select2::MEDIUM,

                'options' => [
                    'multiple' => false,
                    'placeholder' => 'Select trainee...'
                ],

                'pluginOptions' => [
                    'allowClear' => true,
                    'width' => 320,
                ],
            ])->label('Trainee'); ?>


            <?= $form->field($model, 'notify')->checkbox()->label('Notify by email?') ?>

            <div class="pull-left col-sm-offset-3">
                <?= Html::submitButton('Assign', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
