<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\db\search\LedgerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Available tests to assign';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <h2>Tests available for assignment to trainees</h2>
</div>

<div class="row">
    <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
        <?= $this->render('_notification') ?>
    </div>
</div>

<div class="row">
    <?php
    try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                    'id',
//                    'obj_system',
//                    'node_type',
//                    'node_id',
                [
                    'attribute' => 'Test',
                    'value' => function ($model) {
                        /** @var \app\models\db\ext\LprLedger $model */
                        /** @var \app\models\db\ext\LprTest $lprTest */
                        $lprTest = $model->getLprTest()->one();

                        return sprintf("%s (id: %s)", $lprTest->title, $model->node_id);
                    },
                ],

//                    'node_status',
//                    'subnode_type',
//                    'subnode_id',
                //'created_at',
                //'args:ntext',
                //'notes:ntext',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Assign to trainees',
                    'template'   => '{assign}',
                    'urlCreator' => function ($action, $model) {
                        return Url::to([$action, 'tid' => $model['node_id']]);
                    },
                    'options' => [
                        'style' => 'width: 5%'
                    ],
                    'buttons' => [

                        //assign button
                        'assign' => function ($url, $model) {
                            /** @var \app\models\db\ext\LprLedger $lprLedger */
                            $lprLedger = $model;
                            return "<div style='text-align:center;margin-left:auto;margin-right:auto;'>"
                                . Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, [
                                    'title' => 'Assign',
                                    'class' => 'btn btn-info btn-sm',
                                    'style' => '',
                                    'data' => [
//                                        'confirm' => 'This will help you assign this test to a trainee.',
                                        'method' => 'POST',
                                    ],
                                ]) . "</div>";
                        },
                    ],
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Preview Test',
                    'template'   => '{preview}',
                    'urlCreator' => function ($action, $model) {
                        return Url::to([$action, 'tid' => $model['node_id']]);
                    },
                    'options' => [
                        'style' => 'width: 5%'
                    ],
                    'buttons' => [

                        //assign button
                        'preview' => function ($url, $model) {
                            /** @var \app\models\db\ext\LprLedger $lprLedger */
                            $lprLedger = $model;
                            return "<div style='text-align:center;margin-left:auto;margin-right:auto;'>"
                                . Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => 'Assign',
                                    'class' => 'btn btn-primary btn-sm',
                                    'style' => '',
                                    'data' => [
//                                        'confirm' => 'This will help you assign this test to a trainee.',
//                                        'method' => 'POST',
                                    ],
                                ]) . "</div>";
                        },
                    ],
                ],
            ],
        ]);
    } catch (Exception $e) {
        if (true) {
            print "\n<pre>" . $e->getMessage() . "</pre>\n";
        }
    }
    ?>
</div>
