<?php

use app\models\form\TraineeProfileEntryForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\db\ext\TraineeUser */
/* @var $profile \dektrium\user\models\Profile */
/* @var $prop_names array */
/* @var $prop_values array */
/* @var $user_profiles TraineeProfileEntryForm[] */



$this->title = (!empty($user->profile->name)) ? $user->profile->name : $user->username;
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Update Profile';

$newUpForm = new TraineeProfileEntryForm();
$newUpForm->up_uid = $user->id;
?>
<div class="row" style="padding: 0 2em;">
    <div>
        <h2 style="margin-bottom: 1em;">Profile Details:</h2>
    </div>

    <?php

    $cntr = 1;

    /** @var TraineeProfileEntryForm $user_profile */
    foreach ($user_profiles as $user_profile) {
    ?>
        <div class="row" style="margin-bottom: 1em;">
            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-sm-9',
                    ],
                ],
            ]); ?>
            <div class="col-sm-4">
                <?php echo Html::activeHiddenInput($user_profile, 'up_pname'); ?>
                <?php echo Html::activeHiddenInput($user_profile, 'up_uid'); ?>
                <?php echo Html::activeHiddenInput($user_profile, 'upid'); ?>
                <?php echo Html::activeHiddenInput($user_profile, 'up_ctrl_hash'); ?>
                <p style="font-weight: bold;"><?= $prop_names[$user_profile['up_pname']]; ?></p>
            </div>
            <div class="col-sm-5">
                <?php

                if(in_array($user_profile['up_pname'], ['te_inst', 'te_pgy', 'te_type'])){
                    echo Html::activeDropDownList(
                        $user_profile,
                        'up_pval',
                        $prop_values[$user_profile['up_pname']],
                        [
                            'id'    => sprintf('up_prop_pval_id_%s', $cntr),
                            'prompt' => '-',
                            'style' => 'background:#f9f9f9;',
                            'class' => 'form-control',
                        ]);
                }else{

                }

                ?>
            </div>
            <div class="col-sm-3">
                <?= Html::submitButton('<span class="glyphicon glyphicon-floppy-disk"></span>', [
                    'class' => 'btn btn-sm btn-primary',
                    'name' => 'sbmtBtn',
                    'id' => sprintf('sbmtBtnUpdate_%s', $cntr),
                    'value' => 'sbmtBtnUpdate',
                ]) ?>
                <?= Html::submitButton('<span class="glyphicon glyphicon-trash"></span>', [
                    'class' => 'btn btn-sm btn-danger',
                    'name' => 'sbmtBtn',
                    'id' => sprintf('sbmtBtnDelete_%s', $cntr++),
                    'value' => 'sbmtBtnDelete',
                ]) ?>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php } ?>
</div>

<div class="row" style="padding: 0 2em;">
    <div>
        <h3 style="margin-bottom: 1em;margin-top: 1em;">Add profile entry:</h3>
   </div>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-12',
            ],
        ],
    ]); ?>

    <div class="col-sm-4" style="margin:1em;">
        <?= Html::activeDropDownList(
            $newUpForm,
            'up_pname',
            $prop_names,
            [
                'id'    => 'up_prop_pname_id',
                'prompt' => '-',
                'onchange' => "getPropValues($(this).val())",
                'style' => 'background:#f9f9f9;',
                'class' => 'form-control',
            ]); ?>
    </div>

    <div class="col-sm-4" style="margin:1em;">
        <?= Html::activeDropDownList(
            $newUpForm,
            'up_pval',
            [],
            [
                'id'    => 'up_prop_pval_id',
                'prompt' => '-',
                'style' => 'background:#f9f9f9;',
                'class' => 'form-control',
            ]); ?>
    </div>

    <div class="col-sm-2" style="margin:1em;">
        <?= Html::submitButton('Add', [
            'class' => 'btn btn-sm btn-info',
            'name' => 'sbmtBtn',
            'id' => 'sbmtBtnNew',
            'value' => 'sbmtBtnNew',
        ]) ?>
    </div>

    <?= Html::activeHiddenInput($newUpForm, 'up_uid') ?>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< EOD

function getPropValues(pname){
   $.get("/tr/trainees/get-te-prop-vals", { 'pname': pname})
   .done(
       function(data){
           // console.log('got something back! ' + data);
           $("select#up_prop_pval_id").html(data);
       }
   );   
}

$(document).ready(function() {
   
});

EOD;

$this->registerJs($script, \yii\web\View::POS_END);

?>