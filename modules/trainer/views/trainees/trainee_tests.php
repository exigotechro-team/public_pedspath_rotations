<?php

use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\db\ext\TraineeUser */
/* @var $userTestsDataProvider yii\data\ActiveDataProvider */


$this->title = (!empty($user->profile->name)) ? $user->profile->name : $user->username;
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Tests';
?>
<div class="row" style="padding: 0 2em;">
    <div>
        <h2 style="margin-bottom: 1em;">Tests:</h2>

        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-9',
                ],
            ],
        ]); ?>

        <div class="pull-left col-sm-offset-3">
            <?php // echo  Html::submitButton('Assign', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="row" style="margin-top: 2em;">
    <div class="col-sm-12">

        <?php
        try {
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $userTestsDataProvider,
                'itemView' => '_list_item',
                'options' => [
                    'tag' => 'div',
                    'class' => 'list-wrapper',
                    'id' => 'list-wrapper',
                ],
                'layout' => "{pager}\n{items}\n{summary}",
            ]);
        }
        catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            if (true) {
                echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            }
        }

        ?>
    </div>
</div>

