<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\db\ext\TraineeUser */
/* @var $profile \dektrium\user\models\Profile */

$this->title = (!empty($user->profile->name)) ? $user->profile->name : $user->username;
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row" style="padding: 0 2em;">
    <div>
        <h2 style="margin-bottom: 1em;">Trainee Details:</h2>

        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-9',
                ],
            ],
        ]); ?>

        <?= $form->field($profile, 'name') ?>
        <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($user, 'password')->passwordInput() ?>

        <div class="pull-left col-sm-offset-3">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>