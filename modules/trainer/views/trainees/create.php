<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\ext\TraineeUser */
/* @var $profile \dektrium\user\models\Profile */

$this->title = 'Create Trainee';
$this->params['breadcrumbs'][] = ['label' => 'Trainees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainers-create col-lg-6 col-md-6">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'wrapper' => 'col-sm-9  col-md-8 col-lg-6',
            ],
        ], ]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 32]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 55]) ?>

    <?= $form->field($profile, 'name') ?>

    <div class="form-group field-profile-name">
        <label class="control-label col-sm-3" for="send_msg">Send email</label>
        <div class="col-sm-9  col-md-8 col-lg-6">
            <?php
                echo Html::checkbox('send_msg', false);
            ?>
            <p class="help-block help-block-error "></p>
        </div>
    </div>



    <?php // echo $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>
    <?php // echo $form->field($model, 'password')->passwordInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
