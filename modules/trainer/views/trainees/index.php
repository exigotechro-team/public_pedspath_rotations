<?php

use app\models\db\ext\TraineeUser;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\db\search\TraineeUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prop_names array */
/* @var $prop_values array */
/* @var $filter_params array */


$this->title = 'Trainees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainers-index">
    <div class="row">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <?= $this->render('_notification') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
        <p><?= Html::a('Create Trainee', ['create'], ['class' => 'btn btn-success']) ?></p>
        </div>
    </div>
    <div class="row" style="margin-top: 0em;">
        <div class="col-sm-offset-1 col-sm-9" style="">
            <h4>Filter by user properties:</h4>
        </div>

        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'action' => '/tr/trainees/route',
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-9',
                ],
            ],
        ]); ?>
        <div class="col-sm-offset-1 col-sm-3" style="margin-top:1em;">
            Institution:
            <?= Html::dropDownList(
                'te_inst',
                $filter_params['te_inst'],
                $prop_values['te_inst'],
                [
                    'id'    => 'te_inst_prop_pval',
                    'prompt' => '-',
                    'style' => 'background:#f9f9f9;',
                    'class' => 'form-control',
                ]); ?>
        </div>
        <div class="col-sm-3" style="margin-top:1em;">
            PGY:
            <?= Html::dropDownList(
                'te_pgy',
                $filter_params['te_pgy'],
                $prop_values['te_pgy'],
                [
                    'id'    => 'te_pgy_prop_pval',
                    'prompt' => '-',
                    'style' => 'background:#f9f9f9;',
                    'class' => 'form-control',
                ]); ?>
        </div>
        <div class="col-sm-3" style="margin-top:1em;">
            Trainee Type:
            <?= Html::dropDownList(
                'te_type',
                $filter_params['te_type'],
                $prop_values['te_type'],
                [
                    'id'    => 'te_type_prop_pval',
                    'prompt' => '-',
                    'style' => 'background:#f9f9f9;',
                    'class' => 'form-control',
                ]); ?>
        </div>
        <div class="col-sm-1" style="margin-top:1em;">&nbsp;<br/>
            <?= Html::submitButton('<span class="glyphicon glyphicon-blackboard"></span>', [
                'class' => 'btn btn-sm btn-primary',
                'name' => 'sbmtBtn',
                'id' => 'sbmtBtnConfig',
                'value' => 'sbmtBtnConfig',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="row" style="margin-top: 2em;">
        <div class="col-sm-12">
            <?php try {
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        'username',
                        'email:email',
//                [
//                    'attribute' => 'created_at',
//                    'value' => function ($model) {
//                        if (!isset($model->created_at)) {
//                            return "";
//                        }
//
//                        if (extension_loaded('intl')) {
//                            return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
//                        } else {
//                            return date('Y-m-d G:i:s', $model->created_at);
//                        }
//                    },
//                ],

                        [
                            'header' => Yii::t('user', 'Test'),
                            'value' => function ($model) {
                                return "<div style='text-align:center;margin-left:auto;margin-right:auto;'>"
                                    . Html::a('<span class="glyphicon glyphicon-copy"></span> Info',
                                        Url::to(['update', 'id' => $model->id]),
                                        [
                                            'title' => 'Info',
                                            'class' => 'btn btn-xs btn-info btn-block',
                                            'style' => '',
                                            'data' => ['method' => 'POST',],
                                        ]) . "</div>";
                            },
                            'format' => 'raw',
                            'options' => [
                                'style' => 'width: 15%'
                            ],
                        ],

                        [
                            'header' => Yii::t('user', 'Block status'),
                            'value' => function ($model) {
                                if ($model->isBlocked) {
                                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-success btn-block',
                                        'data-method' => 'post',
                                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                                    ]);
                                } else {
                                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                                        'class' => 'btn btn-xs btn-danger btn-block',
                                        'data-method' => 'post',
                                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                                    ]);
                                }
                            },
                            'format' => 'raw',
                            'options' => [
                                'style' => 'width: 10%'
                            ],
                        ],





//                        [
//                            'class' => 'yii\grid\ActionColumn',
//                            'template' => '{view} {update} {delete}',
//
//                            'options' => [
//                                'style' => 'width: 10%'
//                            ],
//                        ],
                    ],
                ]);
            } catch (Exception $e) {
                if (true) {
                    echo "\n<pre>" . $e->getMessage() . "</pre>\n";
                    echo "\n<pre>" . $e->getTraceAsString() . "</pre>\n";
                }
            } ?>
        </div>
    </div>
</div>