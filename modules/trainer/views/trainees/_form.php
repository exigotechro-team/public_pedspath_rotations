<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\ext\TrainersGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trainers-form col-lg-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_user_role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'parent_user_id')->textInput() ?>

    <?= $form->field($model, 'child_user_role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'child_user_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
