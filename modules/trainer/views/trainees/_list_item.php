<?php

/* @var $model \app\models\TstTestUser */

use app\models\attrs\ExamStartStatus;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var int $examStartStatus */
$examStartStatus = $model->getExamCanBeginStatus();

$testTimeLeft = $model->getTestTimeRemaining();
$isTestCompleted = (strcmp(strtolower( $testTimeLeft), strtolower( "Test Completed")) === 0) ? true : false;

?>

<div class="row" style="margin-bottom: 1em;border-right: thin double #e4eef3;">
    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-6" style="border-left: thin double #ff0000;margin-left: 1em;margin-bottom: 1em;">

        <?php
        if (!true) {
            if ($examStartStatus == ExamStartStatus::START_NOT_SET
                || $examStartStatus == ExamStartStatus::PRIOR_TO_START_TIME) {

                echo "<div style='margin:0 0 2em;'><span class=\"label label-default\" style='padding:8px;'>Test NOT started</span></div>";

            } elseif ($examStartStatus == ExamStartStatus::DURING_START_END_TIME) {

                echo "<div style='margin:1em;'><span class=\"label label-info\" style='padding:1em;'>Test started</span></div>";

            } elseif ($examStartStatus == ExamStartStatus::POST_END_TIME) {
                echo "<div style='margin:1em;'><span class=\"label label-success\" style='padding:1em;'>Test finished</span></div>";
            }
        }
        ?>
        <div style="margin-top: 10px;">
            <span style="font-size: 14pt;font-weight: bold"><?= $model->test_title ?></span>
            <p><span class="label label-info"><?= sprintf("%s / %s ", $model->no_of_questions , $model->no_of_answers) ?></span> <span style="font-size: smaller">(Qs/As)</span></p>
            <p>Time to complete: <span class="label label-<?= ($isTestCompleted) ? 'success' : 'default'  ?>"><?= $testTimeLeft ?></span>
            <p>Notify user:
                <?php

                $url = Url::to(["/tr/my-tests/notify", 'tu_id' => $model->id, 'ret' => 'tes']);

                echo Html::a('<span class="glyphicon glyphicon-bell"></span> notify trainee', $url, [
                    'title' => 'Notify trainee about test',
                    'class' => 'btn btn-default btn-xs',
                    'style' => '',
                    'data' => [
                         'confirm' => 'Notify trainee about taking this test?',
                         'method' => 'POST',
                    ],
                ]);
                ?>
            </p>

            <p>Test Settings:
                <?php

                $url = Url::to(["/tr/my-tests/configure", 'tu_id' => $model->id, 'ret' => 'tes']);

                echo Html::a('<span class="glyphicon glyphicon-cog"></span> configure', $url, [
                    'title' => 'configure properties',
                    'class' => 'btn btn-default btn-xs',
                    'style' => '',
                    'data' => [
                        // 'confirm' => 'This will help you assign this test to a trainee.',
                        // 'method' => 'POST',
                    ],
                ]);

                ?>

            </p>

        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5" style="margin-left: 1em;margin-bottom: 1em;">
        <div style="width:120px">
            <?php
            if (true) {
                echo Html::a('<span class="glyphicon glyphicon-time"></span> Review test',
                    Url::to(['/tr/tests/exam/continue', 'utid' => $model->id, 'te_uid' => $model->user_id, 'rv' => 1]),
                    [
                        'title' => 'Preview test',
                        'class' => 'btn btn-xs btn-primary btn-block',
                        'style' => '',
                        'data' => ['method' => 'POST',],
                    ]);
            }

            if (true) {
                echo Html::a('<span class="glyphicon glyphicon-blackboard"></span> Test results',
                    Url::to(['/tr/tests/exam/review', 'utid' => $model->id, 'te_uid' => $model->user_id]),
                    [
                        'title' => 'Review test',
                        'class' => 'btn btn-xs btn-primary btn-block',
                        'style' => '',
                        'data' => ['method' => 'POST',],
                    ]);
            }


            if (true) {
                if ($examStartStatus == ExamStartStatus::DURING_START_END_TIME
                    || $examStartStatus == ExamStartStatus::POST_END_TIME) {

                    echo Html::a('<span class="glyphicon glyphicon-refresh"></span> Reset test',
                        Url::to(['/tr/tests/exam/reset', 'utid' => $model->id, 'te_uid' => $model->user_id]),
                        [
                            'title' => 'Reset test',
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'style' => '',
                            'data' => [
                                'confirm' => 'This will delete all existing answers for this test!',
                                'method' => 'POST',
                            ],
                        ]);
                }


                if ($examStartStatus == ExamStartStatus::POST_END_TIME && !$model->is_test_completed) {

                    echo Html::a('<span class="glyphicon glyphicon-expand"></span> Extend time',
                        Url::to(['/tr/tests/exam/extend', 'utid' => $model->id, 'te_uid' => $model->user_id]),
                        [
                            'title' => 'Reset test',
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'style' => '',
                            'data' => [
                                'confirm' => 'This will extend by 1 day the exam time!',
                                'method' => 'POST',
                            ],
                        ]);
                }




                if ($examStartStatus == ExamStartStatus::START_NOT_SET
                    || $examStartStatus == ExamStartStatus::PRIOR_TO_START_TIME) {

                    $url = Url::to(["/tr/my-tests/delete", 'tu_id' => $model->id, 'ret' => 'tes']);

                    echo Html::a('<span class="glyphicon glyphicon-trash"></span> de-assign test', $url, [
                        'title' => 'de-assign test',
                        'class' => 'btn btn-danger btn-xs btn-block',
                        'style' => '',
                        'data' => [
                        // 'confirm' => 'This will help you assign this test to a trainee.',
                        // 'method' => 'POST',
                        ],
                    ]);

                }

            }
            ?>
        </div>
    </div>
</div>