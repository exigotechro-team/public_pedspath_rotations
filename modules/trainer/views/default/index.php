<?php

use yii\helpers\Url;

$this->title = 'Trainer Home Page';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row" style="margin-top: 1.5em;">
    <div class="col-lg-12">
        <?= $this->render('_notification') ?>
    </div>
</div>

<div class="tr-default-index">
    <h3><?= $this->title ?></h3>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4>Trainer Tools</h4>
                    <ul>
                        <li><a href="<?= Url::to(['/' . $this->context->module->id . '/trainees']) ?>">Trainees</a></li>
                        <li><a href="<?= Url::to(['/' . $this->context->module->id . '/my-tests']) ?>">Tests</a></li>
                        <li style="list-style: none"><br/></li>
                        <li><a href="<?= Url::to(['/' . $this->context->module->id . '/my-tests/assign']) ?>">Assign Tests</a></li>
                        <li style="list-style: none"><br/></li>
                        <li><a href="<?= Url::to(['/' . $this->context->module->id . '/tests']) ?>">Manage Assignments</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4>Account Admin Tools</h4>
                    <ul>
                        <li><a href="<?= Url::to(['/user/settings/profile']) ?>">Profile</a></li>
                        <li><a href="<?= Url::to(['/user/settings/account']) ?>">Account</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>