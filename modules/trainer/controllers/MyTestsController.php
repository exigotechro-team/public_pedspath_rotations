<?php

namespace app\modules\trainer\controllers;

use app\models\db\ext\LprQuestion;
use app\models\db\ext\LprTest;
use app\models\db\ext\Organization;
use app\models\db\ext\TraineeUser;
use app\models\db\ext\TstAnswer;
use app\models\db\search\LedgerSearch;
use app\models\db\search\TraineeUserSearch;
use app\models\db\search\TrainerUserSearch;
use app\models\db\search\TstTestQuestionOrder;
use app\models\db\search\TstTestUserSearch;
use app\models\form\TestQuestionPreviewForm;
use app\models\form\UserTestAssign;
use app\models\LprLedger;
use app\models\LprQMediaContent;
use app\models\pojos\UserTestProperties;
use app\models\search\LprQuestionSearch;
use app\models\TstTestUser;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use yii\base\Exception;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class MyTestsController extends \yii\web\Controller
{
    /** @var Organization $org */
    public $org;

    public $trainer_id;


    public function actionIndex()
    {
        $searchModel = new LedgerSearch();
        $searchModel->subnode_id = $this->org->id;
        $dataProvider = $searchModel->searchOrgAssignedTests(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param int $tid LprTest ID
     * @return \yii\web\Response|string
     */
    public function actionPreview($tid)
    {
        if(!empty($tid)){
            try {
                $this->validateTestAssignedToOrg($tid);
            } catch (Exception $e) {
                return $this->redirectWithAlertOperation('This test was not assigned to your organization!', 'alert-danger', [Url::to('index')]);
            }
        }

        /** @var LprTest $lprTest */
        $lprTest = LprTest::find()->where(['id' => $tid])->one();

        if(empty($lprTest)){
            return $this->redirectWithAlertOperation(
                sprintf("No test was found w. id: %s!", $tid),
                'alert-danger',
                [Url::to('index')]);
        }

        $searchModel = new LprQuestionSearch();
        $totalCount = $searchModel->getTestQuestionsCount($tid);

        /** @var TestQuestionPreviewForm $formModel */
        $formModel = new TestQuestionPreviewForm();
        $formModel->t_id = $lprTest->id;

        $post = \Yii::$app->request->post();

        $navDir = 1;

        if (!empty($post) && $formModel->load($post) && $formModel->validate())
        {
            $qid = $formModel->q_id;
            $tid = $formModel->t_id;
            $q_answr = (!empty($formModel->q_answr) && $formModel->q_answr > 0) ? $formModel->q_answr : null;

            if(!$formModel->verifyControlHash()){
                return $this->redirectWithAlertOperation("Invalid test - question association!",
                    "alert-danger",
                    ['index', 'tid' => $tid]);
            }


            if (!empty($q_answr))
            {
                $validation_status = $searchModel->validateQAnswerForTest($q_answr, $qid, $tid);

                if ($validation_status == 0) {
                    // we have a validation error -> return to the same question w. a message
                    return $this->redirectWithAlertOperation("Invalid answer submission!",
                        "alert-danger",
                        ['preview', 'tid' => $tid, 'qid' => $qid]);
                }
            }

            $navDir = ( $post['submitBtn'] == 'prev') ? -1 : 1;

            /* If we select a question to jump to... */

            if ($post['submitBtn'] == 'jump') {

                /* do not increment the position for the question => we do not advance */
                $navDir = 0;

                $jump_to_q = (array_key_exists('jump_to_q', $post)) ? $post['jump_to_q'] : null;

                if (!empty($jump_to_q)) {
                    $qid = $jump_to_q;
                }
            }


            // decide if to do something with the data we've got: save the answer for the student'
            // NOTHING IN PREVIEW
        }

        $pos = 1;

        /** @var LprQuestion $lprQuestion */
        $lprQuestion = null;

        if (!empty($qid) && intval($qid) > 0) {
            $curr_pos = $searchModel->getCurrPosForTestQuestion($tid, $qid);
            $pos = $curr_pos + $navDir;

            if($pos > $totalCount){
                // go to preview-done page with message
                $this->setAlertOperation("You reached the end of the test!", "alert-success");
                return $this->render('preview-done', [
                    'lprTestModel'  => $lprTest,
                    'totalCount'    => $totalCount,
                    'msg' => "You reached the end of the test!",
                ]);
            }elseif ($pos <= 0){
                // go to preview-done page with message
                $this->setAlertOperation("You reached the beginning of the test!", "alert-info");
                return $this->render('preview-done', [
                    'lprTestModel'  => $lprTest,
                    'totalCount'    => $totalCount,
                    'msg'   => "You reached the beginning of the test!",
                ]);
            }
        }

        $lprQuestion = $searchModel->getTestQuestionForPosition($tid, $pos);

        if(empty($lprQuestion)){
            $this->setAlertOperation("There was an error retrieving your next question!", "alert-danger");
            return $this->render('preview-done', [
                'lprTestModel'  => $lprTest,
                'totalCount'    => $totalCount,
            ]);
        }

        $formModel->q_id = $lprQuestion->id;
        $formModel->q_answr = null;
        $formModel->setDisplayTimestamp();
        $formModel->setControlHash();

        /** @var TstAnswer[] $qAnswers */
        $qAnswers = $lprQuestion->getTstAnswers()->orderBy([TstAnswer::COL_DISPLAY_ORDER => SORT_ASC])->all();

        /** @var array|LprQMediaContent[] $qMediaItems */
        $qMediaItems = LprQMediaContent::GetQuestionMediaItems($lprQuestion->id);

        // get questions assigned to test
        /** @var TstTestQuestionOrder[] $testQuestions */
        $testQuestions = $searchModel->getQuestionsAssignedToTest($tid)->all();

        return $this->render('preview', [
            'lprTestModel'  => $lprTest,
            'pos'           => $pos,
            'totalCount'    => $totalCount,
            'lprQuestion'   => $lprQuestion,
            'qAnswers'      => $qAnswers,
            'qMediaItems'   => $qMediaItems,
            'formModel'     => $formModel,
            'testQuestions' => $testQuestions,
        ]);

    }

        /**
     * @param null|int $tid
     * @param null $id
     * @param null|string $ret
     * @return string
     * @throws Exception
     */
    public function actionAssign($tid = null, $id = null, $ret = null)
    {
        if(!empty($id)){
            $this->layout = 'column2';
        }

        // get all trainees for trainer
        $traineeSearch = new TraineeUserSearch();

        /** @var User[] $trainees */
        $trainees = $traineeSearch->searchTraineesForTrainer($this->trainer_id);

        // get all tests for trainer
        $testsSearch = new LedgerSearch();

        /** @var LprTest[] $lprTests */
        $lprTests = $testsSearch->searchTestsForOrg($this->org->id);

        $post = \Yii::$app->request->post();
        /** @var UserTestAssign $formModel */
        $formModel = new UserTestAssign();
        $formModel->load($post);


        if(!empty($tid)){
            $formModel->tid = $tid; }

        if(!empty($formModel->tid)){
            $this->validateTestAssignedToOrg($formModel->tid); }


        if(!empty($id)){
            $formModel->uid = $id; }

        if(!empty($formModel->uid)){
            $this->validateTrainee($formModel->uid); }


        $traineesList = [];

        /** @var User $trainee */
        foreach ($trainees as $trainee) {
            $traineesList[$trainee->id] = $trainee->profile->name;
        };

        $formModel->trainees = $traineesList;
        $formModel->tests = ArrayHelper::map($lprTests, 'id', 'title');

        $this->view->params['user_id'] = $id;
        $this->view->params['action_tag'] = 4;

        return $this->render('assign', [
            'model'     => $formModel,
            'ret' => $ret,
        ]);

    }


    /**
     * @return \yii\web\Response
     * @throws Exception
     */
    public function actionFinishAssign(){
        $post = \Yii::$app->request->post();

        $ret = $post['ret'];

        $utaModel = new UserTestAssign();
        $utaModel->load($post);

        if(!empty($utaModel->tid)){
            $this->validateTestAssignedToOrg($utaModel->tid); }

        if(!empty($utaModel->uid)){
            $this->validateTrainee($utaModel->uid); }


        /**
         * create test-user association/assignment
         */
        $tstTestUser =  new TstTestUser($utaModel->tid, $utaModel->uid);
        $tstTestUser->save();

        if($utaModel->notify){
            $tstTestUser->notifyByEmail();
            $this->setAlertOperation("Notification successfully sent!", 'alert-success');
        }

        if(!empty($ret)){
            if($ret == 'tas'){
                return $this->redirect(['/tr/tests']);
            }elseif ($ret == 'tes'){
                return $this->redirect(Url::to(['/tr/trainees/tests', 'id' => $utaModel->uid]));
            }
        }

        return $this->redirect(['/tr']);
    }


    /**
     * @param $tu_id
     * @param null|string $ret
     * @return \yii\web\Response
     * @throws Exception
     */
    public function actionNotify($tu_id, $ret = null)
    {
        /** @var TstTestUser $tstTestUser */
        $tstTestUser = TstTestUser::find()->where(['id' => $tu_id])->one();
        if(empty($tstTestUser)){
            throw new Exception("Not a valid test assignment provided!"); }

        if(!empty($tstTestUser->test_id)){
            $this->validateTestAssignedToOrg($tstTestUser->test_id); }

        if(!empty($tstTestUser->user_id)){
            $this->validateTrainee($tstTestUser->user_id); }

        $tstTestUser->notifyByEmail();
        $this->setAlertOperation("Notification successfully sent!", 'alert-success');

        if(!empty($ret)){
            if($ret == 'tas'){
                return $this->redirect(['/tr/tests']);
            }elseif ($ret == 'tes'){
                return $this->redirect(Url::to(['/tr/trainees/tests', 'id' => $tstTestUser->user_id]));
            }
        }

        return $this->redirect(['/tr']);
    }


        /**
     * @param $tu_id
     * @param null|string $ret
     * @return \yii\web\Response
     * @throws Exception
     */
    public function actionDelete($tu_id, $ret = null){

        /** @var TstTestUser $ttuModel */
        $ttuModel = TstTestUser::find()->where(['id' => $tu_id])->one();
        if(empty($ttuModel)){
            throw new Exception("Not a valid test assignment provided!"); }

        if(!empty($ttuModel->test_id)){
            $this->validateTestAssignedToOrg($ttuModel->test_id); }

        if(!empty($ttuModel->user_id)){
            $this->validateTrainee($ttuModel->user_id); }

        try {
            $ttuModel->delete();
        } catch (StaleObjectException $e) {
        } catch (\Exception $e) {

        } catch (\Throwable $e) {
        }

        if(!empty($ret)){
            if($ret == 'tas'){
                return $this->redirect(['/tr/tests']);
            }elseif ($ret == 'tes'){
                return $this->redirect(Url::to(['/tr/trainees/tests', 'id' => $ttuModel->user_id]));
            }
        }

        return $this->redirect(['/tr']);
    }


    /**
     * @param $tu_id
     * @param null|string $ret
     * @return mixed
     * @throws Exception
     */
    public function actionConfigure($tu_id, $ret = null){

        if(!empty($tu_id)){
            $this->layout = 'column2';
        }

        /** @var TstTestUser $ttuModel */
        $ttuModel = TstTestUser::find()->where(['id' => $tu_id])->one();
        if(empty($ttuModel)){
            throw new Exception("Not a valid test assignment provided!"); }

        if(!empty($ttuModel->test_id)){
            $this->validateTestAssignedToOrg($ttuModel->test_id); }

        if(!empty($ttuModel->user_id)){
            $this->validateTrainee($ttuModel->user_id); }

        $post = \Yii::$app->request->post();
        $isPostRequest = (!empty($post)) ? true : false;

        $forUsrType = 'te';

        if ($isPostRequest) {

            $sbmtBtn = $post['sbmtBtn'];

            if ($sbmtBtn == 'sbmtBtnDone'){
                $ret = $post['ret'];

                if(!empty($ret)){
                    if($ret == 'tas'){
                        return $this->redirect(['/tr/tests']);
                    }elseif ($ret == 'tes'){
                        return $this->redirect(Url::to(['/tr/trainees/tests', 'id' => $ttuModel->user_id]));
                    }
                }

                return $this->redirect(['/tr/']);
            }

            if ($sbmtBtn == 'sbmtBtnSave') {

                $forUsrType = strcmp($post['for'], 'te') == 0 ? 'te' : 'tr';

                $userProps = new UserTestProperties();
                $userProps->load($post);

                if ($forUsrType == 'te') {
                    $ttuModel->usr_tst_properties = $userProps;
                } elseif ($forUsrType == 'tr') {
                    $ttuModel->usr_tst_trainer_props = $userProps;
                }

                $ttuModel->save();

                $msg_tmpl = "%s's user test properties successfully saved!";
                $part = ($forUsrType == 'te') ? "Trainee" : "Trainer";
                $msg = sprintf($msg_tmpl, $part);
                $this->setAlertOperation($msg, 'alert-success');
            }
        }


        /** @var TraineeUser $user */
        $user = TraineeUser::find()->where(['id' => $ttuModel->user_id])->one();

        /** @var LprTest $user */
        $lprTest = LprTest::find()->where(['id' => $ttuModel->test_id])->one();

        $this->view->params['user_id'] = $ttuModel->user_id;
        $this->view->params['action_tag'] = 3;

        return $this->render('configure', [
            'model_trainee' => $ttuModel->usr_tst_properties,
            'model_trainer' => $ttuModel->usr_tst_trainer_props,
            'ttuModel' => $ttuModel,
            'user' => $user,
            'lprTest' => $lprTest,
            'ret' => $ret,
            'active_tab' => $forUsrType,
        ]);
    }


    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->trainer_id = \Yii::$app->user->id;

        $org = Organization::GetOrgForTrainer($this->trainer_id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a trainer!");
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $tid
     * @throws Exception
     */
    private function validateTestAssignedToOrg($tid)
    {
        $searchModel = new LedgerSearch();
        if(!$searchModel->checkTestIsAssignedToOrg($tid, $this->org->id)){
            throw new Exception(sprintf("Test %s is not assigned to this organization %s", $tid, $this->org->id));
        };
    }

    /**
     * @param $uid
     * @throws Exception
     */
    private function validateTrainee($uid)
    {
        $traineeSearch = new TraineeUserSearch();
        if(!$traineeSearch->checkTraineeExistsForTrainer($uid, $this->trainer_id)){
            throw new Exception(sprintf("Trainee %s is not assigned to this trainer %s", $uid, $this->trainer_id));
        };
    }


    /**
     * @param null $msg
     * @param string $msg_type
     * @param array $redirUrlObj
     * @return \yii\web\Response
     */
    public function redirectWithAlertOperation($msg = null, $msg_type = 'alert-danger', $redirUrlObj = ['index'])
    {
        $msg = (empty($msg)) ? 'Error #' . rand(1000, 2000) : $msg;

        \Yii::$app->session->setFlash('alert', $msg);
        \Yii::$app->session->setFlash('alert-type', $msg_type);

        return $this->redirect($redirUrlObj);
    }


    /**
     * @param null $msg
     * @param string $msg_type
     */
    public function setAlertOperation($msg = null, $msg_type = 'alert-danger')
    {
        $msg = (empty($msg)) ? 'Error #' . rand(1000, 2000) : $msg;

        \Yii::$app->session->setFlash('alert', $msg);
        \Yii::$app->session->setFlash('alert-type', $msg_type);
    }


}