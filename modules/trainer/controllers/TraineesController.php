<?php

namespace app\modules\trainer\controllers;

use app\models\db\ext\Organization;
use app\models\db\ext\TraineeUser;
use app\models\db\ext\TrainerUser;
use app\models\db\ext\UserGroupsRepository;
use app\models\db\search\TraineeUserSearch;
use app\models\db\search\TrainerUserSearch;
use app\models\db\search\TstTestUserSearch;
use app\models\db\search\UserProfileSearch;
use app\models\db\UserGroups;
use app\models\db\UserProfile;
use app\models\form\TraineeProfileEntryForm;
use app\models\rbac\roles\RbacRole;
use dektrium\user\events\UserEvent;
use dektrium\user\Finder;
use dektrium\user\helpers\Password;
use dektrium\user\models\User;
use dektrium\user\traits\EventTrait;
use dektrium\user\models\Profile;
use DusanKasan\Knapsack\Collection;
use Yii;
use app\models\db\ext\TrainersGroup;
use app\models\db\search\TrainersGroupSearch;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Module as Module2;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * TraineesController implements the CRUD actions for Trainers model.
 */
class TraineesController extends Controller
{
    public $layout = null;

    static $prop_names = [
        'te_pgy' => 'PGY',
        'te_inst' => 'INSTITUTION',
        'te_type' => 'TRAINEE TYPE',
//        'te_rotation_start' => 'ROTATION START',
//        'te_rotation_end' => 'ROTATION END',
    ];

    static $prop_values = [
        'te_pgy' => [
            'PGY I' => 'PGY I',
            'PGY II' => 'PGY II',
            'PGY III' => 'PGY III',
            'PGY IV' => 'PGY IV',
            'PGY V' => 'PGY V',
            'PGY VI' => 'PGY VI',
        ],
        'te_inst' => [
            'Loyola' => 'Loyola',
            'NMH' => 'NMH',
            'Northshore' => 'Northshore',
            'UofC' => 'UofC',
            'Rush' => 'Rush',
            'NS' => 'NS',
        ],
        'te_type' => [
            'RESIDENT' => 'RESIDENT',
            'FELLOW' => 'FELLOW',
            'STUDENT' => 'STUDENT',
            'TEST TAKER' => 'TEST TAKER',
        ],
    ];

    use EventTrait;

    /**
     * Event is triggered before creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CREATE = 'beforeCreate';

    /**
     * Event is triggered after creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * Event is triggered before updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';

    /**
     * Event is triggered after updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';

    /**
     * Event is triggered before deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';

    /**
     * Event is triggered after deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_DELETE = 'afterDelete';

    /** @var Finder */
    protected $finder;

    /** @var Organization $org */
    public $org;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'update', 'create', 'delete', 'block', 'update-profile', 'tests'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['Trainer'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view', 'update', 'delete', 'block', 'update-profile', 'tests'],
                        'roles' => ['adminTrainees'],
                        'roleParams' => function () {
                            return [
                                'trainer_uid' => Yii::$app->user->id,
                                'trainee_uid' => Yii::$app->request->get('id'),
                                'org_id' => $this->org->id,
                            ];
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all Trainers models.
     * @param string|null $inst
     * @param string|null $pgy
     * @param string|null $ttype
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionIndex($inst=null, $pgy=null, $ttype = null)
    {
        $filter_params = [
            'te_inst'   => $inst,
            'te_pgy'    => $pgy,
            'te_type'   => $ttype,
            'tr_uid'    => \Yii::$app->user->id,
        ];

        $has_search_params = false;
        if(!empty($inst) || !empty($pgy) || !empty($ttype)){
            $has_search_params = true; }

        $uids = [];

        if($has_search_params){
            $userProfileSearch = new UserProfileSearch();
            $uids = ArrayHelper::getColumn($userProfileSearch->findOwnersForProps($filter_params)->asArray()->all(), 'owner_id');
        }

        /** @var TraineeUserSearch $searchModel */
        $searchModel  = \Yii::createObject(TraineeUserSearch::className());
        $dataProvider = $searchModel->searchUnderTrainer(Yii::$app->request->queryParams, Yii::$app->user->id, $uids, $has_search_params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'prop_names'    => self::$prop_names,
            'prop_values'   => self::$prop_values,
            'filter_params' => $filter_params,
        ]);
    }


    /**
     * @return Response
     */
    public function actionRoute()
    {
        $post = \Yii::$app->request->post();

        $isPostRequest = (!empty($post)) ? true : false;

        /**
         * If POST => redirect
         */
        if ($isPostRequest) {

            $sbmtBtn = $post['sbmtBtn'];

            if ($sbmtBtn == 'sbmtBtnConfig') {

                $inst = '';
                if(array_key_exists('te_inst', $post)){
                    $inst = $post['te_inst']; }

                $pgy = '';
                if(array_key_exists('te_pgy', $post)){
                    $pgy = $post['te_pgy']; }

                $ttype = '';
                if(array_key_exists('te_type', $post)){
                    $ttype = $post['te_type']; }

                return $this->redirect(Url::to(['index', 'inst' => $inst, 'pgy' => $pgy, 'ttype' => $ttype]));
            }
        }

        return $this->redirect(Url::to(['index']));
    }

    /**
     * Displays a single TraineeUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Trainees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var TraineeUser $user */
        try {
            $user = \Yii::createObject([
                'class' => TraineeUser::class,
                'scenario' => 'createTrainee',
            ]);

            $this->performAjaxValidation($user);

        } catch (InvalidConfigException $e) {
        } catch (ExitException $e) {}

        //  $event = $this->getUserEvent($user);
        //  $this->trigger(self::EVENT_BEFORE_CREATE, $event);

        $post = \Yii::$app->request->post();

        if ($user->load($post))
        {
            $send_welcome_message = false;

            if(array_key_exists('send_msg', $post)){
                $send_msg = intval($post['send_msg']);
                $send_msg = ($send_msg == 1) ? 1 : 0;
                $send_welcome_message = ($send_msg == 1) ? true : false;
            }

            $username = null;
            $password = null;

            if (array_key_exists('TraineeUser', $post)){

                if(array_key_exists('username', $post['TraineeUser'])){
                    if(!empty($post['TraineeUser']['username'])){
                        $username = $post['TraineeUser']['username'];
                    }
                }
                if(array_key_exists('password', $post['TraineeUser'])) {
                    if(!empty($post['TraineeUser']['password'])) {
                        $password = $post['TraineeUser']['password'];
                    }
                }
            }

            try {
                if(!empty($username)){
                    $user->username = $username; }

                if(!empty($password)){
                    $user->password = $password; }

                $user->create($send_welcome_message);

            } catch (\Exception $e) {
                echo sprintf("<pre>%s</pre>", $e->getMessage());
                echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            }

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));
//            $this->trigger(self::EVENT_AFTER_CREATE, $event);

            UserGroupsRepository::AddTraineeToTrainer(\Yii::$app->user->id, $user->id);

            $auth = Yii::$app->authManager;
            $userRole = $auth->getRole(RbacRole::TRAINEE);
            try {
                $auth->assign($userRole, $user->id);
            } catch (\Exception $e) {}

            $profile = $user->profile;

            if ($profile == null) {
                try {
                    $profile = \Yii::createObject(Profile::className());
                } catch (InvalidConfigException $e) {
                }
                $profile->link('user', $user);
            }

            $profile->load(\Yii::$app->request->post());
            $profile->save();

            return $this->redirect(['update', 'id' => $user->id]);
        }

        return $this->render('create', [
            'model' => $user,
            'profile' => new Profile(),
        ]);
    }

    /**
     * Updates an existing Trainers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws ExitException
     */
    public function actionUpdate($id)
    {
        $this->layout = 'column2';

        Url::remember('', 'actions-redirect');
        $user = $this->findModel($id);

        $user->scenario = 'update';
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $profile = $user->profile;

        if ($profile == null) {
            $profile = \Yii::createObject(Profile::className());
            $profile->link('user', $user);
        }

        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);
        if ( $user->load(\Yii::$app->request->post()) )
        {
            $user->save();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);

            $profile->load(\Yii::$app->request->post());
            $profile->save();

            return $this->refresh();
        }

        $this->view->params['user_id'] = $user->id;
        $this->view->params['action_tag'] = 1;

        return $this->render('update', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * Updates an existing Trainers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionUpdateProfile($id)
    {
        $this->layout = 'column2';

        /** @var TrainerUser $trainer */
        $trainer = TrainerUser::find()->where(['id' => Yii::$app->user->id])->one();

        /** @var TraineeUser $trainee */
        $trainee = $trainer->getTrainee($id);

        if(empty($trainee)){
            $msg = "Trainee does not belong to this trainer!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', ['/tr/trainees/']);
        }

        $profile = $trainee->profile;
        if ($profile == null) {
            $profile = \Yii::createObject(Profile::className());
            $profile->link('user', $trainee);
        }

        $post = \Yii::$app->request->post();

        $isPostRequest = (!empty($post)) ? true : false;

        /**
         * If POST => save record
         * make sure you know what to save
         * Differentiate between NEW records and UPDATES to existing records
         */
        if ($isPostRequest) {

            $sbmtBtn = $post['sbmtBtn'];

            if($sbmtBtn == 'sbmtBtnNew'){

                $newUpForm = new TraineeProfileEntryForm();
                $newUpForm->load($post);

                /** @var UserProfile $newUserProfile */
                $newUserProfile = (new UserProfile($newUpForm))->save();

                if (!true) {
                    print "\n<pre>" . VarDumper::dumpAsString([
                            'post' => $post,
                            'newUpForm' => $newUpForm->toArray(),
                            'newUserProfile' => $newUserProfile->toArray(),
                        ]) . "</pre>\n";
                    exit;
                }

            }elseif (in_array($sbmtBtn , ['sbmtBtnUpdate', 'sbmtBtnDelete'])){

                $existingUpForm = new TraineeProfileEntryForm();
                $existingUpForm->load($post);

                if(!$existingUpForm->verifyControlHash()){
                    $msg = "There was an error in data integrity!";
                    return $this->redirectWithAlertOperation($msg, 'alert-danger', ['/tr/trainees/']);
                }

                $user_p = UserProfile::find()->where(['id' => $existingUpForm->upid])->one();
                if(empty($user_p)){
                    $msg = "There was an error processing your data!";
                    return $this->redirectWithAlertOperation($msg, 'alert-danger', ['/tr/trainees/']);
                }

                if($sbmtBtn == 'sbmtBtnUpdate'){
                    $user_p->prop_val = $existingUpForm->up_pval;
                    $user_p->save();
                }elseif ($sbmtBtn == 'sbmtBtnDelete'){
                    try {
                        $user_p->delete();

                    } catch (StaleObjectException $e) {
                    } catch (\Throwable $e) {
                    }
                }
            }else{
                $this->redirect('/tr/trainees');
            }
        }

        $user_profiles = Collection::from($trainee->getUserProps()->asArray()->all())
            ->map(function ($value) {
                return TraineeProfileEntryForm::CreateFromUserProfile($value);
            })->toArray();

        $this->view->params['user_id'] = $trainee->id;
        $this->view->params['action_tag'] = 2;


        return $this->render('update_profile', [
            'user'          => $trainee,
            'profile'       => $profile,
            'prop_names'    => self::$prop_names,
            'prop_values'   => self::$prop_values,
            'user_profiles' => $user_profiles,
        ]);
    }

    /**
     * @param string $pname
     * @return string
     */
    public function actionGetTePropVals($pname)
    {
        $prop_values = self::$prop_values;

        $out_str = "<option> - </option>";

        if(array_key_exists($pname, $prop_values)){
            $vals = $prop_values[$pname];

            foreach ($vals as $prop_key => $prop_val) {
                $out_str .= sprintf("<option value='%s'>%s</option>\n", $prop_key, $prop_val);
            }
        }

        return $out_str;
    }

    /**
     * Updates an existing Trainers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws ExitException
     */
    public function actionTests($id)
    {
        $this->layout = 'column2';

        /** @var TrainerUser $trainer */
        $trainer = TrainerUser::find()->where(['id' => Yii::$app->user->id])->one();

        /** @var TraineeUser $trainee */
        $trainee = $trainer->getTrainee($id);

        if(empty($trainee)){
            $msg = "Trainee does not belong to this trainer!";
            return $this->redirectWithAlertOperation($msg, 'alert-danger', ['/tr/trainees/']);
        }

        $post = \Yii::$app->request->post();

        $isPostRequest = (!empty($post)) ? true : false;

        if ($isPostRequest) {

        }

        $this->view->params['user_id'] = $trainee->id;
        $this->view->params['action_tag'] = 3;

        $teTestsSearch = new TstTestUserSearch();
        $teTestsDataProvider = $teTestsSearch->searchAssignedTestsToTrainee($trainee->id);

        return $this->render('trainee_tests', [
            'user' => $trainee,
            'userTestsDataProvider' => $teTestsDataProvider,
        ]);
    }


    /**
     * Deletes an existing Trainers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionDelete($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'You can not remove your own account'));
        } else {
            $traineeUser = $this->findModel($id);
            $event = $this->getUserEvent($traineeUser);
            $this->trigger(self::EVENT_BEFORE_DELETE, $event);

            $userGroups = UserGroups::find()->where([
                'parent_user_id' => Yii::$app->user->id,
                'child_user_id' => $id ])->all();

            /** @var UserGroups $userGroup */
            foreach($userGroups as $userGroup){
                try {
                    $userGroup->delete();
                } catch (StaleObjectException $e) {
                } catch (\Throwable $e) {
                }
            }

            $auth = Yii::$app->authManager;
            $auth->revokeAll($traineeUser->id);

            try {
                $traineeUser->delete();
            } catch (StaleObjectException $e) {
            } catch (\Throwable $e) {
            }

            $this->trigger(self::EVENT_AFTER_DELETE, $event);
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been deleted'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param int $id
     *
     * @return TraineeUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var TraineeUser $user */
        $user = $this->finder->findUserById($id);
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }

        return $user;
    }

    /**
     * Performs AJAX validation.
     *
     * @param array|Model $model
     *
     * @throws ExitException
     */
    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && !\Yii::$app->request->isPjax) {
            if ($model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }
    }

    /**
     * Blocks the user.
     *
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionBlock($id)
    {
        if ($id == \Yii::$app->user->getId()) {
            \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'You can not block your own account'));
        } else {
            $user  = $this->findModel($id);
//            $event = $this->getUserEvent($user);
            if ($user->getIsBlocked()) {
//                $this->trigger(self::EVENT_BEFORE_UNBLOCK, $event);
                $user->unblock();
//                $this->trigger(self::EVENT_AFTER_UNBLOCK, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been unblocked'));
            } else {
//                $this->trigger(self::EVENT_BEFORE_BLOCK, $event);
                $user->block();
//                $this->trigger(self::EVENT_AFTER_BLOCK, $event);
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been blocked'));
            }
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }

    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $org = Organization::GetOrgForTrainer(\Yii::$app->user->id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a trainer!");
        }

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    /**
     * @param null $msg
     * @param string $msg_type
     * @param array $redirUrlObj
     * @return \yii\web\Response
     */
    public function redirectWithAlertOperation($msg = null, $msg_type = 'alert-danger', $redirUrlObj = ['/tr/trainees/'])
    {
        $msg = (empty($msg)) ? 'Error #' . rand(1000, 2000) : $msg;

        \Yii::$app->session->setFlash('alert', $msg);
        \Yii::$app->session->setFlash('alert-type', $msg_type);

        return $this->redirect($redirUrlObj);
    }


    /**
     * @param null $msg
     * @param string $msg_type
     */
    public function setAlertOperation($msg = null, $msg_type = 'alert-danger')
    {
        $msg = (empty($msg)) ? 'Error #' . rand(1000, 2000) : $msg;

        \Yii::$app->session->setFlash('alert', $msg);
        \Yii::$app->session->setFlash('alert-type', $msg_type);
    }


} // end class