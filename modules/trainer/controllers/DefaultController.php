<?php

namespace app\modules\trainer\controllers;

use app\models\db\ext\Organization;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Default controller for the `tr` module
 */
class DefaultController extends Controller
{
    /** @var Organization $org */
    public $org;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $org = Organization::GetOrgForTrainer(\Yii::$app->user->id);

        if(!empty($org)){
            $this->view->params['organization'] = $org;
            $this->org = $org;
        }else{
            throw new ForbiddenHttpException("There is no organization for which you are a trainer!");
        }

        return parent::beforeAction($action);
    }

}