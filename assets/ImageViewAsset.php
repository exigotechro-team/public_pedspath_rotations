<?php
namespace app\assets;

use yii\web\AssetBundle;

class ImageViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'blueimp/css/blueimp-gallery.min.css',
        'bimgg/css/bootstrap-image-gallery.min.css',
    ];
    public $js = [
//        'bimgg/js/bootstrap-image-gallery.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}