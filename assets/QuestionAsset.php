<?php
namespace app\assets;

use yii\web\AssetBundle;

class QuestionAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'blueimp/css/blueimp-gallery.min.css',
        'bimgg/css/bootstrap-image-gallery.min.css',
    ];
    public $js = [
        'js/q_main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}