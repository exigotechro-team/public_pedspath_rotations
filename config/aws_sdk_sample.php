<?php

// https://github.com/fedemotta/yii2-aws-sdk

return [
    'class' => 'fedemotta\awssdk\AwsSdk',
    'credentials' => [ //you can use a different method to grant access
        'key' => 'your-aws-key',
        'secret' => 'your-aws-secret',
    ],
    'region' => 'us-east-1',
    'version' => 'latest',
];