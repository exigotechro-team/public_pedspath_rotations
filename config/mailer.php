<?php

return [
    'class' => 'yii\swiftmailer\Mailer',
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    'useFileTransport' => false,

    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'smtp_server', // e.g. smtp.mandrillapp.com or smtp.gmail.com
        'username' => 'smtp_username' 
        'password' => 'smtp_pwd' 
        'port' => '587', // Port 25 is a very common port too
        'encryption' => 'tls', // It is often used, check your provider or mail server specs
    ],

    'messageConfig' => [
        'from' => ['no-reply@luriepathrotation.info' => 'No-Reply'], // this is needed for sending emails
        'charset' => 'UTF-8',
    ]
];
