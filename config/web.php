<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$mailer = require(__DIR__ . '/mailer.php');
$user_web = require(__DIR__ . '/user_web.php');
$aws_sdk = require(__DIR__ . '/aws_sdk.php');

$config = [
    'id' => 'basic',
    'name' => 'Path Rotation',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],

    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],

    'modules' =>
        [
            'user' => $user_web,

            'rbac' => [
                'class' => 'dektrium\rbac\RbacWebModule',
            ],
            'gm-admin' => [
                'basePath' => '@app/modules/gm_admin',
                'class' => 'app\modules\gm_admin\AdminTasks',
            ],
            'tr' => [
                'basePath' => '@app/modules/trainer',
                'class' => 'app\modules\trainer\Module',
            ],
            'te' => [
                'basePath' => '@app/modules/trainee',
                'class' => 'app\modules\trainee\Module',
            ],
        ],

    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '...',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],

        'authManager' => [
//            'class' => 'yii\rbac\DbManager',
            'class' => 'dektrium\rbac\components\DbManager',
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user'
                ],
            ],
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],

        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],

        'session' => [
            'class' => 'yii\redis\Session',
        ],

        'cache' => [
            'class' => 'yii\redis\Cache' /*'yii\caching\FileCache'*/,
        ],

        'awssdk' => $aws_sdk,
    ],
    'params' => $params,

    'homeUrl' => '/my-account',
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment

    if (!true) {
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '::1'],
        ];
    }

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;