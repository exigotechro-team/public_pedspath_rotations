<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;dbname=db_name',
    'username' => 'db_uname',
    'password' => 'db_pwd',
    'charset' => 'utf8',
];
