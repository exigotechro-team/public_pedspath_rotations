<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$mailer = require(__DIR__ . '/mailer.php');
$user_console = require(__DIR__ . '/user_console.php');
$aws_sdk = require(__DIR__ . '/aws_sdk.php');

return [
    'id' => 'basic-console',
    'name' => 'Path Rotation',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',

    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],

    'modules' =>
        [
            'gii' => 'yii\gii\Module',

            'user' => $user_console,

            'rbac' => [
                'class' => 'dektrium\rbac\RbacConsoleModule',
            ],
            'gm-admin' => [
                'basePath' => '@app/modules/gm_admin',
                'class' => 'app\modules\gm_admin\AdminTasks',
            ],
            'tr' => [
                'basePath' => '@app/modules/trainer',
                'class' => 'app\modules\trainer\Module',
            ],
            'te' => [
                'basePath' => '@app/modules/trainee',
                'class' => 'app\modules\trainee\Module',
            ],
        ],

    'components' =>
        [
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'mailer' => $mailer,
            'db' => $db,
            'authManager' => [
//    		'class' => 'yii\rbac\DbManager',
                'class' => 'dektrium\rbac\components\DbManager',
            ],

            'redis' => [
                'class' => 'yii\redis\Connection',
                'hostname' => 'localhost',
                'port' => 6379,
                'database' => 0,
            ],

            'session' => [
                'class' => 'yii\redis\Session',
            ],

            'cache' => [
                'class' => 'yii\redis\Cache' /*'yii\caching\FileCache'*/,
            ],

            'awssdk' => $aws_sdk,
        ],

    'params' => $params,
];
