<?php

use app\models\rbac\roles\RbacRole;
use dektrium\user\controllers\AdminController;
use dektrium\user\controllers\RegistrationController;
use dektrium\user\controllers\SecurityController;
use dektrium\user\controllers\SettingsController;

return [

    'class' => 'dektrium\user\Module',
    'admins' => [''],
    'modelMap' => [
        'RegistrationForm'  => 'app\models\user\RegistrationForm',
        'ResendForm'        => 'app\models\user\ResendForm',
        'RecoveryForm'      => 'app\models\user\RecoveryForm',
    ],
    'mailer' => [
        'sender'                => [
            'no-reply@luriepathrotation.info' => 'PedsPath Notifications'
        ],
        'welcomeSubject'        => 'Welcome to Path Rotation',
        'confirmationSubject'   => 'Please confirm your email',
        'reconfirmationSubject' => 'Please confirm your new email',
        'recoverySubject'	    => 'Recover your login',

        'viewPath'              => '@app/views/user/mail',
    ],

//    'as frontend' => 'dektrium\user\filters\FrontendFilter',
    'controllerMap' => [
        'registration' => [
            'class' => RegistrationController::className(),
            'on ' . RegistrationController::EVENT_AFTER_CONFIRM => function ($e)
            {
                $auth = Yii::$app->authManager;
                $userRole = $auth->getRole(RbacRole::USER);
                $auth->assign($userRole, Yii::$app->user->id);

                Yii::$app->response->redirect(array('/user/settings/profile'))->send();
                Yii::$app->end();
            }
        ],
        'security' => [
            'class' => SecurityController::className(),
            'on ' . SecurityController::EVENT_AFTER_LOGIN => function ($e)
            {
                if(Yii::$app->user->can(RbacRole::ADMIN))
                {
                    Yii::$app->response->redirect(array('/gm-admin/'))->send();
                }
                elseif (Yii::$app->user->can(RbacRole::PROGRAM_COORDINATOR))
                {
                    Yii::$app->response->redirect(array('/tr/admin'))->send();
                }
                elseif (Yii::$app->user->can(RbacRole::TRAINER))
                {
                    Yii::$app->response->redirect(array('/tr/'))->send();
                }
                elseif (Yii::$app->user->can(RbacRole::TRAINEE))
                {
                    Yii::$app->response->redirect(array('/te/tests'))->send();
                }
                elseif (Yii::$app->user->can(RbacRole::USER))
                {
                    Yii::$app->response->redirect(array('/'))->send();
                }
                else
                    {
                    Yii::$app->response->redirect(array('/'))->send();
                }

                Yii::$app->end();
            }
        ],
        'admin' => [
            'class' => AdminController::className(),
            'on ' . AdminController::EVENT_BEFORE_DELETE => function ($e)
            {
                $auth = Yii::$app->authManager;
                $auth->revokeAll(Yii::$app->user->id);
            }
        ],
        'settings' => [
            'class' => SettingsController::className(),
            'on ' . SettingsController::EVENT_AFTER_PROFILE_UPDATE => function ($e) {
                Yii::$app->response->redirect(array('/user/settings/profile'))->send();
                Yii::$app->end();
            }
        ],
    ],
];