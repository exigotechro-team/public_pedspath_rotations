<?php

return [
    'class' => 'dektrium\user\Module',
    'admins' => ['admin'],
    'mailer' => [
        'sender'                => [
            'no-reply@luriepathrotation.info' => 'Path Rotation Notifications'
        ],
        'welcomeSubject'        => 'Welcome to Path Rotation',
        'confirmationSubject'   => 'Please confirm your email',
        'reconfirmationSubject' => 'Please confirm your new email',
        'recoverySubject'	    => 'Recover your login',

        'viewPath'              => '@app/views/user/mail',
    ],
];