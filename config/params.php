<?php

return [
    'adminEmail' => 'web@luriepathrotation.info',
    'domainName' => 'https://www.luriepathrotation.info',
    'siteName'   => 'Path Rotation',
];
