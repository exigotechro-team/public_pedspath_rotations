<?php

/* @var $this yii\web\View */

$this->title = 'Peds Path Trainee Testing';
?>
<div class="site-index" style="margin-top:2em;">
    <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
            <h3 style="text-align: center;">Peds Path Tests</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
            <p style="text-align: center;font-weight: bold;font-size: 16pt;">
                <a class="btn btn-info btn-sm"
                   style="text-align: center;font-weight: bold;font-size: 10pt;"
                   href="/<?= (\Yii::$app->user->isGuest) ? 'user/login' : 'my-account' ?>">&nbsp;&nbsp;&nbsp;<?= (\Yii::$app->user->isGuest) ? 'Login' : 'My Account &raquo;' ?>&nbsp;&nbsp;&nbsp;</a>
            </p>
        </div>
    </div>
</div>
