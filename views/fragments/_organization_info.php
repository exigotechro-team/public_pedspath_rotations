<?php

/* @var $this yii\web\View */
/* @var $model app\models\db\ext\Organization */

?>

<?php if(!empty($model)) { ?>
    <ul class="breadcrumb pull-right" style="width: 100%; text-align: right;">
        <li><?= $model->short_label ?></li>
        <li>
            <span class="label label-info">
                <?php echo sprintf("(o%s : u%s)", $model->id, \Yii::$app->user->id); ?>
            </span>
        </li>
    </ul>
<?php } ?>