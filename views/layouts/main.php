<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

\Yii::$app->homeUrl = '/my-account';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => \Yii::$app->params['siteName'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    try {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],

                !Yii::$app->user->isGuest ? (
                ['label' => 'My Account', 'url' => ['/my-account']]
                ) : (""),

                Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/user/security/login']]
                ) : (
                ['label' => 'Sign out (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/user/security/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ]
                )
            ],
        ]);
    } catch (Exception $e) {
    }
    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 " style="padding-right: 0px;">
                <?php try {
                    echo Breadcrumbs::widget([
                        'homeLink' => [
                            'label' => Yii::t('yii', 'My Account'),
                            'url' => ['/my-account'],
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 " style="padding-left: 0px;">
                <?php
                    if(isset($this->params['organization'])){
                        echo $this->render('../fragments/_organization_info', ['model' => $this->params['organization'],]);
                    }
                ?>
            </div>
        </div>

        <?php
        try {
            echo Alert::widget();
        }
        catch (Exception $e){
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
        }
        ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Lurie Path Rotation <?= date('Y') ?><br/>
            <?= Yii::powered() ?>
        </p>

        <p class="pull-right" style="text-align: right;">
            &nbsp;<a href="<?= Url::to('/site/index');?>" >Home</a><br/>
            &nbsp;<a href="<?= Url::to('/site/contact');?>" >Contact</a><br/>
            &nbsp;<a href="<?= Url::to('/site/about');?>" >About</a><br/>
        </p>
    </div>
</footer>

<?php if (isset($this->blocks['bootstrap_image_gallery'])): ?>
    <?= $this->blocks['bootstrap_image_gallery'] ?>
<?php endif; ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
