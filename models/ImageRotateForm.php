<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ImageRotateForm is the model behind the contact form.
 */
class ImageRotateForm extends Model
{
    public $media_id;
    public $rotation_rate;

    const IMG_ROT_GRD_1 = 1;
    const IMG_ROT_GRD_2 = 2;
    const IMG_ROT_GRD_3 = 3;

    const IMG_CW90  = 1;
    const IMG_CW180 = 2;
    const IMG_CCW90 = 3;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['media_id'], 'required'],
            [['media_id','rotation_rate'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'media_id' => 'Media ID',
            'rotation_rate' => 'Rotation amount',
        ];
    }

}
