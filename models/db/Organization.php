<?php

namespace app\models\db;

use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "organization".
 *
 * @property integer $id
 * @property string $institution_name
 * @property string $short_label
 * @property string $department_name
 * @property integer $prog_admin_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_active
 * @property string $properties
 *
 * @property User $progAdmin
 */
class Organization extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_INSTITUTION_NAME = 'institution_name';
    const COL_SHORT_LABEL = 'short_label';
    const COL_DEPARTMENT_NAME = 'department_name';
    const COL_PROG_ADMIN_ID = 'prog_admin_id';
    const COL_CREATED_AT = 'created_at';
    const COL_UPDATED_AT = 'updated_at';
    const COL_IS_ACTIVE = 'is_active';
    const COL_PROPERTIES = 'properties';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['institution_name'], 'required'],
            [['prog_admin_id', 'is_active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['properties'], 'string'],
            [['institution_name', 'department_name'], 'string', 'max' => 125],
            [['short_label'], 'string', 'max' => 65],
            [['prog_admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['prog_admin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institution_name' => 'Institution Name',
            'short_label' => 'Short Label',
            'department_name' => 'Department Name',
            'prog_admin_id' => 'Prog Admin ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_active' => 'Is Active',
            'properties' => 'Properties',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgAdmin()
    {
        return $this->hasOne(User::className(), ['id' => 'prog_admin_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\OrganizationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\OrganizationQuery(get_called_class());
    }
}
