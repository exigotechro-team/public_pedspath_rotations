<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "user_groups".
 *
 * @property integer $id
 * @property string $parent_user_role
 * @property integer $parent_user_id
 * @property string $child_user_role
 * @property integer $child_user_id
 * @property string $created_at
 *
 * @property User $childUser
 */
class UserGroups extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_PARENT_USER_ROLE = 'parent_user_role';
    const COL_PARENT_USER_ID = 'parent_user_id';
    const COL_CHILD_USER_ROLE = 'child_user_role';
    const COL_CHILD_USER_ID = 'child_user_id';
    const COL_CREATED_AT = 'created_at';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_user_role', 'child_user_role'], 'string'],
            [['parent_user_id', 'child_user_id'], 'required'],
            [['parent_user_id', 'child_user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['child_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['child_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_user_role' => 'Parent User Role',
            'parent_user_id' => 'Parent User ID',
            'child_user_role' => 'Child User Role',
            'child_user_id' => 'Child User ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildUser()
    {
        return $this->hasOne(User::className(), ['id' => 'child_user_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\UserGroupsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\UserGroupsQuery(get_called_class());
    }
}
