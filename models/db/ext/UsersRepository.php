<?php

namespace app\models\db\ext;

use yii\db\ActiveQuery;

class UsersRepository
{

    /**
     * @param integer $org_id
     * @param integer $pgc_uid
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function GetTrainersForOrgList($org_id, $pgc_uid)
    {
        $sql = <<<SQLSTR
                SELECT
                    u.id,
                    CONCAT( -- IFNULL(p.name, '<name-missing>'), ' - ', 
                    u.username, ' [ ', u.email, ' ] ') as name 
                FROM user u
                    JOIN profile p ON p.user_id = u.id
                WHERE u.id IN (
                    SELECT
                        DISTINCT (ug.child_user_id)
                    -- , ug.*
                    FROM user_groups ug
                    WHERE ug.parent_user_role = :org_role -- 'ORG'
                        AND ug.parent_user_id =  :org_id -- 15
                        AND ug.child_user_role = :trainer_role -- 'TRAINER'
--                        AND ug.child_user_id != :pgc_uid -- 50

                );
SQLSTR;

        $query = \Yii::$app->getDb()->createCommand($sql, [
            ':org_role'  =>   UserGroups::PRNT_USR_ROLE_ORG,
            ':trainer_role'  =>   UserGroups::CHLD_USR_ROLE_TRAINER,
            ':org_id'    =>   $org_id,
#            ':pgc_uid'    =>   $pgc_uid,
        ]);

        return $query->queryAll();
    }

    public static function GetProgCoordForOrgList($org_id)
    {
        $sql = <<<SQLSTR
                SELECT
                    u.id,
                    CONCAT( -- IFNULL(p.name, '<name-missing>'), ' - ', 
                    u.username, ' [ ', u.email, ' ] ') as name 
                FROM user u
                    JOIN profile p ON p.user_id = u.id
                WHERE u.id IN (
                    SELECT
                        DISTINCT (ug.child_user_id)
                    -- , ug.*
                    FROM user_groups ug
                    WHERE parent_user_role = :org_role -- 'ORG'
        				AND ug.child_user_role = :pgc_role -- 'PROG_COORD'
                        AND ug.parent_user_id =  :org_id -- 15
                );
SQLSTR;

        $query = \Yii::$app->getDb()->createCommand($sql, [
            ':org_role'  =>   UserGroups::PRNT_USR_ROLE_ORG,
            ':org_id'    =>   $org_id,
            ':pgc_role'  =>   UserGroups::CHLD_USR_ROLE_PROG_COORD,
        ]);

        return $query->queryAll();
    }

    public static function GetTraineesForTrainerList($org_id, $trainer_uid)
    {
        $sql = <<<SQLSTR
                SELECT
                    u.id,
                    CONCAT( -- IFNULL(p.name, '<name-missing>'), ' - ', 
                    u.username, ' [ ', u.email, ' ] ') as name 
                FROM user u
                    JOIN profile p ON p.user_id = u.id
                WHERE u.id IN 
                (
                    SELECT ug2.child_user_id
                        FROM user_groups ug
                            JOIN user_groups ug2
                                ON ug.child_user_role = ug2.parent_user_role
                                     AND ug.child_user_id = ug2.parent_user_id
                                     AND ug.child_user_role = :trainer_role -- 'TRAINER'
                                     AND ug.child_user_id = :trainer_uid -- 55 
                        WHERE ug.parent_user_id = :org_id -- 16
                            AND ug.parent_user_role = :org_role -- 'ORG'
                );
SQLSTR;

        $query = \Yii::$app->getDb()->createCommand($sql, [
            ':org_role'  =>   UserGroups::PRNT_USR_ROLE_ORG,
            ':org_id'    =>   $org_id,
            ':trainer_role'  =>   UserGroups::CHLD_USR_ROLE_TRAINER,
            ':trainer_uid'  =>   $trainer_uid,
        ]);

        return $query->queryAll();
    }


}