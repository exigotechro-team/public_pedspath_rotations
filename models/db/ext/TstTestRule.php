<?php

namespace app\models\db\ext;

use app\models\TstTestRule as TstTestRuleDb;
use yii\helpers\VarDumper;

class TstTestRule extends TstTestRuleDb
{
    /**
     * LprLedger constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @inheritdoc
     */
    public function delete()
    {
        return parent::delete();
    }


    /**
     * @param int $rb_id
     * @return TstTestRule
     */
    public function clone($rb_id = null)
    {
        /** @var TstTestRule $testRule_new */
        $testRule_new = $this->copy($rb_id);

        return $testRule_new;
    }


    /**
     * @param int|null $rb_id
     * @return TstTestRule
     */
    private function copy($rb_id = null)
    {
        /** @var TstTestRule $testRule_new */
        $testRule_new = new TstTestRule();
        $testRule_new->rb_id = (empty($rb_id)) ? $this->rb_id : $rb_id;
        $testRule_new->ctag_id = $this->ctag_id;
        $testRule_new->q_count = $this->q_count;
        $testRule_new->edit_status = $this->edit_status;
        $testRule_new->access_status = $this->access_status;
        $testRule_new->difficulty = $this->difficulty;
        $testRule_new->cloned_from = $this->id;
        $testRule_new->save();

        return $testRule_new;
    }


}