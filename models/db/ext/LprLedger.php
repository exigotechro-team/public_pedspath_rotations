<?php

namespace app\models\db\ext;

use app\models\LprLedger as LprLedgerDb;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\helpers\VarDumper;


class LprLedger extends LprLedgerDb
{
    /**
     * LprLedger constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param integer $test_id
     * @param boolean $isLocked
     * @return LprLedger
     */
    public static function SetTestLockedStatus($test_id, $isLocked)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForTest($test_id, 'NODE')
            ->andWhere(['IN', self::COL_NODE_STATUS, ['LOCKED', 'UNLOCKED'],])->one();

        $lockedStatus = ($isLocked === true) ? 'LOCKED' : 'UNLOCKED';

        if(empty($ledgerEntry)){
            $ledgerEntry = new self();
            $ledgerEntry->obj_system = 'LprTEST';
            $ledgerEntry->node_type = 'TEST';
            $ledgerEntry->node_id = $test_id;
        }

        $ledgerEntry->node_status = $lockedStatus;
        $ledgerEntry->save();

        return $ledgerEntry;
    }

    /**
     * @param integer $test_id
     * @return LprLedger
     */
    public static function GetTestLockedStatus($test_id)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForTest($test_id, 'NODE')
            ->andWhere(['IN', self::COL_NODE_STATUS, ['LOCKED', 'UNLOCKED'],])->one();

        if (empty($ledgerEntry)) {
            $ledgerEntry = new self();
            $ledgerEntry->obj_system = 'LprTEST';
            $ledgerEntry->node_type = 'TEST';
            $ledgerEntry->node_id = $test_id;

            $ledgerEntry->node_status = 'UNLOCKED';
            $ledgerEntry->save();
        }

        return $ledgerEntry;
    }

    /**

     */
    public function isLprTestLocked()
    {
        $lockedStatus = ($this->node_status === 'LOCKED') ? true : false;
        return $lockedStatus;
    }

    /**
     * @param boolean $isLocked
     */
    public function setLockedStatusForTest($isLocked)
    {
        $lockedStatus = ($isLocked === true) ? 'LOCKED' : 'UNLOCKED';
        $this->node_status = $lockedStatus;
        $this->save();
    }

    // --------------------------------------------------------------------------------------------


    /**
     * @param integer $subnode_test_id
     * @param boolean $isCloned
     * @param null|integer $node_test_id
     * @return LprLedger|null
     */
    public static function SetTestClonedStatus($subnode_test_id, $isCloned, $node_test_id = null)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForClonedTest($subnode_test_id);

        if (empty($ledgerEntry) && $isCloned && !empty($node_test_id)) {
            // create new ledgerEntry

            $ledgerEntry = new self();
            $ledgerEntry->obj_system = 'LprTEST';
            $ledgerEntry->node_type = 'TEST';
            $ledgerEntry->node_id = $node_test_id;
            $ledgerEntry->node_status = 'CLONED';
            $ledgerEntry->subnode_type = 'TEST';
            $ledgerEntry->subnode_id = $subnode_test_id;
            $ledgerEntry->save();

        } elseif (!empty($ledgerEntry) && !$isCloned) {
            // delete ledgerEntry
            try {
                $ledgerEntry->delete();
            } catch (StaleObjectException $e) {
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
            $ledgerEntry = null;
        }

        return $ledgerEntry;
    }


    /**
     * @param integer $subnode_q_id
     * @param boolean $isCloned
     * @param null|integer $node_q_id
     * @return LprLedger|null
     */
    public static function SetClonedStatusForQuestion($subnode_q_id, $isCloned, $node_q_id = null)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForQuestion($subnode_q_id, 'SUBNODE')
            ->andWhere(['IN', self::COL_NODE_STATUS, ['CLONED'],])
            ->one();

        if (empty($ledgerEntry) && $isCloned && !empty($node_q_id)) {
            // create new ledgerEntry

            $ledgerEntry = new self();
            $ledgerEntry->obj_system = 'LprQUESTION';
            $ledgerEntry->node_type = 'QUESTION';
            $ledgerEntry->node_id = $node_q_id;
            $ledgerEntry->node_status = 'CLONED';
            $ledgerEntry->subnode_type = 'QUESTION';
            $ledgerEntry->subnode_id = $subnode_q_id;
            $ledgerEntry->save();

        } elseif (!empty($ledgerEntry) && !$isCloned) {
            // delete ledgerEntry
            try {
                $ledgerEntry->delete();
            } catch (StaleObjectException $e) {
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
            $ledgerEntry = null;
        }

        return $ledgerEntry;
    }

    /**
     * @param integer $test_id
     * @return string
     */
    public static function GetClonedStatusForTest($test_id)
    {
        /** @var string $ledgerEntryStatus */
        $ledgerEntryStatus = self::FindLedgerForClonedTest($test_id)->node_status;

        return $ledgerEntryStatus;
    }


    /**

     */
    public function isLprTestCloned()
    {
        $clonedStatus = ($this->node_status === 'CLONED') ? true : false;
        return $clonedStatus;
    }

    // --------------------------------------------------------------------------------------------

    /**
     * @param integer $test_id
     * @return LprLedger
     */
    public static function FindLedgerForClonedTest($test_id)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForTest($test_id, 'SUBNODE')
            ->andWhere(['IN', self::COL_NODE_STATUS, ['CLONED'],])
            ->one();

//        $ledgerEntry = self::FindLedgerForClonedItem('LprTEST', 'TEST', $test_id);

        return $ledgerEntry;
    }

    /**
     * @param integer $question_id
     * @return LprLedger
     */
    public static function FindLedgerForClonedQuestion($question_id)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForQuestion($question_id, 'SUBNODE')
            ->andWhere(['IN', self::COL_NODE_STATUS, ['CLONED'],])
            ->one();

//        $ledgerEntry = self::FindLedgerForClonedItem('LprQUESTION', 'QUESTION', $question_id);

        return $ledgerEntry;
    }

    /**
     * @param $obj_system
     * @param $item_type
     * @param integer $item_id
     * @return LprLedger
     */
    public static function FindLedgerForClonedItem($obj_system, $item_type, $item_id)
    {
        /** @var LprLedger $ledgerEntry */
        $ledgerEntry = self::FindLedgerForSubnodeItem($obj_system, $item_type, $item_id)
            ->andWhere(['IN', self::COL_NODE_STATUS, ['CLONED'],])
            ->one();

        return $ledgerEntry;
    }

    // --------------------------------------------------------------------------------------------

    /**
     * @param integer $test_id
     * @return ActiveQuery $this
     */
    public static function FindLedgerForTest($test_id, $obj_type = 'NODE')
    {
        if($obj_type === 'NODE'){
            return self::FindLedgerForNodeItem('LprTEST','TEST',$test_id );
        }elseif ($obj_type === 'SUBNODE'){
            return self::FindLedgerForSubnodeItem('LprTEST','TEST',$test_id );
        }

        return null;
    }

    /**
     * @param integer $question_id
     * @return ActiveQuery $this
     */
    public static function FindLedgerForQuestion($question_id, $obj_type = 'NODE')
    {
        if($obj_type === 'NODE'){
            return self::FindLedgerForNodeItem('LprQUESTION', 'QUESTION', $question_id);
        }elseif ($obj_type === 'SUBNODE'){
            return self::FindLedgerForSubnodeItem('LprQUESTION', 'QUESTION', $question_id);
        }

        return null;
    }

    // --------------------------------------------------------------------------------------------

    /**
     * @param $obj_system
     * @param $item_type
     * @param integer $item_id
     * @return ActiveQuery
     */
    public static function FindLedgerForSubnodeItem($obj_system, $item_type, $item_id)
    {
        /** @var ActiveQuery $ledgerEntry */
        $ledgerAQ = self::find()->where([
            self::COL_OBJ_SYSTEM => $obj_system,
            self::COL_SUBNODE_TYPE => $item_type,
            self::COL_SUBNODE_ID => $item_id,]);

        return $ledgerAQ;
    }

    /**
     * @param $obj_system
     * @param $item_type
     * @param integer $item_id
     * @return ActiveQuery
     */
    public static function FindLedgerForNodeItem($obj_system, $item_type, $item_id)
    {
        /** @var ActiveQuery $ledgerEntry */
        $ledgerAQ = self::find()->where([
            self::COL_OBJ_SYSTEM => $obj_system,
            self::COL_NODE_TYPE => $item_type,
            self::COL_NODE_ID => $item_id,]);

        return $ledgerAQ;
    }

    // --------------------------------------------------------------------------------------------

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = time();
        } elseif (!$this->isNewRecord && empty($this->created_at)) {
            $this->created_at = time();
        }

        return parent::beforeSave($insert);
    }

    public function getOrganization(){
        return $this->hasOne(Organization::className(), ['id' => 'subnode_id']);
    }

    public function getLprTest(){
        return $this->hasOne(LprTest::className(), ['id' => 'node_id']);
    }


}