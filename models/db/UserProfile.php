<?php

namespace app\models\db;

use app\models\form\TraineeProfileEntryForm;
use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id
 * @property string $owner_type
 * @property int $owner_id
 * @property string $prop_name
 * @property string $prop_val
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * UserProfile constructor.
     * @param TraineeProfileEntryForm|array $teProfileForm
     */
    public function __construct($teProfileForm=[])
    {
        parent::__construct();

        if(is_array($teProfileForm)){

            if(empty($teProfileForm)){
                return; }

            $this->owner_type = 'TRAINEE';
            $this->owner_id  = $teProfileForm['up_uid'];
            $this->prop_name = $teProfileForm['up_pname'];
            $this->prop_val  = $teProfileForm['up_pval'];
        }else{
            $this->owner_type = 'TRAINEE';
            $this->owner_id  = $teProfileForm->up_uid;
            $this->prop_name = $teProfileForm->up_pname;
            $this->prop_val  = $teProfileForm->up_pval;
        }

//        $this->save();
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'prop_name', 'prop_val'], 'required'],
            [['owner_id'], 'integer'],
            [['owner_type', 'prop_name'], 'string', 'max' => 32],
            [['prop_val'], 'string', 'max' => 56],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_type' => 'Owner Type',
            'owner_id' => 'Owner ID',
            'prop_name' => 'Prop Name',
            'prop_val' => 'Prop Val',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\UserProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\UserProfileQuery(get_called_class());
    }
}
