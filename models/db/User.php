<?php

namespace app\models\db;

use app\models\db\ext\UserGroups;
use dektrium\user\models\User as DektriumUser;

class User extends DektriumUser
{
    public static function GetAllUserForRole($rbac_role, $asArray = false)
    {
        $sql = <<<SQL_STR
            select a.item_name as role,
                a.user_id,
                u.username,
                u.email,
                IF(p.name IS NULL or p.name = '', u.username, p.name) as name
            from auth_assignment a
                join user u on u.id = a.user_id
                join profile p on p.user_id = u.id
                AND a.item_name = :rbac_role;
SQL_STR;

        if($asArray){
            return static::findBySql($sql, [':rbac_role' => $rbac_role])->asArray()->all();
        }else{
            return static::findBySql($sql, [':rbac_role' => $rbac_role])->all();
        }
    }


    /**
     * @param bool $asArray
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function GetUnassignedProgCoords($asArray = false)
    {
        $sql = <<<SQL_STR
            SELECT
                ug.child_user_id                                      AS user_id,
                u.username,
                u.email,
                IF(p.name IS NULL OR p.name = '', u.username, p.name) AS name
            FROM user_groups ug
                JOIN user u ON ug.child_user_id = u.id
                JOIN profile p ON p.user_id = u.id
            WHERE ug.parent_user_role =  :prnt_role                             -- 'ADMIN'
                        AND ug.child_user_role =  :chld_role                    -- 'PROG_COORD'
                        AND ug.child_user_id NOT IN (
                            SELECT
                                ug2.child_user_id
                            FROM user_groups ug2
                            WHERE ug2.parent_user_role = :prnt2_role            -- 'ORG'
                                        AND ug2.child_user_role =  :chld_role2  -- 'PROG_COORD'
                        );
SQL_STR;

        $query = static::findBySql($sql, [
            ':prnt_role'  =>   UserGroups::PRNT_USR_ROLE_ADMIN,
            ':chld_role'  =>   UserGroups::CHLD_USR_ROLE_PROG_COORD,
            ':prnt2_role' =>   UserGroups::PRNT_USR_ROLE_ORG,
            ':chld_role2' =>   UserGroups::CHLD_USR_ROLE_PROG_COORD,
        ]);

        return ($asArray) ? $query->asArray()->all() : $query->all();
    }


    /**
     * @param integer $org_id
     * @param bool $asArray
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function GetProgCoordForOrganization($org_id, $asArray = false)
    {
        $sql = <<<SQL_STR
            SELECT
                ug.child_user_id                                      AS user_id,
                u.username,
                u.email,
                IF(p.name IS NULL OR p.name = '', u.username, p.name) AS name
            FROM user_groups ug
                JOIN user u ON ug.child_user_id = u.id
                JOIN profile p ON p.user_id = u.id
            WHERE ug.parent_user_role =  :prnt_role                             -- 'ORG'
                        AND ug.child_user_role =  :chld_role                    -- 'PROG_COORD'
                        AND ug.parent_user_id = :org_id ;
SQL_STR;

        $query = static::findBySql($sql, [
            ':prnt_role'  =>   UserGroups::PRNT_USR_ROLE_ORG,
            ':chld_role'  =>   UserGroups::CHLD_USR_ROLE_PROG_COORD,
            ':org_id' =>   $org_id,
        ]);

        return ($asArray) ? $query->asArray()->all() : $query->all();
    }



}