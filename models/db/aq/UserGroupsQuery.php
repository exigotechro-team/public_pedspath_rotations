<?php

namespace app\models\db\aq;

/**
 * This is the ActiveQuery class for [[\app\models\db\UserGroups]].
 *
 * @see \app\models\db\UserGroups
 */
class UserGroupsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\db\UserGroups[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\UserGroups|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
