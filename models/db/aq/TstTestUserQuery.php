<?php

namespace app\models\db\aq;

/**
 * This is the ActiveQuery class for [[\app\models\TstTestUser]].
 *
 * @see \app\models\TstTestUser
 */
class TstTestUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\TstTestUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\TstTestUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
