<?php

namespace app\models\db;

use app\ro\exigotech\lpr\GmUtils;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "tst_user_consent".
 *
 * @property int $id
 * @property int $user_id
 * @property int $consent
 * @property string $consented_on
 *
 * @property User $user
 */
class TstUserConsent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_user_consent';
    }

    /**
     * @param int $te_uid
     * @param int $consent
     * @param bool $override
     */
    public static function SetUserConsent($te_uid, $consent, $override = false)
    {
        $consent_bool = (intval($consent) == 1 ) ? true : false;

        $curr_datetime = GmUtils::GetCurrentDatetime();

        /** @var TstUserConsent $user_consent */
        $user_consent = TstUserConsent::find()->where(['user_id' => $te_uid])->one();

        if (!empty($user_consent) && $override == true) {
            $user_consent->consent = $consent_bool;
            $user_consent->consented_on = $curr_datetime;
            $user_consent->save(false);
        } elseif (empty($user_consent)) {
            $user_consent = new TstUserConsent();
            $user_consent->user_id = $te_uid;
            $user_consent->consent = $consent_bool;
            $user_consent->consented_on = $curr_datetime;
            $user_consent->save(false);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['consented_on'], 'safe'],
            [['consent'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'consent' => 'Consent',
            'consented_on' => 'Consented On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\TstUserConsentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\TstUserConsentQuery(get_called_class());
    }
}
