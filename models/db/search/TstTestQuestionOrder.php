<?php

namespace app\models\db\search;

use app\models\TstTestQuestion;

/**
 * This is the model class for table "tst_test_question".
 *
 * @property string $title
 * @property string $description
 *
 */
class TstTestQuestionOrder extends TstTestQuestion
{
    const COL_TITLE = 'title';
    const COL_DESCRIPTION = 'description';

    public $title;
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title', 'description'], 'string'],
//            [['title', 'description'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'title' => 'Question Title',
                'description' => 'Question Description',
            ]
        );
    }

}