<?php

namespace app\models\db\search;

use app\models\db\ext\TraineeUser;
use app\models\db\ext\TrainerUser;
use app\models\db\ext\UserGroups;
use dektrium\user\Finder;
use dektrium\user\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;


/**
 * TraineeUserSearch represents the model behind the search form about User.
 */

class TraineeUserSearch extends Model
{
    /** @var string */
    public $username;

    /** @var string */
    public $email;

    /** @var int */
    public $created_at;

    /** @var int */
    public $last_login_at;

    /** @var string */
    public $registration_ip;

    /** @var Finder */
    protected $finder;

    /**
     * @param Finder $finder
     * @param array  $config
     */
    public function __construct(Finder $finder = null, $config = [])
    {
        if(empty($finder)){
            $finder = new Finder(); }

        $this->finder = $finder;
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            'fieldsSafe' => [['username', 'email', 'registration_ip', 'created_at', 'last_login_at'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null],
            'lastloginDefault' => ['last_login_at', 'default', 'value' => null],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username'        => Yii::t('user', 'Username'),
            'email'           => Yii::t('user', 'Email'),
            'created_at'      => Yii::t('user', 'Registration time'),
            'last_login_at'   => Yii::t('user', 'Last login'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }


    /**
     * @param array $params
     * @param int $tr_uid
     * @param array $uids
     * @param bool $hasSearchParams
     * @return ActiveDataProvider
     */
    public function searchUnderTrainer($params, $tr_uid, $uids = [], $hasSearchParams = false)
    {
        $tr_uid = intval($tr_uid);

        /** @var TrainerUser $trainer */
        $trainer = TrainerUser::find()->where(['id'=>$tr_uid])->one();

        /** @var ActiveQuery $query */
        $query = $trainer->getTrainees();

        if($hasSearchParams || !empty($uids)){
            if(empty($uids)){
                $uids = [-1, -2]; }

            $query->andFilterWhere(['IN', 'id', $uids]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query, // $query,
            'totalCount' => $query->count(),
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);


        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

    public function searchTraineesForTrainer($trainer_uid)
    {
        $trainer_uid = intval($trainer_uid);

        $sql = <<<SQL
            SELECT u.*
            FROM user u
            WHERE u.id IN (
                SELECT ug.child_user_id AS id
                FROM user_groups ug
                WHERE ug.parent_user_role = :trainer_role-- 'TRAINER'
                    AND ug.parent_user_id = :trainer_uid
                    AND ug.child_user_role = :trainee_role -- 'TRAINEE'
            );
SQL;

        $query = TraineeUser::findBySql($sql, [
            ':trainer_role' => UserGroups::PRNT_USR_ROLE_TRAINER,
            ':trainer_uid' => $trainer_uid,
            ':trainee_role' => UserGroups::CHLD_USR_ROLE_TRAINEE,
        ]);

        /** @var User[] $trainees */
        $trainees = $query->all();

        return $trainees;
    }


    /**
     * @param $trainee_uid
     * @param $trainer_uid
     * @return User
     */
    public function getTraineeForTrainer($trainee_uid, $trainer_uid)
    {
        $trainee_uid = intval($trainee_uid);
        $trainer_uid = intval($trainer_uid);

        $sql = <<<SQL
            SELECT u.*
            FROM user u
            WHERE u.id IN (
                SELECT ug.child_user_id AS id
                FROM user_groups ug
                WHERE ug.parent_user_role = :trainer_role-- 'TRAINER'
                            AND ug.parent_user_id = :trainer_uid
                            AND ug.child_user_role = :trainee_role -- 'TRAINEE'
                            AND ug.child_user_id = :trainee_id
            );
SQL;

        $query = TraineeUser::findBySql($sql, [
            ':trainer_role' => UserGroups::PRNT_USR_ROLE_TRAINER,
            ':trainer_uid' => $trainer_uid,
            ':trainee_role' => UserGroups::CHLD_USR_ROLE_TRAINEE,
            ':trainee_id' => $trainee_uid,
        ]);

        /** @var User $trainee */
        $trainee = $query->one();

        return $trainee;
    }

    public function checkTraineeExistsForTrainer($trainee_id, $trainer_id)
    {
        $trainee = $this->getTraineeForTrainer($trainee_id, $trainer_id);

        if(empty($trainee)){
            return 0;
        }

        return 1;
    }


}