<?php

namespace app\models\db\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TstUserAnswer;

/**
 * TstUserAnswerSearch represents the model behind the search form about `app\models\TstUserAnswer`.
 */
class TstUserAnswerSearch extends TstUserAnswer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ttu_id', 'question_id', 'question_order', 'answer_id', 'is_correct'], 'integer'],
            [['answer_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TstUserAnswer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ttu_id' => $this->ttu_id,
            'question_id' => $this->question_id,
            'question_order' => $this->question_order,
            'answer_id' => $this->answer_id,
            'answer_time' => $this->answer_time,
            'is_correct' => $this->is_correct,
        ]);

        return $dataProvider;
    }
}
