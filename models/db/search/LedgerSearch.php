<?php

namespace app\models\db\search;

use app\models\db\ext\LprTest;
use app\models\db\ext\Organization;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\ext\LprLedger;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * LedgerSearch represents the model behind the search form of `app\models\db\ext\LprLedger`.
 */
class LedgerSearch extends LprLedger
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'node_id', 'subnode_id', 'created_at'], 'integer'],
            [['obj_system', 'node_type', 'node_status', 'subnode_type', 'args', 'notes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LprLedger::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'node_id' => $this->node_id,
            'subnode_id' => $this->subnode_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'obj_system', $this->obj_system])
            ->andFilterWhere(['like', 'node_type', $this->node_type])
            ->andFilterWhere(['like', 'node_status', $this->node_status])
            ->andFilterWhere(['like', 'subnode_type', $this->subnode_type])
            ->andFilterWhere(['like', 'args', $this->args])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchOrgAssignedTests($params)
    {
        $query = LprLedger::find()->where([
            LprLedger::COL_OBJ_SYSTEM => 'LprTEST',
            LprLedger::COL_NODE_TYPE => 'TEST',
            LprLedger::COL_NODE_STATUS => 'TEST_ASSIGN',
            LprLedger::COL_SUBNODE_TYPE => 'ORG',
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'node_id' => $this->node_id,
            'subnode_id' => $this->subnode_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'obj_system', $this->obj_system])
            ->andFilterWhere(['like', 'node_type', $this->node_type])
            ->andFilterWhere(['like', 'node_status', $this->node_status])
            ->andFilterWhere(['like', 'subnode_type', $this->subnode_type])
//            ->andFilterWhere(['like', 'args', $this->args])
//            ->andFilterWhere(['like', 'notes', $this->notes])
            ;

        if (!true) {
            print "\n<pre>" . $query->createCommand()->getRawSql() . "</pre>\n";
            exit;
        }


        return $dataProvider;
    }

    public function checkTestIsAssignedToOrg($tid, $org_id)
    {

        $lprLedger = LprLedger::find()->where([
            LprLedger::COL_OBJ_SYSTEM => 'LprTEST',
            LprLedger::COL_NODE_TYPE => 'TEST',
            LprLedger::COL_NODE_ID => $tid,
            LprLedger::COL_NODE_STATUS => 'TEST_ASSIGN',
            LprLedger::COL_SUBNODE_TYPE => 'ORG',
            LprLedger::COL_SUBNODE_ID => $org_id,
        ])->one();

        if(empty($lprLedger)){
            return 0;
        }

        return 1;
    }

    /**
     * @param $org_id
     * @return LprTest|array
     */
    public function searchTestsForOrg($org_id)
    {

        $lprLedgers = LprLedger::find()
            ->select([LprLedger::COL_NODE_ID])
            ->where([
            LprLedger::COL_OBJ_SYSTEM => 'LprTEST',
            LprLedger::COL_NODE_TYPE => 'TEST',
//            LprLedger::COL_NODE_ID => $tid,
            LprLedger::COL_NODE_STATUS => 'TEST_ASSIGN',
            LprLedger::COL_SUBNODE_TYPE => 'ORG',
            LprLedger::COL_SUBNODE_ID => $org_id,
        ])->asArray()->all();

        if(empty($lprLedgers)){
            return [];
        }

        $tids = ArrayHelper::getColumn($lprLedgers , LprLedger::COL_NODE_ID);

        /** @var LprTest $lprTests */
        $lprTests = LprTest::find()
            ->where(['IN', 'id', $tids])
//            ->asArray()
            ->all();

        if(empty($lprTests)){
            return [];
        }

        return $lprTests;
    }


}
