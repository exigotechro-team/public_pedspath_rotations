<?php

namespace app\models\db\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TstTestUser;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * TstTestUserSearch represents the model behind the search form about `app\models\TstTestUser`.
 *
 * @property string $user_name
 * @property string $test_title
 * @property string $no_of_qs
 * @property string $test_duration
 *
 */
class TstTestUserSearch extends TstTestUser
{
    public $user_name;
    public $test_title;
    public $no_of_qs;
    public $test_duration;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'test_id', 'user_id', 'final_score', 'max_score'], 'integer'],
            [['created_on', 'started_on', 'ended_on', 'user_name', 'test_title', 'no_of_qs', 'test_duration'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'user_name' => 'User Name',
                'test_title' => 'Test Title',
                'no_of_qs' => 'Number of questions',
                'test_duration' => 'Test Duration',
            ]
        );
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TstTestUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'test_id' => $this->test_id,
            'user_id' => $this->user_id,
            'created_on' => $this->created_on,
            'started_on' => $this->started_on,
            'ended_on' => $this->ended_on,
            'final_score' => $this->final_score,
            'max_score' => $this->max_score,
        ]);

        return $dataProvider;
    }



    /**
     * @param $trainer_id
     * @return ActiveDataProvider
     */
    public function searchAssignedTestsByTrainer($trainer_id){

        $query = $this->getAssignedTestsByTrainerQuery($trainer_id);
        $totalcount = $this->getAssignedTestsByTrainerTotalCount($trainer_id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $totalcount,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }


    /**
     * @param $trainer_id
     * @return Query
     */
    public function getAssignedTestsByTrainerQuery($trainer_id)
    {

        $sql = <<<SQL_STR
            SELECT
              CASE
              WHEN p.name IS NULL
                THEN '-'
              ELSE p.name
              END
                AS user_name,
              lt.title AS test_title,
              tu.*
            FROM tst_test_user tu
              JOIN profile p ON tu.user_id = p.user_id
              JOIN lpr_test lt ON tu.test_id = lt.id
            WHERE tu.user_id IN (
              SELECT ug.child_user_id
              FROM user_groups ug
              WHERE ug.parent_user_id = :trainer_id
                    AND ug.parent_user_role = 'TRAINER'
                    AND ug.child_user_role = 'TRAINEE'
            )
            ORDER BY tu.user_id, tu.test_id;
SQL_STR;

        /** @var Query $tuQuery */
        $tuQuery = TstTestUser::findBySql($sql, [':trainer_id' => $trainer_id]);

        return $tuQuery;
    }


    /**
     * @param $trainer_id
     * @return false|null|string
     */
    public function getAssignedTestsByTrainerTotalCount($trainer_id){

        $sql = <<<SQL_STR
            SELECT count(tu.id) as totalcount
            FROM tst_test_user tu
            WHERE tu.user_id IN (
              SELECT ug.child_user_id
              FROM user_groups ug
              WHERE ug.parent_user_id = :trainer_id
                    AND ug.parent_user_role = 'TRAINER'
                    AND ug.child_user_role = 'TRAINEE'
            )
            ORDER BY tu.user_id, tu.test_id;
SQL_STR;

        try {
            $totalcount = \Yii::$app->getDb()->createCommand($sql, [':trainer_id' => $trainer_id])->queryScalar();
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
        }

        return $totalcount;
    }


// -----------------------------------------------------------------------------

    /**
     * @param $trainee_id
     * @return ActiveDataProvider
     */
    public function searchAssignedTestsToTrainee($trainee_id){

        $query = $this->getAssignedTestsToTraineeQuery($trainee_id);
        $totalcount = $this->getAssignedTestsToTraineeTotalCount($trainee_id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $totalcount,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @param null|integer $trainee_id
     * @param null $tu_id
     * @return Query
     */
    public function getAssignedTestsToTraineeQuery($trainee_id, $tu_id = null)
    {
        $sql_tmpl = <<<SQL_STR
            SELECT
              CASE
              WHEN p.name IS NULL
                THEN '-'
              ELSE p.name
              END
                AS user_name,
              lt.title AS test_title,
             (select count(ttq.id) from tst_test_question ttq where ttq.test_id = lt.id) as no_of_qs,
              lt.duration_sec as test_duration,
              tu.*
            FROM tst_test_user tu
              JOIN profile p ON tu.user_id = p.user_id
              JOIN lpr_test lt ON tu.test_id = lt.id
            WHERE tu.user_id = :trainee_id
              %s
            ORDER BY tu.user_id, tu.test_id;
SQL_STR;

        $tu_id_str = empty($tu_id) ? '' : 'AND tu.id = :tu_id';
        $sql = sprintf($sql_tmpl, $tu_id_str);

        $params = [':trainee_id' => $trainee_id];
        if(!empty($tu_id)){
            $params[':tu_id'] = $tu_id; }

        /** @var Query $tuQuery */
        $tuQuery = TstTestUserSearch::findBySql($sql, $params);

        return $tuQuery;
    }


    /**
     * @param $trainee_id
     * @return false|null|string
     */
    public function getAssignedTestsToTraineeTotalCount($trainee_id){

        $sql = <<<SQL_STR
            SELECT count(tu.id) as totalcount
            FROM tst_test_user tu
            WHERE tu.user_id = :trainee_id
            ORDER BY tu.user_id, tu.test_id;
SQL_STR;

        try {
            $totalcount = \Yii::$app->getDb()->createCommand($sql, [':trainee_id' => $trainee_id])->queryScalar();
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
        }

        return $totalcount;
    }

    /**
     * @param int $utid
     * @return ArrayDataProvider
     */
    public function getStatsForUserTest($utid)
    {
        $sql = <<<SQL_STR
            SELECT -- tua.*, q.title, l.label
              l.label, count(tua.id) as no_of_q, sum(tua.is_correct) as correct
            FROM tst_user_answer tua
            -- join tst_test_user ttu ON tua.ttu_id = ttu.id
              join lpr_question q ON tua.question_id = q.id
              join tst_ctag_question tcq ON q.id = tcq.question_id
              join lpr_ctag l ON tcq.category_id = l.id
            WHERE tua.ttu_id = :utid
            --    and ttu.user_id = :uid
            GROUP BY l.label;
SQL_STR;

        $user_test_stats = [];

        try {
            $user_test_stats = \Yii::$app->getDb()->createCommand($sql, [':utid' => $utid])->queryAll();
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $user_test_stats,
            'pagination' => [
                'pageSize' => 30,
            ],
            'sort' => [
                'attributes' => ['label'],
            ],
        ]);

        return $dataProvider;
    }


}