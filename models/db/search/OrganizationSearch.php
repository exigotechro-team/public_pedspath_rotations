<?php

namespace app\models\db\search;

use app\models\db\ext\UserGroupsRepository;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\Organization;

/**
 * OrganizationSearch represents the model behind the search form about `app\models\db\Organization`.
 */
class OrganizationSearch extends Organization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prog_admin_id', 'is_active'], 'integer'],
            [['institution_name', 'short_label', 'department_name', 'created_at', 'updated_at', 'properties'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Organization::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prog_admin_id' => $this->prog_admin_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'institution_name', $this->institution_name])
            ->andFilterWhere(['like', 'short_label', $this->short_label])
            ->andFilterWhere(['like', 'department_name', $this->department_name])
            ->andFilterWhere(['like', 'properties', $this->properties]);

        return $dataProvider;
    }


    public function searchByProgAdmin($params, $prog_admin_id)
    {
        $query = UserGroupsRepository::GetAllOrgsForProgCoordQuery($prog_admin_id);
        $totalCount = count($query->all());

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $totalCount,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
//            'prog_admin_id' => $this->prog_admin_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'institution_name', $this->institution_name])
            ->andFilterWhere(['like', 'department_name', $this->department_name])
            ->andFilterWhere(['like', 'properties', $this->properties]);

        return $dataProvider;
    }




}
