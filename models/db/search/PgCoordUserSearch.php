<?php

namespace app\models\db\search;

use app\models\db\ext\ProgramCoordinatorUser;
use app\models\db\ext\TrainerUser;
use app\models\db\ext\UserGroups;
use app\models\db\ext\UserGroupsRepository;
use dektrium\user\Finder;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\helpers\VarDumper;


/**
 * ProgramCoordinatorUserSearch represents the model behind the search form about User.
 */

class PgCoordUserSearch extends Model
{
    /** @var string */
    public $username;

    /** @var string */
    public $email;

    /** @var int */
    public $created_at;

    /** @var int */
    public $last_login_at;

    /** @var string */
    public $registration_ip;

    /** @var Finder */
    protected $finder;

    /**
     * @param Finder $finder
     * @param array  $config
     */
    public function __construct(Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            'fieldsSafe' => [['username', 'email', 'registration_ip', 'created_at', 'last_login_at'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null],
            'lastloginDefault' => ['last_login_at', 'default', 'value' => null],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'username'        => Yii::t('user', 'Username'),
            'email'           => Yii::t('user', 'Email'),
            'created_at'      => Yii::t('user', 'Registration time'),
            'last_login_at'   => Yii::t('user', 'Last login'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }



    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function searchAllProgramCoordinators($params)
    {
        $sql = <<<SQL
            SELECT u.*
            FROM user u
            WHERE u.id IN (
                SELECT ug.child_user_id AS id
                FROM user_groups ug
                WHERE ug.parent_user_role = :admin_role
                  AND ug.parent_user_id   = :admin_uid
                  AND ug.child_user_role  = :pgc_role
            );
SQL;

        $query = ProgramCoordinatorUser::findBySql($sql, [
            ':admin_role' => UserGroups::PRNT_USR_ROLE_ADMIN,
            ':admin_uid' => \Yii::$app->user->id,
            ':pgc_role' => UserGroups::CHLD_USR_ROLE_PROG_COORD
        ]);

        $totalCount = count($query->all());

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }


    /**
     * @param $params
     * @param integer $org_id
     * @return ActiveDataProvider
     */
    public function searchTrainersUnderProgramCoord($params, $org_id)
    {
        $org_id = intval($org_id);

        $sql = <<<SQL
            SELECT u.*
            FROM user u
            WHERE u.id IN (
                SELECT ug.child_user_id
                --	, ug.*
                FROM user_groups ug
                WHERE 1 = 1
                            AND ug.parent_user_role = :org_role
                            AND ug.parent_user_id   = :org_id
                            AND ug.child_user_role  = :trainer_role  
            );
SQL;

        $query = TrainerUser::findBySql($sql, [
            ':org_role' => UserGroups::PRNT_USR_ROLE_ORG,
            ':org_id' => $org_id,
            ':trainer_role' => UserGroups::CHLD_USR_ROLE_TRAINER,
        ]);
        $totalCount = count($query->all());

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['registration_ip' => $this->registration_ip]);

        return $dataProvider;
    }





}