<?php

namespace app\models\db\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\UserProfile;
use yii\helpers\VarDumper;

/**
 * UserProfileSearch represents the model behind the search form of `app\models\db\UserProfile`.
 */
class UserProfileSearch extends UserProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'owner_id'], 'integer'],
            [['owner_type', 'prop_name', 'prop_val'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'owner_id' => $this->owner_id,
        ]);

        $query->andFilterWhere(['like', 'owner_type', $this->owner_type])
            ->andFilterWhere(['like', 'prop_name', $this->prop_name])
            ->andFilterWhere(['like', 'prop_val', $this->prop_val]);

        return $dataProvider;
    }

    /**
     * @param array $filter_params
     * @return \yii\db\ActiveQuery
     */
    public function findOwnersForProps($filter_params)
    {
        $sql_tmpl_pgy = <<<SQL_STR_PGY
              and up1.prop_val = :te_pgy
SQL_STR_PGY;

        $sql_tmpl_inst = <<<SQL_STR_INST
              and up2.prop_val = :te_inst
SQL_STR_INST;

        $sql_tmpl_ttype = <<<SQL_STR_TTYPE
              and up3.prop_val = :te_type
SQL_STR_TTYPE;

        $sql_chunk = "";
        $sql_params = [
            ':tr_uid' => $filter_params['tr_uid']
        ];

        if(!empty($filter_params['te_pgy'])){
            $sql_chunk .=  $sql_tmpl_pgy;
            $sql_params[':te_pgy'] = $filter_params['te_pgy'];
        }

        if(!empty($filter_params['te_inst'])){
            $sql_chunk .=  $sql_tmpl_inst;
            $sql_params[':te_inst'] = $filter_params['te_inst'];
        }

        if(!empty($filter_params['te_type'])){
            $sql_chunk .=  $sql_tmpl_ttype;
            $sql_params[':te_type'] = $filter_params['te_type'];
        }

        $sql_tmpl = <<<SQL_STR
            select ug.child_user_id as owner_id
            from user_groups ug
              join user_profile up1 on ug.child_user_id = up1.owner_id
                  AND ug.parent_user_id = :tr_uid
                  AND ug.child_user_role = 'TRAINEE'
                  AND ug.parent_user_role = 'TRAINER'
              join user_profile up2 on up2.owner_id = up1.owner_id
              join user_profile up3 on up3.owner_id = up2.owner_id
            where 1=1
--              AND ug.parent_user_id = :tr_id
--              AND ug.child_user_role = 'TRAINEE'
--              AND ug.parent_user_role = 'TRAINER'
              and up1.prop_name = 'te_pgy'
              and up2.prop_name = 'te_inst'
              and up3.prop_name = 'te_type'
              %s
            ;
SQL_STR;

        $sql = sprintf($sql_tmpl, $sql_chunk);

        return UserProfile::findBySql($sql, $sql_params);
    }

}