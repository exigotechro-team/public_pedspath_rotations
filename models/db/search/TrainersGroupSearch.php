<?php

namespace app\models\db\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\ext\TrainersGroup;

/**
 * TrainersGroupSearch represents the model behind the search form about `app\models\db\ext\Trainers`.
 */
class TrainersGroupSearch extends TrainersGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_user_id', 'child_user_id'], 'integer'],
            [['parent_user_role', 'child_user_role', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainersGroup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_user_id' => $this->parent_user_id,
            'child_user_id' => $this->child_user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'parent_user_role', $this->parent_user_role])
            ->andFilterWhere(['like', 'child_user_role', $this->child_user_role]);

        return $dataProvider;
    }

    public function searchUnderProgAdmin($params)
    {
        $prog_admin_id = Yii::$app->user->id;
        $query = TrainersGroup::find()->where(['parent_user_id' => $prog_admin_id ]);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_user_id' => $this->parent_user_id,
            'child_user_id' => $this->child_user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'parent_user_role', $this->parent_user_role])
            ->andFilterWhere(['like', 'child_user_role', $this->child_user_role]);

        return $dataProvider;
    }


}
