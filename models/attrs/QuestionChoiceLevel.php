<?php

namespace app\models\attrs;

use app\models\utils\BaseEnum;

class QuestionChoiceLevel extends BaseEnum
{
    const QCL_SINGLE_CHOICE    = 1;
    const SINGLE_CHOICE_LBL    = 'SINGLE CHOICE';
    const QCL_TRUE_FALSE       = 2;
    const TRUE_FALSE_LBL       = 'TRUE / FALSE';
    const QCL_MULTIPLE_CHOICE  = 3;
    const MULTIPLE_CHOICE_LBL  = 'MULTIPLE CHOICE';

    public static function getList()
    {
        return [
            self::SINGLE_CHOICE_LBL   => \Yii::t('app', 'single choice'),
            self::TRUE_FALSE_LBL      => \Yii::t('app', 'true / false'),
            self::MULTIPLE_CHOICE_LBL => \Yii::t('app', 'multiple choice'),
        ];
    }

    public static function getDropDownList()
    {
        return [
            self::QCL_SINGLE_CHOICE   => self::SINGLE_CHOICE_LBL,
            self::QCL_TRUE_FALSE      => self::TRUE_FALSE_LBL,
            self::QCL_MULTIPLE_CHOICE => self::MULTIPLE_CHOICE_LBL,
        ];
    }

    /**
     * @param int $ies_id
     * @return bool
     */
    public static function isValidItem($ies_id){
        return in_array($ies_id, [
            self::QCL_SINGLE_CHOICE,
            self::QCL_TRUE_FALSE,
            self::QCL_MULTIPLE_CHOICE,
        ]);
    }

    /**
     * @param int|string $ies_id
     * @return string
     */
    public static function getLabel($ies_id)
    {
        switch ($ies_id) {
            case (self::QCL_SINGLE_CHOICE):
                return self::SINGLE_CHOICE_LBL;
            case (self::QCL_TRUE_FALSE):
                return self::TRUE_FALSE_LBL;
            case (self::QCL_MULTIPLE_CHOICE):
                return self::MULTIPLE_CHOICE_LBL;
            default:
                return '';
        }
    }
}