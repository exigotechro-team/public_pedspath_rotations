<?php

namespace app\models\attrs;

use app\models\utils\BaseEnum;

class ItemEditStatus extends BaseEnum
{

    const IES_PUBLISHED = 1;
    const PUBLISHED_LBL = 'PUBLISHED';
    const IES_DRAFT     = 2;
    const DRAFT_LBL     = 'DRAFT';
    const IES_REVIEW    = 3;
    const REVIEW_LBL    = 'REVIEW';


    public static function getList()
    {
        return [
            self::PUBLISHED_LBL   => \Yii::t('app', 'published'),
            self::DRAFT_LBL       => \Yii::t('app', 'draft'),
            self::REVIEW_LBL      => \Yii::t('app', 'review'),
        ];
    }


    public static function getDropDownList()
    {
        return [
            self::IES_PUBLISHED   => self::PUBLISHED_LBL,
            self::IES_DRAFT       => self::DRAFT_LBL,
            self::IES_REVIEW      => self::REVIEW_LBL,
        ];
    }


    /**
     * @param int $ies_id
     * @return bool
     */
    public static function isValidItem($ies_id){
        return in_array($ies_id, [
            self::IES_PUBLISHED,
            self::IES_DRAFT,
            self::IES_REVIEW,
        ]);
    }



    /**
     * @param int|string $ies_id
     * @return string
     */
    public static function getLabel($ies_id)
    {
        switch ($ies_id) {
            case (self::IES_PUBLISHED):
                return self::PUBLISHED_LBL;
            case (self::IES_DRAFT):
                return self::DRAFT_LBL;
            case (self::IES_REVIEW):
                return self::REVIEW_LBL;
            default:
                return '';
        }
    }
}