<?php

namespace app\models\attrs;

use app\models\utils\BaseEnum;

class ExamStartStatus extends BaseEnum
{

    const START_NOT_SET             = 0;
    const PRIOR_TO_START_TIME       = 1;
    const DURING_START_END_TIME     = 2;
    const POST_END_TIME             = 3;

    const START_NOT_SET_LBL         = 'Exam date NOT set';
    const PRIOR_TO_START_TIME_LBL   = 'Current time BEFORE exam date';
    const DURING_START_END_TIME_LBL = 'Current time DURING exam date';
    const POST_END_TIME_LBL         = 'Current time AFTER exam date';


    public static function getList()
    {
        return [
            self::START_NOT_SET          => self::START_NOT_SET_LBL        ,
            self::PRIOR_TO_START_TIME    => self::PRIOR_TO_START_TIME_LBL  ,
            self::DURING_START_END_TIME  => self::DURING_START_END_TIME_LBL,
            self::POST_END_TIME          => self::POST_END_TIME_LBL        ,
        ];
    }

    public static function getDropDownList()
    {
        return [
            self::START_NOT_SET          => self::START_NOT_SET_LBL        ,
            self::PRIOR_TO_START_TIME    => self::PRIOR_TO_START_TIME_LBL  ,
            self::DURING_START_END_TIME  => self::DURING_START_END_TIME_LBL,
            self::POST_END_TIME          => self::POST_END_TIME_LBL        ,
        ];
    }

    /**
     * @param int $ies_id
     * @return bool
     */
    public static function isValidItem($ies_id){
        return in_array($ies_id, [
            self::START_NOT_SET          ,
            self::PRIOR_TO_START_TIME    ,
            self::DURING_START_END_TIME  ,
            self::POST_END_TIME          ,
        ]);
    }

    /**
     * @param int|string $ies_id
     * @return string
     */
    public static function getLabel($ies_id)
    {
        switch ($ies_id) {
            case (self::START_NOT_SET):
                return self::START_NOT_SET_LBL;
            case (self::PRIOR_TO_START_TIME):
                return self::PRIOR_TO_START_TIME_LBL;
            case (self::DURING_START_END_TIME):
                return self::DURING_START_END_TIME_LBL;
            case (self::POST_END_TIME):
                return self::POST_END_TIME_LBL;
            default:
                return '';
        }
    }
}