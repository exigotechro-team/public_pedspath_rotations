<?php

namespace app\models\attrs;

use app\models\utils\BaseEnum;

class ItemDifficultyLevel extends BaseEnum
{

    const IDL_EASY       = 1;
    const EASY_LBL       = 'EASY';
    const IDL_MEDIUM     = 2;
    const MEDIUM_LBL     = 'MEDIUM';
    const IDL_DIFFICULT  = 3;
    const DIFFICULT_LBL  = 'DIFFICULT';

    public static function getList()
    {
        return [
            self::EASY_LBL      => \Yii::t('app', 'easy'),
            self::MEDIUM_LBL    => \Yii::t('app', 'medium'),
            self::DIFFICULT_LBL => \Yii::t('app', 'difficult'),
        ];
    }

    public static function getDropDownList()
    {
        return [
            self::IDL_EASY      => self::EASY_LBL,
            self::IDL_MEDIUM    => self::MEDIUM_LBL,
            self::IDL_DIFFICULT => self::DIFFICULT_LBL,
        ];
    }

    /**
     * @param int $ies_id
     * @return bool
     */
    public static function isValidItem($ies_id){
        return in_array($ies_id, [
            self::IDL_EASY,
            self::IDL_MEDIUM,
            self::IDL_DIFFICULT,
        ]);
    }

    /**
     * @param int|string $ies_id
     * @return string
     */
    public static function getLabel($ies_id)
    {
        $ies_id = intval($ies_id);

        switch ($ies_id) {
            case (self::IDL_EASY):
                return self::EASY_LBL;
            case (self::IDL_MEDIUM):
                return self::MEDIUM_LBL;
            case (self::IDL_DIFFICULT):
                return self::DIFFICULT_LBL;
            default:
                return '';
        }
    }
}