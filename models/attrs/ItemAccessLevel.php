<?php

namespace app\models\attrs;

use app\models\utils\BaseEnum;

class ItemAccessLevel extends BaseEnum
{

    const IAL_PUBLIC            = 1;
    const PUBLIC_LBL        = 'PUBLIC';
    const IAL_PRIVATE           = 2;
    const PRIVATE_LBL       = 'PRIVATE';
    const IAL_ORGANIZATION      = 3;
    const ORGANIZATION_LBL  = 'ORGANIZATION';
    const IAL_TRAINER           = 4;
    const TRAINER_LBL       = 'TRAINER';


    public static function getList()
    {
        return [
            self::PUBLIC_LBL        => \Yii::t('app', 'public'),
            self::PRIVATE_LBL       => \Yii::t('app', 'private'),
            self::ORGANIZATION_LBL  => \Yii::t('app', 'organization'),
            self::TRAINER_LBL       => \Yii::t('app', 'trainer'),
        ];
    }


    public static function getDropDownList()
    {
        return [
            self::IAL_PUBLIC        => self::PUBLIC_LBL        ,
            self::IAL_PRIVATE       => self::PRIVATE_LBL       ,
            self::IAL_ORGANIZATION  => self::ORGANIZATION_LBL  ,
            self::IAL_TRAINER       => self::TRAINER_LBL       ,
        ];
    }


    /**
     * @param int $ial_id
     * @return bool
     */
    public static function isValidItem($ial_id){
        return in_array($ial_id, [
                self::IAL_PUBLIC        ,
                self::IAL_PRIVATE       ,
                self::IAL_ORGANIZATION  ,
                self::IAL_TRAINER       ,
        ]);
    }



    /**
     * @param int|string $ial_id
     * @return string
     */
    public static function getLabel($ial_id)
    {
        switch ($ial_id) {
            case (self::IAL_PUBLIC):
                return self::PUBLIC_LBL;
            case (self::IAL_PRIVATE):
                return self::PRIVATE_LBL;
            case (self::IAL_ORGANIZATION):
                return self::ORGANIZATION_LBL;
            case (self::IAL_TRAINER):
                return self::TRAINER_LBL;
            default:
                return '';
        }
    }
}