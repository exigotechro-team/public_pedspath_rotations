<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * SelectQuestionForm is the model behind the contact form.
 */
class QuestionRuleForm extends Model
{
    /** @var null|integer $test_id */
    public $test_id;


/**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['test_id'], 'required'],
            [['test_id'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'test_id' => 'Test ID',
        ];
    }

}
