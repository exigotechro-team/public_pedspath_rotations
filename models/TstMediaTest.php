<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tst_media_test".
 *
 * @property integer $id
 * @property integer $test_id
 * @property integer $media_id
 *
 * @property LprTest $test
 * @property LprMediaContent $media
 */
class TstMediaTest extends \yii\db\ActiveRecord
{

    const COL_ID = 'id';
    const COL_TEST_ID = 'test_id';
    const COL_MEDIA_ID = 'media_id';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_media_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id', 'media_id'], 'required'],
            [['test_id', 'media_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'media_id' => 'Media ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(LprTest::className(), ['id' => 'test_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(LprMediaContent::className(), ['id' => 'media_id']);
    }
}
