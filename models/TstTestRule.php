<?php

namespace app\models;

use app\models\attrs\ItemAccessLevel;
use app\models\attrs\ItemDifficultyLevel;
use app\models\attrs\ItemEditStatus;
use Yii;

/**
 * This is the model class for table "tst_test_rule".
 *
 * @property integer $id
 * @property integer $rb_id
 * @property integer $ctag_id
 * @property integer $q_count
 * @property integer $edit_status
 * @property integer $access_status
 * @property string $difficulty
 * @property integer $cloned_from
 * @property string $created_on
 *
 * @property TstTestQuestion[] $tstTestQuestions
 * @property RuleBook $rb
 * @property LprCtag $ctag
 */
class TstTestRule extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_RB_ID = 'rb_id';
    const COL_CTAG_ID = 'ctag_id';
    const COL_Q_COUNT = 'q_count';
    const COL_EDIT_STATUS = 'edit_status';
    const COL_ACCESS_STATUS = 'access_status';
    const COL_DIFFICULTY = 'difficulty';
    const COL_CLONED_FROM = 'cloned_from';
    const COL_CREATED_ON = 'created_on';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_test_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rb_id', 'ctag_id'], 'required'],
            [['rb_id', 'ctag_id', 'q_count', 'edit_status', 'access_status', 'cloned_from'], 'integer'],
            [['difficulty'], 'string', 'max' => 16],
            [['rb_id'], 'exist', 'skipOnError' => true, 'targetClass' => RuleBook::className(), 'targetAttribute' => ['rb_id' => 'id']],
            [['ctag_id'], 'exist', 'skipOnError' => true, 'targetClass' => LprCtag::className(), 'targetAttribute' => ['ctag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rb_id' => 'Rule Book ID',
            'ctag_id' => 'Ctag ID',
            'q_count' => '# of questions',
            'edit_status' => 'Edit Status',
            'access_status' => 'Access Status',
            'difficulty' => 'Difficulty',
            'cloned_from' => 'Cloned From',
            'created_on' => 'Created On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstTestQuestions()
    {
        return $this->hasMany(TstTestQuestion::className(), ['rb_rule_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleBook()
    {
        return $this->hasOne(RuleBook::className(), ['id' => 'rb_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCtag()
    {
        return $this->hasOne(LprCtag::className(), ['id' => 'ctag_id']);
    }

    /**
     * @inheritdoc
     * @return TstTestRuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TstTestRuleQuery(get_called_class());
    }

    public function getCtagLabel($show_id = true)
    {
        $tmpl = ($show_id) ? "%s (ctag_id: %s)" : "%s";

        /** @var LprCtag $ctag */
        $ctag = $this->getCtag()->one();

        return sprintf($tmpl, $ctag->label, $ctag->id);
    }

    public function getEditStatusLabel()
    {
        $val = "";

        switch ($this->edit_status) {
            case ItemEditStatus::IES_DRAFT:
                $val = ItemEditStatus::DRAFT_LBL;
                break;
            case ItemEditStatus::IES_PUBLISHED:
                $val = ItemEditStatus::PUBLISHED_LBL;
                break;
            case ItemEditStatus::IES_REVIEW:
                $val = ItemEditStatus::REVIEW_LBL;
                break;
            default:
                $val = "";
        }

        return sprintf("%s (id: %s)", $val, $this->edit_status);
    }

    public function getAccessStatusLabel()
    {
        $val = ItemAccessLevel::getLabel($this->access_status);
        return sprintf("%s (id: %s)", $val, $this->access_status);
    }

    public function getDifficultyLabel($show_id = true)
    {
        $tmpl = ($show_id) ? "%s (id: %s)" : "%s";
        $val = ItemDifficultyLevel::getLabel($this->difficulty);
        return sprintf($tmpl, $val, $this->difficulty);
    }

    public function getRuleBookLabel()
    {
        /** @var RuleBook $rb */
        $rb = $this->getRuleBook()->one();

        return sprintf("%s (id: %s)", $rb->title, $rb->id);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_on = date("Y-m-d H:i:s"); // time();
        } elseif (!$this->isNewRecord && empty($this->created_on)) {
            $this->created_on = date("Y-m-d H:i:s"); // time();
        }

        return parent::beforeSave($insert);
    }


}