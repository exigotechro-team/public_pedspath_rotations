<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tst_answer".
 *
 * @property integer $id
 * @property integer $question_id
 * @property string $body
 * @property boolean $has_media
 * @property integer $display_order
 * @property boolean $is_correct
 * @property string $search_text
 *
 * @property LprQuestion $question
 * @property TstMediaAnswer[] $tstMediaAnswers
 */
class TstAnswer extends \yii\db\ActiveRecord
{

    const COL_ID = 'id';
    const COL_QUESTION_ID = 'question_id';
    const COL_BODY = 'body';
    const COL_HAS_MEDIA = 'has_media';
    const COL_DISPLAY_ORDER = 'display_order';
    const COL_IS_CORRECT = 'is_correct';
    const COL_SEARCH_TEXT = 'search_text';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'display_order'], 'integer'],
            [['body'], 'required'],
            [['body', 'search_text'], 'string'],
            [['has_media', 'is_correct'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'body' => 'Body',
            'has_media' => 'Has Media',
            'display_order' => 'Display Order',
            'is_correct' => 'Is Correct',
            'search_text' => 'Search Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(LprQuestion::className(), ['id' => 'question_id']);
    }


    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getTstMediaAnswers()
    {
//        return $this->hasMany(TstMediaAnswer::className(), ['answer_id' => 'id']);

        $tstMediaAnswers = \app\models\db\ext\TstMediaAnswer::find()->where([
            TstMediaAnswer::COL_ANSWER_ID => $this->id,
        ])->all();

        return !empty($tstMediaAnswers) ? $tstMediaAnswers : [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMediaItems()
    {
        return $this->hasMany(LprMediaContent::className(), ['id' => TstMediaAnswer::COL_MEDIA_ID])
            ->viaTable(TstMediaAnswer::tableName(), [TstMediaAnswer::COL_ANSWER_ID => 'id']);
    }

} // end class