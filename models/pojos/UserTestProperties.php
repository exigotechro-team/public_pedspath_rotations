<?php

namespace app\models\pojos;

use yii\base\BaseObject;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "tst_answer".
 *
 * @property boolean $canJump
 * @property boolean $canGoPrevious
 * @property boolean $canGoNext
 * @property boolean $canShowDebug
 * @property boolean $canRndQuestions
 * @property boolean $canRndAnswers
 * @property boolean $canChangeAnswer
 * @property boolean $canSaveAndExit
 *
 */

class UserTestProperties extends Model
{
    public $canJump;
    public $canGoPrevious;
    public $canGoNext;
    public $canShowDebug;
    public $canRndQuestions;
    public $canRndAnswers;
    public $canChangeAnswer;
    public $canSaveAndExit;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['canJump', 'canGoPrevious', 'canGoNext', 'canShowDebug', 'canRndQuestions', 'canRndAnswers', 'canChangeAnswer', 'canSaveAndExit' ], 'boolean'],
            [['canJump', 'canGoPrevious', 'canGoNext', 'canShowDebug', 'canRndQuestions', 'canRndAnswers', 'canChangeAnswer', 'canSaveAndExit' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'canJump' => 'Can Jump To Q',
            'canGoPrevious' => 'Go Previous',
            'canGoNext' => 'Go Next',
            'canShowDebug' => 'Show Debug Info',
            'canRndQuestions' => 'Randomize Questions Order',
            'canRndAnswers' => 'Randomize Answers Order',
            'canChangeAnswer' => 'Allow Change Answer',
            'canSaveAndExit' => 'Can Save and Exit',
        ];
    }


    /**
     * UserTestProperties constructor.
     * @param array|null $args
     */
    public function __construct($args = null)
    {
        parent::__construct();

        if (empty($args) || !is_array($args)) {
            $args = [];
        }

        if (is_array($args)) {
            $this->canJump = array_key_exists('canJump', $args) ? $args['canJump'] : false;
            $this->canGoPrevious = array_key_exists('canGoPrevious', $args) ? $args['canGoPrevious'] : true;
            $this->canGoNext = array_key_exists('canGoNext', $args) ? $args['canGoNext'] : true;
            $this->canShowDebug = array_key_exists('canShowDebug', $args) ? $args['canShowDebug'] : false;
            $this->canRndQuestions = array_key_exists('canRndQuestions', $args) ? $args['canRndQuestions'] : false;
            $this->canRndAnswers = array_key_exists('canRndAnswers', $args) ? $args['canRndAnswers'] : false;
            $this->canChangeAnswer = array_key_exists('canChangeAnswer', $args) ? $args['canChangeAnswer'] : true;
            $this->canSaveAndExit = array_key_exists('canSaveAndExit', $args) ? $args['canSaveAndExit'] : true;
        }
    }


    /**
     * @return bool
     */
    public function getCanJump(): bool
    {
        return $this->canJump;
    }

    /**
     * @param bool|null $canJump
     */
    public function setCanJump($canJump = false)
    {
        $this->canJump = (empty($canJump)) ? false : $canJump;
    }


    /**
     * @return bool
     */
    public function getCanGoPrevious(): bool
    {
        return $this->canGoPrevious;
    }

    /**
     * @param bool $canGoPrevious
     */
    public function setCanGoPrevious($canGoPrevious = false)
    {
        $this->canGoPrevious = (empty($canGoPrevious)) ? false : $canGoPrevious;
    }

    /**
     * @return bool
     */
    public function getCanGoNext(): bool
    {
        return $this->canGoNext;
    }

    /**
     * @param bool $canGoNext
     */
    public function setCanGoNext($canGoNext = false)
    {
        $this->canGoNext = (empty($canGoNext)) ? false : $canGoNext;
    }

    /**
     * @return bool
     */
    public function getCanShowDebug(): bool
    {
        return $this->canShowDebug;
    }

    /**
     * @param bool $canShowDebug
     */
    public function setCanShowDebug($canShowDebug = false)
    {
        $this->canShowDebug = (empty($canShowDebug)) ? false : $canShowDebug;
    }


    /**
     * @return bool
     */
    public function getCanRndQuestions()
    {
        return $this->canRndQuestions;
    }

    /**
     * @param mixed $canRndQuestions
     */
    public function setCanRndQuestions($canRndQuestions = false)
    {
        $this->canRndQuestions = $canRndQuestions;
    }

    /**
     * @return bool
     */
    public function getCanRndAnswers()
    {
        return $this->canRndAnswers;
    }

    /**
     * @param mixed $canRndAnswers
     */
    public function setCanRndAnswers($canRndAnswers = false)
    {
        $this->canRndAnswers = $canRndAnswers;
    }

    /**
     * @return bool
     */
    public function getCanChangeAnswer()
    {
        return $this->canChangeAnswer;
    }

    /**
     * @param mixed $canChangeAnswer
     */
    public function setCanChangeAnswer($canChangeAnswer = false)
    {
        $this->canChangeAnswer = $canChangeAnswer;
    }


    /**
     * @return bool
     */
    public function getCanSaveAndExit()
    {
        return $this->canSaveAndExit;
    }

    /**
     * @param mixed $canSaveAndExit
     */
    public function setCanSaveAndExit($canSaveAndExit = false)
    {
        $this->canSaveAndExit = $canSaveAndExit;
    }


    public function __toString()
    {
        $dmpVar = VarDumper::dumpAsString([
            'usr_tst_props' => $this,
        ]);

        return sprintf("\n%s\n", $dmpVar);
    }

}