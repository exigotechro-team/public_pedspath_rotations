<?php

namespace app\models\utils;

use yii\base\InvalidParamException;

class BaseEnum implements IEnum
{
    public static function getList()
    {
        return [];
    }

    public static function getLabel($value)
    {
        $list = static::getList();

        if (!isset($list[$value])) {
            throw new InvalidParamException();
        }

        return $list[$value];
    }
}
