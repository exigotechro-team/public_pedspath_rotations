<?php

namespace app\models\form;

use app\ro\exigotech\lpr\HashUtils;
use DateTime;
use Yii;
use yii\base\Model;

/**
 * QuestionsMassAssignForm is the model behind the contact form.
 */
class TestQuestionPreviewForm extends Model
{
    /** @var integer $t_id */
    public $t_id;

    /** @var integer $q_id */
    public $q_id;

    /** @var integer|null $q_answr */
    public $q_answr;

    /** @var integer|null $display_timestamp */
    public $display_timestamp;

    /** @var string|null $data_hash */
    public $data_hash;

    /**
     * TestQuestionPreviewForm constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->q_answr = null;
        $this->q_id = -1;
        $this->t_id = -1;
        $this->setDisplayTimestamp();
        $this->data_hash = null;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['t_id', 'q_id', 'q_answr', 'display_timestamp'], 'integer'],
            [['data_hash'], 'string'],
            [['t_id', 'q_id', 'display_timestamp', 'data_hash'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            't_id'              => 'Test ID',
            'q_id'              => 'Question ID',
            'q_answr'           => 'Answer ID',
            'display_timestamp' => 'Timestamp at display',
            'data_hash'         => 'Hash for object data',
        ];
    }

    /**
     * @param int|null $disp_timestamp
     */
    public function setDisplayTimestamp($disp_timestamp = null)
    {
        if(!empty($disp_timestamp)){
            $this->display_timestamp = $disp_timestamp;
        }else{
            $date = new DateTime();
            $this->display_timestamp = $date->getTimestamp();
        }
    }


    /**
     * @return null|string
     */
    public function getControlHash()
    {
        if(empty($this->data_hash)){
            return $this->buildControlHash();
        }

        return $this->data_hash;
    }

    public function setControlHash()
    {
        $this->data_hash = $this->buildControlHash();
    }

    public function buildControlHash()
    {
        return HashUtils::GetHash([$this->t_id, $this->q_id, $this->display_timestamp]);
    }

    public function verifyControlHash()
    {
        return HashUtils::VerifyHash($this->buildControlHash(), $this->data_hash);
    }

}