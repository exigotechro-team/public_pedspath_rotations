<?php

namespace app\models\form;


class UserTestAssign extends \yii\base\Model
{
    public $uid;
    public $tid;

    /** @var bool $notify */
    public $notify;

    public $tests;
    public $trainees;


    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->notify = false;
        $this->tests = [/*'5' => 'T1', '34' => 'T2'*/];
        $this->trainees = [/*'56' => 'U1', '58' => 'U2'*/];
    }

    public function rules()
    {
        return [
            [['uid', 'tid', 'notify'], 'safe'],
            [['uid', 'tid'], 'required'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid'        => 'Trainee',
            'tid'        => 'Test',
            'notify'     => 'Notify by email',
        ];
    }
}
