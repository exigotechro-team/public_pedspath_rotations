<?php

namespace app\models\form;

use app\ro\exigotech\lpr\HashUtils;
use DateTime;
use Yii;
use yii\base\Model;

/**
 * QuestionsMassAssignForm is the model behind the contact form.
 */
class TestTakeQuestionForm extends Model
{
    /** @var integer $t_id */
    public $t_id;

    /** @var integer $q_id */
    public $q_id;

    /** @var integer|null $q_answr */
    public $q_answr;

    /** @var integer|null $display_timestamp */
    public $display_timestamp;

    /** @var string|null $data_hash */
    public $data_hash;

    /** @var int $curr_pos */
    public $curr_pos;

    /** @var bool $allowGoJump */
    public $allowGoJump;

    /** @var bool $allowGoPrevious */
    public $allowGoPrevious;

    /** @var bool $allowDebugInfo */
    public $allowDebugInfo;

    /** @var bool $allowSaveAndExit */
    public $allowSaveAndExit;

    /** @var bool $isReviewMode */
    public $isReviewMode;

    /**
     * TestTakeQuestionForm constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->q_answr = null;
        $this->q_id = -1;
        $this->t_id = -1;
        $this->setDisplayTimestamp();
        $this->data_hash = null;
        $this->curr_pos = null;
        $this->allowGoJump = false;
        $this->allowGoPrevious = false;
        $this->allowDebugInfo = false;
        $this->allowSaveAndExit = true;
        $this->isReviewMode = false;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['t_id', 'q_id', 'q_answr', 'display_timestamp', 'curr_pos'], 'integer'],
            [['allowGoJump', 'allowGoPrevious', 'allowDebugInfo', 'allowSaveAndExit', 'isReviewMode'], 'boolean'],
            [['data_hash'], 'string'],
            [['t_id', 'q_id', 'display_timestamp', 'data_hash', 'curr_pos'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            't_id'              => 'Test ID',
            'q_id'              => 'Question ID',
            'q_answr'           => 'Answer ID',
            'display_timestamp' => 'Timestamp at display',
            'data_hash'         => 'Hash for object data',
            'curr_pos'          => 'Current position',
            'allowGoJump'       => 'Allow jumping to question',
            'allowGoPrevious'   => 'Allow going to previous question',
            'allowDebugInfo'    => 'Show debug info panel',
            'allowSaveAndExit'  => 'Allow Save and Exit',
            'isReviewMode'      => 'In review mode',
        ];
    }

    /**
     * @param int|null $disp_timestamp
     */
    public function setDisplayTimestamp($disp_timestamp = null)
    {
        if(!empty($disp_timestamp)){
            $this->display_timestamp = $disp_timestamp;
        }else{
            $date = new DateTime();
            $this->display_timestamp = $date->getTimestamp();
        }
    }


    /**
     * @return null|string
     */
    public function getControlHash()
    {
        if(empty($this->data_hash)){
            return $this->buildControlHash();
        }

        return $this->data_hash;
    }

    public function setControlHash()
    {
        $this->data_hash = $this->buildControlHash();
    }

    public function buildControlHash()
    {
        $answr = (!empty($this->q_answr)) ? $this->q_answr : '';

        return HashUtils::GetHash([$this->t_id, $this->q_id, $this->display_timestamp, $this->curr_pos]);
    }

    public function verifyControlHash()
    {
        return HashUtils::VerifyHash($this->buildControlHash(), $this->data_hash);
    }

}