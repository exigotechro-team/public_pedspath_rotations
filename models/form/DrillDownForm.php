<?php

namespace app\models\form;

use yii\base\Model;


class DrillDownForm extends Model
{

    /** @var  integer $org_id */
    public $org_id;

    /** @var  integer $pgc_uid */
    public $pgc_uid;

    /** @var  integer $trainer_uid */
    public $trainer_uid;

    /** @var  integer $trainee_uid */
    public $trainee_uid;

    /** @var  integer $ttu_id */
    public $ttu_id;


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_id'        => \Yii::t('app', 'Organizations'),
            'pgc_uid'       => \Yii::t('app', 'Program Coordinator(s)'),
            'trainer_uid'   => \Yii::t('app', 'Trainer(s)'),
            'trainee_uid'   => \Yii::t('app', 'Trainee(s)'),
            'ttu_id'       => \Yii::t('app', 'Exam(s)'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'trainer_uid', 'trainee_uid', 'ttu_id'], 'integer'],
        ];
    }
}