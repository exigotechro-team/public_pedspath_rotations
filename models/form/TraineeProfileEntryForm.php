<?php

namespace app\models\form;

use app\models\db\UserProfile;
use app\ro\exigotech\lpr\HashUtils;
use DateTime;
use Yii;
use yii\base\Model;

/**
 * TraineeProfileEntryForm is the model behind the contact form.
 */
class TraineeProfileEntryForm extends Model
{
    /** @var integer $upid */
    public $upid;

    /** @var integer $up_uid */
    public $up_uid;

    /** @var string $up_pname */
    public $up_pname;

    /** @var string $up_pval */
    public $up_pval;

    /** @var string|null $up_ctrl_hash */
    public $up_ctrl_hash;


    /**
     * TestTakeQuestionForm constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->up_pname = null;
        $this->up_uid = null;
        $this->upid = null;
        $this->up_pval = null;
        $this->up_ctrl_hash = null;
    }

    /**
     * @param UserProfile|array $userProfile
     * @return TraineeProfileEntryForm
     */
    public static function CreateFromUserProfile($userProfile)
    {
        $te_uprofile_form = new TraineeProfileEntryForm();

        if(is_array($userProfile)){
            $te_uprofile_form->upid         = $userProfile['id'];
            $te_uprofile_form->up_uid       = $userProfile['owner_id'];
            $te_uprofile_form->up_pname     = $userProfile['prop_name'];
            $te_uprofile_form->up_pval      = $userProfile['prop_val'];
        }else{
            $te_uprofile_form->upid         = $userProfile->id;
            $te_uprofile_form->up_uid       = $userProfile->owner_id;
            $te_uprofile_form->up_pname     = $userProfile->prop_name;
            $te_uprofile_form->up_pval      = $userProfile->prop_val;
        }

        $te_uprofile_form->setControlHash();

        return $te_uprofile_form;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['upid', 'up_uid'], 'integer'],
            [['up_pname', 'up_pval', 'up_ctrl_hash'], 'string'],
            [['upid', 'up_uid', 'up_pname', 'up_pval', 'up_ctrl_hash'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'upid'         => 'Entry ID',
            'up_uid'       => 'Trainee ID',
            'up_pname'     => 'Property Name',
            'up_pval'      => 'Property Value',
            'up_ctrl_hash' => 'Control Hash',
        ];
    }

    /**
     * @return null|string
     */
    public function getControlHash()
    {
        return $this->up_ctrl_hash;
    }

    public function setControlHash()
    {
        $this->up_ctrl_hash = $this->buildControlHash();
    }

    /**
     * @return string
     */
    public function buildControlHash()
    {
        return HashUtils::GetHash([$this->upid, $this->up_uid, $this->up_pname/*, $this->up_pval*/]);
    }

    /**
     * @return bool
     */
    public function verifyControlHash()
    {
        return HashUtils::VerifyHash($this->buildControlHash(), $this->up_ctrl_hash);
    }

}