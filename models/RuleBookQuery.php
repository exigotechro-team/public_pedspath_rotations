<?php

namespace app\models;

use Yii;

/**
 * This is the ActiveQuery class for [[RuleBook]].
 *
 * @see RuleBook
 */
class RuleBookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return RuleBook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RuleBook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @deprecated
     */
    public function findRuleBlockForTest($tid)
    {
        $sql = <<<SQL_STR
            select ifnull( sum(q_count), 0) as q_count_total from tst_test_rule where rb_id = :rb_id;
SQL_STR;

        /** @var RuleBook $rule_book */
        $rule_book = RuleBook::find()->where([RuleBook::COL_ID => $this->id])->one();

        $val = $rule_book->findBySql($sql, [':rb_id' => $this->id])->asArray()->one();

        return intval($val['q_count_total']);

    }

}
