<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "lpr_ledger".
 *
 * @property integer $id
 * @property string $obj_system
 * @property string $node_type
 * @property integer $node_id
 * @property string $node_status
 * @property string $subnode_type
 * @property integer $subnode_id
 * @property integer $created_at
 * @property string $args
 * @property string $notes
 */
class LprLedger extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_OBJ_SYSTEM = 'obj_system';
    const COL_NODE_TYPE = 'node_type';
    const COL_NODE_ID = 'node_id';
    const COL_NODE_STATUS = 'node_status';
    const COL_SUBNODE_TYPE = 'subnode_type';
    const COL_SUBNODE_ID = 'subnode_id';
    const COL_CREATED_AT = 'created_at';
    const COL_ARGS = 'args';
    const COL_NOTES = 'notes';


    const SCENARIO_TEST_ASSIGN = 'test_assignment';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lpr_ledger';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['obj_system', 'node_type', 'node_id'], 'required'],
            [['node_id', 'subnode_id', 'created_at'], 'integer'],
            [['args', 'notes'], 'string'],
            [['obj_system', 'node_type', 'node_status', 'subnode_type'], 'string', 'max' => 32],

            ['node_id', 'validateTestOrg', 'on' => self::SCENARIO_TEST_ASSIGN],
            ['subnode_id', 'validateTestOrg', 'on' => self::SCENARIO_TEST_ASSIGN],

            [['node_id', 'subnode_id'], 'required', 'on' => self::SCENARIO_TEST_ASSIGN],
        ];
    }


    public function validateTestOrg($attribute){
        $node_id = $this->node_id;
        $subnode_id = $this->subnode_id;

        if(!empty($node_id) && !empty($subnode_id) ){

            $lprLedger = LprLedger::find()->where([
                'node_id' => intval( $node_id),
                'subnode_id' => intval( $subnode_id ),
                'obj_system' => 'LprTEST',
                'node_type' => 'TEST',
                'node_status' => 'TEST_ASSIGN',
                'subnode_type' => 'ORG',
            ])->one();

            if(!empty($lprLedger)){
                $this->addError($attribute, "Test already assigned to Organization");
            }
        }
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_TEST_ASSIGN] = ['obj_system', 'node_type', 'node_id', 'node_status', 'subnode_type', 'subnode_id'];
        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'obj_system' => 'Obj System',
            'node_type' => 'Node Type',
            'node_id' => 'Node ID',
            'node_status' => 'Node Status',
            'subnode_type' => 'Subnode Type',
            'subnode_id' => 'Subnode ID',
            'created_at' => 'Created At',
            'args' => 'Args',
            'notes' => 'Notes',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\LprLedgerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\LprLedgerQuery(get_called_class());
    }
}
