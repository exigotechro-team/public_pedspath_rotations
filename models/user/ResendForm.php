<?php

namespace app\models\user;

use \dektrium\user\models\ResendForm as BaseResendForm;

class ResendForm extends BaseResendForm
{
    /**
     * @var string
     */
    public $captcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['captcha', 'required'];
        $rules[] = ['captcha', 'captcha'];
        return $rules;
    }
}