<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TstTestRule;

/**
 * TstTestRuleSearch represents the model behind the search form about `app\models\TstTestRule`.
 */
class TstTestRuleSearch extends TstTestRule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rb_id', 'ctag_id', 'q_count', 'edit_status', 'access_status'], 'integer'],
            [['difficulty'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TstTestRule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rb_id' => $this->rb_id,
            'ctag_id' => $this->ctag_id,
            'q_count' => $this->q_count,
            'edit_status' => $this->edit_status,
            'access_status' => $this->access_status,
        ]);

        $query->andFilterWhere(['like', 'difficulty', $this->difficulty]);

        return $dataProvider;
    }

    public function getRulesForRuleBook($rb_id)
    {
        $query = TstTestRule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'rb_id' => $rb_id,
        ]);


        return $dataProvider;
    }

    public static function GetRulesCountForRuleBook($rb_id)
    {
        $rules_count = count(TstTestRule::find()
            ->where([
                TstTestRule::COL_RB_ID => $rb_id,
            ])->asArray()->all());

        if(empty($rules_count)){
            $rules_count = 0;
        }

        return $rules_count;
    }

    /**
     * @param $qparams array
     * @return \app\models\TstTestRule[]|array
     */
    public function retrieve($qparams)
    {
        return TstTestRule::find()
            ->where($qparams)->all();
    }


}
