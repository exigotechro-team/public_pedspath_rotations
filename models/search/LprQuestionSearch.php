<?php

namespace app\models\search;

use app\models\LprCtag;
use app\models\db\search\TstTestQuestionOrder;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LprQuestion;
use yii\data\SqlDataProvider;
use yii\db\Exception;
use yii\helpers\VarDumper;

/**
 * LprQuestionSearch represents the model behind the search form about `app\models\LprQuestion`.
 */
class LprQuestionSearch extends LprQuestion
{

    public $questionViewForGrid;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'difficulty', 'is_qtag_used', 'edit_status', 'access_level', 'owner_id', 'question_type'], 'integer'],
            [['title', 'description', 'qgroup_tag', 'created_on', 'updated_on'], 'safe'],
            [['questionViewForGrid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LprQuestion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => ['id'=>SORT_DESC],
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'difficulty' => $this->difficulty,
            'is_qtag_used' => $this->is_qtag_used,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'edit_status' => $this->edit_status,
            'access_level' => $this->access_level,
            'owner_id' => $this->owner_id,
            'question_type' => $this->question_type,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'qgroup_tag', $this->qgroup_tag]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchForTest($params, $tid)
    {
        $query = LprQuestion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> [
                'defaultOrder' => ['id'=>SORT_DESC],
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['questionViewForGrid'] = [
            'asc' => ['title' => SORT_ASC],
            'desc' => ['title' => SORT_DESC],
            'label' => 'Question',
            'default' => SORT_ASC
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'difficulty' => $this->difficulty,
            'is_qtag_used' => $this->is_qtag_used,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
//            'title' => $this->questionViewForGrid,
//            'description' => $this->questionViewForGrid,
        ]);


        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'title', $this->questionViewForGrid])
//            ->andFilterWhere(['like', 'description', $this->questionViewForGrid])
            ->andFilterWhere(['like', 'qgroup_tag', $this->qgroup_tag]);

        $query->andWhere('title LIKE "%' . $this->questionViewForGrid . '%" ' .
                  'OR description LIKE "%' . $this->questionViewForGrid . '%"'
              );

        $query->joinWith(['ctags'])->orFilterWhere(['like', LprCtag::tableName() . '.label', $this->questionViewForGrid]);

        return $dataProvider;
    }

    /**
     * @param $params array
     * @param $tid integer
     * @return ActiveDataProvider
     */
    public function searchTestQuestions($params, $tid)
    {
        $sql = <<<SQL_STR
            SELECT q.*
            FROM lpr_question q
              JOIN tst_test_question tq
                ON tq.question_id = q.id
                  AND tq.test_id = :tid
            ORDER BY tq.test_order ASC, tq.id ASC;
SQL_STR;

        $query = LprQuestion::findBySql($sql, [':tid' => $tid]);

        $totalCount = $this->getTestQuestionsCount($tid);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $totalCount,
            'sort'=> [
                'defaultOrder' => ['id'=>SORT_DESC],
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['questionViewForGrid'] = [
            'asc' => ['q.title' => SORT_ASC],
            'desc' => ['q.title' => SORT_DESC],
            'label' => 'Question',
            'default' => SORT_ASC
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'q.id' => $this->id,
            'q.difficulty' => $this->difficulty,
            'q.is_qtag_used' => $this->is_qtag_used,
            'q.created_on' => $this->created_on,
            'q.updated_on' => $this->updated_on,
//            'title' => $this->questionViewForGrid,
//            'description' => $this->questionViewForGrid,
        ]);


        $query->andFilterWhere(['like', 'q.title', $this->title])
            ->andFilterWhere(['like', 'q.description', $this->description])
//            ->andFilterWhere(['like', 'title', $this->questionViewForGrid])
//            ->andFilterWhere(['like', 'description', $this->questionViewForGrid])
            ->andFilterWhere(['like', 'q.qgroup_tag', $this->qgroup_tag]);

        $query->andWhere('q.title LIKE "%' . $this->questionViewForGrid . '%" ' .
            'OR q.description LIKE "%' . $this->questionViewForGrid . '%"'
        );

        $query->joinWith(['ctags'])->orFilterWhere(['like', LprCtag::tableName() . '.label', $this->questionViewForGrid]);

        return $dataProvider;
    }

    /**
     * @param $params array
     * @param $tid integer
     * @return SqlDataProvider
     */
    public function searchQuestionsDisplayOrder($params, $tid)
    {
        $sql = <<<SQL_STR
            SELECT tq.test_order,
              tq.id,
              tq.test_id,
              tq.question_id,
              q.title as title, 
              q.description as description 
              -- ,q.*
            FROM lpr_question q
              JOIN tst_test_question tq
                ON tq.question_id = q.id
                   AND tq.test_id = :tid
            ORDER BY tq.test_order ASC, tq.id ASC;
SQL_STR;

        $totalCount = $this->getTestQuestionsCount($tid);

//        $query = TstTestQuestionOrder::findBySql($sql, [':tid' => $tid]);
//        $totalCount = count($query->all());

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':tid' => $tid],
            'totalCount' => $totalCount,
            'sort'=> [
                'defaultOrder' => ['test_order'=>SORT_ASC],
                'attributes' => ['test_order'],
            ],
            'pagination' => [
                'pageSize' => 200,
            ],
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['questionViewForGrid'] = [
            'asc' => ['tq.test_order' => SORT_ASC],
            'desc' => ['tq.test_order' => SORT_DESC],
            'label' => 'Question',
            'default' => SORT_ASC
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

        return $dataProvider;
    }


    public function searchAvailableQuestions($tid, $term)
    {
        $sql = <<<SQL_STR
            SELECT q.id, q.title
            -- ,q.*
            FROM lpr_question q
              JOIN tst_ctag_question tcq ON q.id = tcq.question_id
              JOIN lpr_ctag l ON tcq.category_id = l.id
            WHERE (q.title LIKE :term
                   OR l.label LIKE :term)
                  AND q.id NOT IN (
              SELECT tq.question_id
              FROM tst_test_question tq
              WHERE tq.test_id = :tid
            );
SQL_STR;

        $res = ['id' => '', 'title' => ''];
        $term = preg_replace("/[^a-z\d\s\-_]+/i", '', $term);
        $term = addcslashes($term, '%_'); // escape LIKE's special characters

        $tid = preg_replace("/[^\d]/i", '', $tid);

        try {
            $res2 = \Yii::$app->db->createCommand($sql, [':tid' => $tid, ':term' => "%$term%"])->queryAll();
            if(!empty($res2)){
                $res = $res2;
            }
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            exit;
        }

        return $res;
    }

    public function searchAssignedQuestions($tid, $term)
    {
        $sql = <<<SQL_STR
            SELECT q.id, q.title
            -- ,q.*
            FROM lpr_question q
              JOIN tst_ctag_question tcq ON q.id = tcq.question_id
              JOIN lpr_ctag l ON tcq.category_id = l.id
            WHERE (q.title LIKE :term
                   OR l.label LIKE :term)
                  AND q.id IN (
              SELECT tq.question_id
              FROM tst_test_question tq
              WHERE tq.test_id = :tid
            );
SQL_STR;

        $res = ['id' => '', 'title' => ''];
        $term = preg_replace("/[^a-z\d\s\-_]+/i", '', $term);
        $term = addcslashes($term, '%_'); // escape LIKE's special characters

        $tid = preg_replace("/[^\d]/i", '', $tid);

        try {
            $res2 = \Yii::$app->db->createCommand($sql, [':tid' => $tid, ':term' => "%$term%"])->queryAll();
            if(!empty($res2)){
                $res = $res2;
            }
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            exit;
        }

        return $res;
    }




    /**
     * @param $tid integer
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionsAssignedToTest($tid)
    {
        $sql = <<<SQL_STR
            SELECT tq.test_order,
              tq.id,
              tq.test_id,
              tq.question_id,
              q.title as title
              -- ,q.*
            FROM lpr_question q
              JOIN tst_test_question tq
                ON tq.question_id = q.id
                   AND tq.test_id = :tid
            ORDER BY tq.test_order ASC,
                tq.id ASC;
SQL_STR;

        return TstTestQuestionOrder::findBySql($sql, [':tid' => $tid]);
    }


    /**
     * @param int $tid
     * @return int
     */
    public function getTestQuestionsCount($tid)
    {
        $sql_count = <<<SQL_STR
            SELECT count(tq.id)
            FROM lpr_question q
              JOIN tst_test_question tq
                ON tq.question_id = q.id
                   AND tq.test_id = :tid;
SQL_STR;

        /** @var int $totalCount */
        $totalCount = 0;

        /** @var int|mixed $totalCount */
        try {
            $totalCount = intval(\Yii::$app->db
                ->createCommand($sql_count)
                ->bindValue(':tid', $tid)
                ->queryScalar());
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            if (!true) {
                echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            }
        }

        return $totalCount;
    }

    /**
     * @param int $tid
     * @param int $pos
     * @return array|null|\yii\db\ActiveRecord|\app\models\db\ext\LprQuestion
     */
    public function getTestQuestionForPosition($tid, $pos)
    {
        $sql = <<<SQL_STR
            SELECT q.*
            FROM lpr_question q
              JOIN tst_test_question tq
                ON tq.question_id = q.id
                   AND tq.test_id = :tid
            WHERE tq.test_order = :pos
            ORDER BY tq.test_order ASC,
                tq.id ASC;
SQL_STR;

        return \app\models\db\ext\LprQuestion::findBySql($sql, [':tid' => $tid, ':pos' => $pos])->one();
    }

    public function getCurrPosForTestQuestion($tid, $qid)
    {
        $sql = <<<SQL_STR
            SELECT tq.test_order AS curr_pos -- , tq.*
            FROM tst_test_question tq
            WHERE tq.test_id = :tid
              AND tq.question_id = :qid;
SQL_STR;

        /** @var int $curr_pos */
        $curr_pos = 0;

        /** @var int|mixed $totalCount */
        try {
            $curr_pos = intval(\Yii::$app->db
                ->createCommand($sql, [':tid' => $tid, ':qid' => $qid])
                ->queryScalar());
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            if (!true) {
                echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            }
        }

        return intval($curr_pos);
    }

    /**
     * @param int $q_answr
     * @param int $qid
     * @param int $tid
     * @return int
     */
    public function validateQAnswerForTest($q_answr, $qid, $tid)
    {
        // check answer(q_answr) belongs to question(qid) &&
        // check question(qid) belongs to question(tid)

        $sql = <<<SQL_STR
            SELECT count(a.id) as a_cnt
            FROM tst_answer a
              join lpr_question q ON a.question_id = q.id
              JOIN tst_test_question t ON q.id = t.question_id
            WHERE a.question_id = :qid
                AND a.id = :aid 
                AND t.test_id = :tid
            ORDER BY display_order ASC;
SQL_STR;

        /** @var int $is_valid */
        $is_valid = 0;

        /** @var int|mixed $totalCount */
        try {
            $is_valid = intval(\Yii::$app->db
                ->createCommand($sql,
                    [
                        ':tid' => $tid,
                        ':qid' => $qid,
                        ':aid' => $q_answr
                    ]
                )->queryScalar());
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            if (!true) {
                echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
            }
        }

        return intval($is_valid);
    }


}