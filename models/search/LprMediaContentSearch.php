<?php

namespace app\models\search;

use app\models\TstMimeType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LprMediaContent;

/**
 * display MimeType hasOne relation
 * tip from: http://www.yiiframework.com/wiki/653/displaying-sorting-and-filtering-model-relations-on-a-gridview/
 */

/**
 * LprMediaContentSearch represents the model behind the search form about `app\models\LprMediaContent`.
 */
class LprMediaContentSearch extends LprMediaContent
{

    public $mimeType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mimeType'], 'safe'],
            [['id', 'mime_type_id'], 'integer'],
            [['media_uri', 'label', 'description'], 'safe'],
            [['id', 'mime_type_id', 'is_qtag_used'], 'integer'],
            [['media_uri', 'label', 'description', 'media_thumb_uri', 'qgroup_tag', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LprMediaContent::find();

        $query->joinWith(['mimeType']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['mimeType'] = [
            'asc' => [TstMimeType::tableName().'.ext' => SORT_ASC],
            'desc' => [TstMimeType::tableName().'.ext' => SORT_DESC],
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mime_type_id' => $this->mime_type_id,
            'is_qtag_used' => $this->is_qtag_used,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
        ]);

        $query->andFilterWhere(['like', 'media_uri', $this->media_uri])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', TstMimeType::tableName().'.ext', $this->mimeType])
            ->andFilterWhere(['like', 'media_thumb_uri', $this->media_thumb_uri])
            ->andFilterWhere(['like', 'qgroup_tag', $this->qgroup_tag]);


        return $dataProvider;
    }
}
