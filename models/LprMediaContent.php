<?php

namespace app\models;

use app\modules\gm_admin\controllers\MimeTypeController;
use Imagine\Image\Point;
use pheme\settings\components\Settings;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;

use Imagine\Image\Box;
use Imagine\Gmagick\Imagine;

/**
 * This is the model class for table "lpr_media_content".
 *
 * @property integer $id
 * @property integer $mime_type_id
 * @property string $media_uri
 * @property string $label
 * @property string $description
 * @property string $media_thumb_uri
 * @property string $qgroup_tag
 * @property integer $is_qtag_used
 * @property string $created_on
 * @property string $updated_on
 *
 * @property TstMimeType $mimeType
 * @property TstCtagMedia[] $tstCtagMedia
 * @property TstMediaAnswer[] $tstMediaAnswers
 * @property TstMediaQuestion[] $tstMediaQuestions
 * @property TstMediaTest[] $tstMediaTests
 */
class LprMediaContent extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_MIME_TYPE_ID = 'mime_type_id';
    const COL_MEDIA_URI = 'media_uri';
    const COL_LABEL = 'label';
    const COL_DESCRIPTION = 'description';
    const COL_MEDIA_THUMB_URI = 'media_thumb_uri';
    const COL_QGROUP_TAG = 'qgroup_tag';
    const COL_IS_QTAG_USED = 'is_qtag_used';
    const COL_CREATED_ON = 'created_on';
    const COL_UPDATED_ON = 'updated_on';

    const COL_SELECTED_CTAGS = 'selected_ctags';
    const COL_MEDIA_FILE = 'media_file';

    /** @var UploadedFile $media_file */
    public $media_file;

    /** @var  string $selected_ctags */
    public $selected_ctags;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lpr_media_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['mime_type_id', 'is_qtag_used'], 'integer'],
            [['description'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['media_uri', 'media_thumb_uri'], 'string', 'max' => 125],
            [['label'], 'string', 'max' => 256],
            [['media_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['qgroup_tag'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mime_type_id' => 'Mime Type ID',
            'media_uri' => 'Media Uri',
            'label' => 'Label',
            'description' => 'Description',
            'media_file' => 'Media File',
            'selected_ctags' => 'Selected Tags',
            'media_thumb_uri' => 'Media Uri (Thumb)',
            'qgroup_tag' => 'Question Group Tag',
            'is_qtag_used' => 'Is Group Tag Used?',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMimeType()
    {
        return $this->hasOne(TstMimeType::className(), ['id' => 'mime_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstCtagMedia()
    {
        return $this->hasMany(TstCtagMedia::className(), ['media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCtags()
    {
        return $this->hasMany(LprCtag::className(), ['id' => TstCtagMedia::COL_CATEGORY_ID])
            ->viaTable(TstCtagMedia::tableName(), [TstCtagMedia::COL_MEDIA_ID => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstMediaAnswers()
    {
        return $this->hasMany(TstMediaAnswer::className(), ['media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(TstAnswer::className(), ['id' => TstMediaAnswer::COL_ANSWER_ID])
            ->viaTable(TstMediaAnswer::tableName(), [TstMediaAnswer::COL_MEDIA_ID => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstMediaQuestions()
    {
        return $this->hasMany(TstMediaQuestion::className(), ['media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(LprQuestion::className(), ['id' => TstMediaQuestion::COL_QUESTION_ID])
            ->viaTable(TstMediaQuestion::tableName(), [TstMediaQuestion::COL_MEDIA_ID => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstMediaTests()
    {
        return $this->hasMany(TstMediaTest::className(), ['media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTests()
    {
        return $this->hasMany(LprTest::className(), ['id' => TstMediaTest::COL_TEST_ID])
            ->viaTable(TstMediaTest::tableName(), [TstMediaTest::COL_MEDIA_ID => 'id']);
    }

    /**
     * @param $destinationFolder
     * @return bool
     */
    public function process_upload($destinationFolder)
    {
        if ($this->validate())
        {
            /** @var Settings $settings */
            $settings = Yii::$app->settings;
            $upload_dir = $settings->get('file_upload.upload_dir');
            $maxFileSize        = $settings->get('file_upload.max_size') * 1024;

            $newFileName = self::cleanFileName($this->media_file->baseName);
            $ext = $this->media_file->extension;

            $file_name = $upload_dir . '/' . $newFileName . '.' . $ext;
            $file_path = $destinationFolder . 'media/' . $newFileName . '.' . $ext;
            $this->media_file->saveAs($file_name);

            $file_thumb_name = $upload_dir. '/' . $newFileName . '_thumb.' . $ext;
            $file_thumb_path = $destinationFolder . 'media/' . $newFileName . '_thumb.' . $ext;

            $imagine = new Imagine();
            $image = $imagine->open($file_path);
            /** @var Box $oImgSize */
            $oImgSize = $image->getSize();

            // 900x900
            $maxRatio = min(900/$oImgSize->getWidth(), 900/$oImgSize->getHeight());
            $newBox = new Box($oImgSize->getWidth()*$maxRatio, $oImgSize->getHeight()*$maxRatio );

            $image->resize($newBox)->save($file_path);

            $image = $imagine->open($file_path);
            $image->resize(new Box(144, 144))
                ->crop(new Point(0, 0), new Box(144, 144))
                ->save($file_thumb_path);

            return ['is_ok' => true];
        } else {
            $errorMsg = "";
            $errors = $this->getErrors();
            foreach($errors as $errKey => $errVal){
                if(is_array($errVal)){
                    foreach($errVal as $subError){
                        $errorMsg .= sprintf(" %s => %s ",$errKey, $subError);
                    }
                }else{
                    $errorMsg .= sprintf(" %s => %s ",$errKey, $errVal);
                }
            }

            return ['is_ok' => false, 'msg' => 'There were errors processing file "'. $this->media_file->baseName . '": ' . $errorMsg];
        }
    }

    public function saveCTags($selected_ctags)
    {
        // print_r(VarDumper::dumpAsString(['selected_ctags' => $selected_ctags]));

        $this->unlinkAll('ctags', true);

        if(!empty($selected_ctags))
        {
            /** @var array $ctags */
            $ctags = LprCtag::find()->where(['IN', LprCtag::COL_ID, $selected_ctags])->all();

            /** @var LprCtag $ctag */
            foreach($ctags as $ctag)
            {
                $this->link('ctags', $ctag);
            }
        }
    }


    /**
     * @param $fname
     * @return mixed
     */
    public static function cleanFileName($fname)
    {
        $replace="_";
        $pattern='/([[:alnum:]_\.-]*)/';
        $fname=str_replace(str_split(preg_replace($pattern,$replace,$fname)),$replace,$fname);

        return $fname;
    }


    public function getImageThumbUrl(){
        return 'media/' . $this->media_thumb_uri;
    }

    public function getIsTagUsedLabel(){
        return ($this->is_qtag_used === 0) ? 'No' : 'Yes';
    }

    public function getAllCtagsAsLabel(){
        $ctags = $this->getCtags()->asArray()->all();

        return (count($ctags) > 0 ) ?
            join(" / ", ArrayHelper::getColumn($ctags, LprCtag::COL_LABEL ))
            : '- no tags applied -';
    }

    public function rename_media_files($destinationFolder, $oldMediaUri, $oldMediaThumbUri)
    {
        $oldFilePath = $destinationFolder . 'media/' . $oldMediaUri;
        $oldThumbFilePath = $destinationFolder . 'media/' . $oldMediaThumbUri;

        $newFilePath = $destinationFolder . 'media/' . $this->media_uri;
        $newThumbFilePath = $destinationFolder . 'media/' . $this->media_thumb_uri;

        if(!true){
            print_r("<pre>".VarDumper::dumpAsString([
                    'destinationFolder' => $destinationFolder,
                    'oldFilePath' => $oldFilePath,
                    'oldThumbFilePath' => $oldThumbFilePath,
                    'newFilePath' => $newFilePath,
                    'newThumbFilePath' => $newThumbFilePath,
                ])."</pre>"); exit;
        }

        if(file_exists($oldFilePath)){
            rename($oldFilePath, $newFilePath); }

        if(file_exists($oldThumbFilePath)){
            rename($oldThumbFilePath, $newThumbFilePath); }

    }

    /**
     * @param $destFolder
     * @param $degreeAmount
     */
    public function rotateImage($destFolder, $degreeAmount)
    {
        $img_path = $destFolder . 'media/' . $this->media_uri;
        $img_thumb_path = $destFolder . 'media/' . $this->media_thumb_uri;

        $imagine = new Imagine();
        $imagine->open($img_path)->rotate($degreeAmount)->save($img_path);
        $imagine->open($img_thumb_path)->rotate($degreeAmount)->save($img_thumb_path);
        $imagine = null;
    }


    /**
     * @param integer $qid
     * @param array $ctags
     * @param bool $asArray
     * @return \yii\db\ActiveQuery|array
     */
    public static function GetMediaItemsByCtagsForQuestion($qid, $ctags, $asArray = false)
    {
        $sql_tmpl = <<<SQL_STR
            select m.*
            from lpr_media_content m
              join tst_ctag_media cm on cm.media_id = m.id
              join lpr_ctag c on c.id = cm.category_id
            where c.id IN (%s)
              and m.id not in (
                select m2.id
                  from lpr_media_content m2
                      join tst_media_question mq on mq.media_id = m2.id
                      join lpr_question q on mq.question_id = q.id
                  where q.id = :qid );
SQL_STR;

        $sql = sprintf($sql_tmpl, join(", ", $ctags));

        if($asArray){
            return static::findBySql($sql, [':qid' => $qid])->asArray()->all();
        }else{
            return static::findBySql($sql, [':qid' => $qid])->all();
        }
    }


    /**
     * @param integer $qid
     * @param string $qgroup_tag
     * @param bool $asArray
     * @return \yii\db\ActiveQuery|array
     */
    public static function GetMediaItemsByQGgroupTagForQuestion($qid, $qgroup_tag, $asArray = false)
    {
        $sql_tmpl = <<<SQL_STR
            select m.*
            from lpr_media_content m
            where m.qgroup_tag = '%s'
              and m.id not in (
                select m2.id
                  from lpr_media_content m2
                      join tst_media_question mq on mq.media_id = m2.id
                      join lpr_question q on mq.question_id = q.id
                  where q.id = :qid );
SQL_STR;

        $sql = sprintf($sql_tmpl, $qgroup_tag);

        if($asArray){
            return static::findBySql($sql, [':qid' => $qid])->asArray()->all();
        }else{
            return static::findBySql($sql, [':qid' => $qid])->all();
        }
    }


} // end class