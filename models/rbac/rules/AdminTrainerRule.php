<?php

namespace app\models\rbac\rules;

use app\models\db\ext\UserGroups;
use app\models\db\ext\UserGroupsRepository;
use yii\helpers\VarDumper;
use yii\rbac\Item;
use yii\rbac\Rule;
use app\models\db\ext\Organization;


/**
 * Checks if trainer is under prog_coord for organization
 */
class AdminTrainerRule extends Rule
{
    public $name = 'adminOwnTrainer';

    /**
     * @param string|int $user_id the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user_id, $item, $params)
    {
        if (!isset($params['pgc_uid'])
            || !isset($params['trainer_uid'])
            || !isset($params['org_id'])
        ) {
            return false; }

        $pgc_uid     = intval($params['pgc_uid']);
        $trainer_uid = intval($params['trainer_uid']);
        $org_id      = intval($params['org_id']);

        return UserGroupsRepository::ProgCoordHasTrainerInOrganization($org_id, $pgc_uid, $trainer_uid);
    }
}