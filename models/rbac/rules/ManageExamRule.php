<?php

namespace app\models\rbac\rules;

use app\models\db\ext\UserGroupsRepository;
use yii\helpers\VarDumper;
use yii\rbac\Item;
use yii\rbac\Rule;
use app\models\db\ext\Organization;


/**
 * Checks if exam is owned by the user
 */
class ManageExamRule extends Rule
{
    public $name = 'manageOwnExam';

    /**
     * @param string|int $user_id the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user_id, $item, $params)
    {
        return false;

        if(!isset($params['organization'])){
            return false;
        }

        /** @var Organization $org */
        $org = $params['organization'];
        $org_id = $org->id;
        $prog_coord_uid = $user_id;

        $hasProgCoord = UserGroupsRepository::CheckIfOrganizationHasProgCoord($org_id, $prog_coord_uid);

        return $org->is_active && $hasProgCoord;

//        return isset($params['organization']) ? $params['organization']->prog_admin_id == $user_id : false;
    }
}