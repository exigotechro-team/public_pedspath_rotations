<?php

namespace app\models\rbac\roles;

use Yii;

abstract class UserRole
{
    const ADMIN = 'Admin';
    const USER = 'User';

    public static function all()
    {
        return [
            self::ADMIN => \Yii::t('app', 'Admin'),
            self::USER => \Yii::t('app', 'User'),
        ];
    }

    public static function getLabel($role_name)
    {
        $all = self::all();

        if (isset($all[$role_name])) {
            return $all[$role_name];
        }

        return Yii::t('app', 'Not set');
    }
}