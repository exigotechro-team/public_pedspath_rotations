<?php

namespace app\models\rbac\roles;

use app\models\utils\BaseEnum;

class RbacRole extends BaseEnum
{
    const ADMIN                 = 'Admin';
    const PROGRAM_COORDINATOR   = 'Program Coordinator';
    const TRAINER               = 'Trainer';
    const ATTENDING             = 'Attending';
    const TRAINEE               = 'Trainee';
    const RESIDENT              = 'Resident';
    const FELLOW                = 'Fellow';
    const USER                  = 'User';

    public static function getList()
    {
        return [
            self::ADMIN                 => \Yii::t('enum', 'Admin'),
            self::PROGRAM_COORDINATOR   => \Yii::t('enum', 'Program Coordinator'),
            self::TRAINER               => \Yii::t('enum', 'Trainer'),
            self::ATTENDING             => \Yii::t('enum', 'Attending'),
            self::TRAINEE               => \Yii::t('enum', 'Trainee'),
            self::RESIDENT              => \Yii::t('enum', 'Resident'),
            self::FELLOW                => \Yii::t('enum', 'Fellow'),
            self::USER                  => \Yii::t('enum', 'User'),
        ];
    }
}