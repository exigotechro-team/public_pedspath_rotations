<?php

namespace app\models\rbac\permissions;

use app\models\utils\BaseEnum;

class RbacPermission extends BaseEnum
{
    const BE_ADMIN           = 'beAdmin';
    const BE_PROGRAM_ADMIN   = 'beProgramAdmin';

    const ADMIN_TRAINERS     = 'adminTrainers';
    const ADMIN_OWN_TRAINERS = 'adminOwnTrainers';

    const ADMIN_TRAINEES     = 'adminTrainees';
    const ADMIN_OWN_TRAINEES = 'adminOwnTrainees';

    const BE_RESIDENT        = 'beResident';
    const BE_FELLOW          = 'beFellow';
    const BE_TRAINEE         = 'beTrainee';
    const TAKE_TESTS         = 'takeTests';
    const BE_ATTENDING       = 'beAttending';
    const BE_TRAINER         = 'beTrainer';
    const ADMINISTER_TESTS   = 'administerTests';
    const BE_USER            = 'beUser';

    const MANAGE_ORGANIZATION     = 'manageOrganization';
    const MANAGE_OWN_ORGANIZATION = 'manageOwnOrganization';

    const MANAGE_EXAM             = 'manageExam';
    const MANAGE_OWN_EXAM         = 'manageOwnExam';

    public static function getList()
    {
        return [
            self::BE_ADMIN           => \Yii::t('enum', 'beAdmin'),
            self::BE_PROGRAM_ADMIN   => \Yii::t('enum', 'beProgramAdmin'),
            self::ADMIN_TRAINERS     => \Yii::t('enum', 'adminTrainers'),
            self::ADMIN_OWN_TRAINERS => \Yii::t('enum', 'adminOwnTrainers'),
            self::ADMIN_TRAINEES     => \Yii::t('enum', 'adminTrainees'),
            self::ADMIN_OWN_TRAINEES => \Yii::t('enum', 'adminOwnTrainees'),
            self::BE_RESIDENT        => \Yii::t('enum', 'beResident'),
            self::BE_FELLOW          => \Yii::t('enum', 'beFellow'),
            self::BE_TRAINEE         => \Yii::t('enum', 'beTrainee'),
            self::TAKE_TESTS         => \Yii::t('enum', 'takeTests'),
            self::BE_ATTENDING       => \Yii::t('enum', 'beAttending'),
            self::BE_TRAINER         => \Yii::t('enum', 'beTrainer'),
            self::ADMINISTER_TESTS   => \Yii::t('enum', 'administerTests'),
            self::BE_USER            => \Yii::t('enum', 'beUser'),
            self::BE_USER            => \Yii::t('enum', 'beUser'),
            self::MANAGE_ORGANIZATION      => \Yii::t('enum', 'manageOrganization'),
            self::MANAGE_OWN_ORGANIZATION  => \Yii::t('enum', 'manageOwnOrganization'),
            self::MANAGE_EXAM        => \Yii::t('enum', 'manageExam'),
            self::MANAGE_OWN_EXAM    => \Yii::t('enum', 'manageOwnExam'),
        ];
    }
}