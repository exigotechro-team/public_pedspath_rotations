<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tst_mime_type".
 *
 * @property integer $id
 * @property string $ext
 * @property string $type
 *
 * @property LprMediaContent[] $lprMediaContents
 */
class TstMimeType extends \yii\db\ActiveRecord
{

    const COL_ID = 'id';
    const COL_EXT = 'ext';
    const COL_TYPE = 'type';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_mime_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ext', 'type'], 'required'],
            [['ext'], 'string', 'max' => 5],
            [['type'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ext' => 'Ext',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLprMediaContents()
    {
        return $this->hasMany(LprMediaContent::className(), ['mime_type_id' => 'id']);
    }
}
