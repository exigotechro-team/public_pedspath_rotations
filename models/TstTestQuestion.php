<?php

namespace app\models;

use Yii;
use yii\db\Exception;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "tst_test_question".
 *
 * @property integer $id
 * @property integer $test_id
 * @property integer $question_id
 * @property integer $test_order
 * @property integer $rb_rule_id
 * @property string $created_on
 *
 * @property LprQuestion $question
 * @property LprTest $test
 * @property TstTestRule $rbRule
 */
class TstTestQuestion extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_TEST_ID = 'test_id';
    const COL_QUESTION_ID = 'question_id';
    const COL_TEST_ORDER = 'test_order';
    const COL_RB_RULE_ID = 'rb_rule_id';
    const COL_CREATED_ON = 'created_on';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_test_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id', 'question_id', 'test_order'], 'required'],
            [['test_id', 'question_id', 'test_order', 'rb_rule_id'], 'integer'],
            [['created_on'], 'safe'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => LprQuestion::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => LprTest::className(), 'targetAttribute' => ['test_id' => 'id']],
            [['rb_rule_id'], 'exist', 'skipOnError' => true, 'targetClass' => TstTestRule::className(), 'targetAttribute' => ['rb_rule_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'question_id' => 'Question ID',
            'test_order' => 'Test Order',
            'rb_rule_id' => 'Rb Rule ID',
            'created_on' => 'Created On',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestRule()
    {
        return $this->getRbRule();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(LprQuestion::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbRule()
    {
        return $this->hasOne(TstTestRule::className(), ['id' => 'rb_rule_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(LprTest::className(), ['id' => 'test_id']);
    }


    /**
     * @param $qids
     * @param $rb_rule_id
     * @param $test_id
     */
    public static function CreateTestQuestionsForRbRuleId($qids, $rb_rule_id, $test_id)
    {
        $columns = [
            TstTestQuestion::COL_TEST_ID,
            TstTestQuestion::COL_QUESTION_ID,
            TstTestQuestion::COL_RB_RULE_ID,
        ];

        $rows = [];

        foreach ($qids as $q_id) {
            $rows[] = [$test_id, $q_id, $rb_rule_id];
        }

        try {
            Yii::$app->db
                ->createCommand()
                ->batchInsert(TstTestQuestion::tableName(), $columns, $rows)
                ->execute();
        } catch (Exception $e) {
        }
    }


    /**
     * @param int $test_id
     * @param int $question_id
     * @param int|null $rb_rule_id
     * @return TstTestQuestion
     */
    public static function CreateTestQuestion($test_id, $question_id, $rb_rule_id=null)
    {
        /** @var TstTestQuestion $test_question */
        $test_question = new TstTestQuestion();
        $test_question->test_id = intval($test_id);
        $test_question->question_id = intval($question_id);
        $test_question->rb_rule_id = (!empty($rb_rule_id)) ? intval($rb_rule_id) : null;
        $test_question->test_order = 0;

        $test_question->save();

        if (!true) {
            print_r("<pre>" . VarDumper::dumpAsString([
                    'model' => $test_question->toArray(),
                ]) . "</pre>");
            // exit;
        }

        return $test_question;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_on = date("Y-m-d H:i:s"); // time();
        } elseif (!$this->isNewRecord && empty($this->created_on)) {
            $this->created_on = date("Y-m-d H:i:s"); // time();
        }

        return parent::beforeSave($insert);
    }

}