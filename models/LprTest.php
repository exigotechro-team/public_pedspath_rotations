<?php

namespace app\models;

use dektrium\user\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "lpr_test".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $status
 * @property integer $duration_sec
 * @property integer $pass_score
 * @property integer $total_score
 * @property string $search_text
 * @property boolean $has_media
 * @property string $created_on
 * @property string $updated_on
 * @property integer $edit_status
 * @property integer $access_level
 * @property integer $is_locked
 * @property integer $owner_id
 * @property integer $rb_id
 * @property integer $is_template
 * @property integer $cloned_from
 *
 * @property RuleBook $rb
 * @property User $owner
 * @property TstTestUser[] $tstTestUsers
 * @property TstCtagTest[] $tstCtagTests
 * @property TstMediaTest[] $tstMediaTests
 * @property TstTestQuestion[] $tstTestQuestions
 * @property TstTestRule[] $tstTestRules
 */

class LprTest extends \yii\db\ActiveRecord
{
    const COL_ID = 'id';
    const COL_TITLE = 'title';
    const COL_DESCRIPTION = 'description';
    const COL_STATUS = 'status';
    const COL_DURATION_SEC = 'duration_sec';
    const COL_PASS_SCORE = 'pass_score';
    const COL_TOTAL_SCORE = 'total_score';
    const COL_SEARCH_TEXT = 'search_text';
    const COL_HAS_MEDIA = 'has_media';
    const COL_CREATED_ON = 'created_on';
    const COL_UPDATED_ON = 'updated_on';
    const COL_EDIT_STATUS = 'edit_status';
    const COL_ACCESS_LEVEL = 'access_level';
    const COL_IS_LOCKED = 'is_locked';
    const COL_OWNER_ID = 'owner_id';
    const COL_RB_ID = 'rb_id';
    const COL_IS_TEMPLATE = 'is_template';
    const COL_CLONED_FROM = 'cloned_from';

    const STATUS_DRAFT          = 'DRAFT';
    const STATUS_PUBLISHED      = 'PUBLISHED';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lpr_test';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'description', 'search_text'], 'string'],
            [['duration_sec', 'pass_score', 'total_score', 'edit_status', 'access_level', 'is_locked', 'owner_id'/*, 'rb_id'*/, 'is_template'], 'integer'],
            [['has_media'], 'boolean'],
            [['created_on', 'updated_on'], 'safe'],
            [['status'], 'string', 'max' => 16],
            [['rb_id'], 'exist', 'skipOnError' => true, 'targetClass' => RuleBook::className(), 'targetAttribute' => ['rb_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'duration_sec' => 'Duration Sec',
            'pass_score' => 'Pass Score',
            'total_score' => 'Total Score',
            'search_text' => 'Search Text',
            'has_media' => 'Has Media',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'edit_status' => 'Edit Status',
            'access_level' => 'Access Level',
            'is_locked' => 'Is Locked',
            'owner_id' => 'Owner ID',
            'rb_id' => 'Rule Book ID',
            'is_template' => 'Is Template',
            'cloned_from' => 'Cloned From',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCtags()
    {
        return $this->hasMany(LprCtag::className(), ['id' => TstCtagTest::COL_CATEGORY_ID])
            ->viaTable(TstCtagTest::tableName(), [TstCtagTest::COL_TEST_ID => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstCtagTests()
    {
        return $this->hasMany(TstCtagTest::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstTestUsers()
    {
        return $this->hasMany(TstTestUser::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstMediaTests()
    {
        return $this->hasMany(TstMediaTest::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMediaItems()
    {
        return $this->hasMany(LprMediaContent::className(), ['id' => TstMediaTest::COL_MEDIA_ID])
            ->viaTable(TstMediaTest::tableName(), [TstMediaTest::COL_TEST_ID => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstTestQuestions()
    {
        return $this->hasMany(TstTestQuestion::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(LprQuestion::className(), ['id' => TstTestQuestion::COL_QUESTION_ID])
            ->viaTable(TstTestQuestion::tableName(), [TstTestQuestion::COL_TEST_ID => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTstTestRules()
    {
        return $this->hasMany(TstTestRule::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRb()
    {
        return $this->hasOne(\app\models\db\ext\RuleBook::className(), ['id' => 'rb_id']);
    }


    /**
     * @param $rb_rule_id int
     * @param $qids_in array
     * @return array
     *
     * Finds all questions for RB_RULE_ID that are not already in the QIDS_IN array
     */
    public function getAvailableQuestionsForRBRule($rb_rule_id, $qids_in)
    {
        /** @var TstTestRule $rb_rule */
        $rb_rule = TstTestRule::findOne([ TstTestRule::COL_ID => $rb_rule_id])->toArray();

        if(empty($rb_rule)){ return []; };

        $sql = <<<SQL_STR
            SELECT
              q.id AS qid
            FROM lpr_question q
              JOIN tst_ctag_question q2ct ON q2ct.question_id = q.id
              JOIN lpr_ctag ct ON ct.id = q2ct.category_id
            WHERE ct.id = :categ_id
                AND q.difficulty = :diffic_id
                AND q.id NOT IN (
                    SELECT tq.question_id AS qid 
                    FROM tst_test_question tq 
                    WHERE tq.test_id = :test_id
                      AND tq.rb_rule_id IS NULL 
                )
SQL_STR;

        $sql_sub_tmpl = "\n  AND q.id NOT IN ( %s ) \n";
        $sql_sub = "";

        if(!empty($qids_in) && count($qids_in) > 0)
        {
            $sql_sub .=  sprintf("%u", intval( array_shift($qids_in) ));

            foreach ($qids_in as $qid_in){
                $sql_sub .= sprintf(", %u", intval( $qid_in) ); }

            $sql_sub = sprintf($sql_sub_tmpl, $sql_sub);

            $sql = $sql . $sql_sub;
        }

//        $sql = $sql . sprintf("\n limit 0,%s; ", $rb_rule[TstTestRule::COL_Q_COUNT]);

        /** @var ActiveQuery $question_query */
        $question_query = $this->findBySql($sql, [
            ':categ_id' => intval($rb_rule[TstTestRule::COL_CTAG_ID]),
            ':diffic_id' => intval($rb_rule[TstTestRule::COL_DIFFICULTY]),
            ':test_id' => intval($this->id),
        ]);

        $qids = $question_query->asArray()->all();

//        if (true) { print "\n<pre>" . VarDumper::dumpAsString(['model' => $question_query->createCommand()->rawSql,]) . "</pre>\n";}

        $qsCounter = $rb_rule[TstTestRule::COL_Q_COUNT];
        $maxCeiling = count($qids) - 1;

        $idxs = [];
        $new_qids = [];

        for ($i = 0; $i < $qsCounter; $i++) {
            $done = false;

            while (!$done) {
                $idx = rand(0, $maxCeiling);
                $idxKey = sprintf("idx%s", $idx);

                if (!array_key_exists($idxKey, $idxs)) {
                    $idxs[$idxKey] = $idx;
                    $new_qids[] = intval($qids[$idx]['qid']);
                    $done = true;
                }
            }
        }

        return $new_qids;
    }

    /**
     * This function will:
     *  this_test_obj -> rb_id -> rb_rule_ids[]
     *      -> fetch qs for each rule
     *      -> associate qs to test
     * @param array $rb_rule_ids
     * @return array|null
     */
    public function getAvailableQuestionsForRuleBook($rb_rule_ids = [])
    {
        $rb_rules_aq = TstTestRule::find()->where(['rb_id' => $this->rb_id]);

        if (!empty($rb_rule_ids)) {
            $rb_rules_aq->andWhere(['IN', TstTestRule::COL_ID, $rb_rule_ids]);
        }

        $rb_rules = $rb_rules_aq->all();

        if(empty($rb_rules) || count($rb_rules) == 0){
            return null; }

        $qids = [];

        $rb_qids = [];
        $rb_kids_key_tmpl = 'rb_rid_%s';

        /** @var TstTestRule $rb_rule */
        foreach ( $rb_rules as $rb_rule )
        {
            $tmp_qids = $this->getAvailableQuestionsForRBRule($rb_rule->id, $qids);
            $qids = ArrayHelper::merge($qids, $tmp_qids);

            $tmp_key = sprintf($rb_kids_key_tmpl, $rb_rule->id);
            $rb_qids[$tmp_key] = $tmp_qids;

            if (0 || !true)
            {
                print_r("<pre>" . VarDumper::dumpAsString([
                        'tmp_qids' => $tmp_qids,
                        'qids' => $qids,
                        '-' => "=========================================",
                    ]) . "</pre>");
            }

            $tmp_qids = null;
            $tmp_key = null;
        }

//        return $qids;
        return $rb_qids;
    }

    /**
     * @return bool
     */
    public function isTestAlreadyDelivered()
    {
        $userTestsCount = TstTestUser::find()
            ->select([TstTestUser::COL_ID])
            ->where([TstTestUser::COL_TEST_ID => $this->id])
            ->count();

        $isTestDelivered = (intval($userTestsCount) > 0) ? true : false;
        return $isTestDelivered;
    }

    /**
     * @param array $rb_rule_ids
     */
    public function clearTestQuestionsFromRuleBookRules($rb_rule_ids = [])
    {
        $ttqs = TstTestQuestion::find()
            ->where([TstTestQuestion::COL_TEST_ID => $this->id])
            ->andWhere(['NOT', [TstTestQuestion::COL_RB_RULE_ID => null]]);

        if (!empty($rb_rule_ids)) {
            $ttqs->andWhere(['IN', TstTestQuestion::COL_RB_RULE_ID, $rb_rule_ids]);
        }

        /** @var TstTestQuestion $ttqObj */
        foreach ($ttqs->each(100) as $ttqObj) {
            try {
                $ttqObj->delete();
            } catch (StaleObjectException $e) {
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
        }
    }


    /**
     * @param array $rb_rule_ids
     * @return \yii\db\ActiveQuery
     */
    public function getTestQuestionsFromRuleBookRules($rb_rule_ids = [])
    {
        $ttqs = TstTestQuestion::find()
            ->where([TstTestQuestion::COL_TEST_ID => $this->id])
            ->andWhere(['NOT', [TstTestQuestion::COL_RB_RULE_ID => null]]);

        if (!empty($rb_rule_ids)) {
            $ttqs->andWhere(['IN', TstTestQuestion::COL_RB_RULE_ID, $rb_rule_ids]);
        }

        if (!true) {
            /** @var TstTestQuestion $ttqObj */
            foreach ($ttqs->each(100) as $ttqObj) {
                echo ""; //$ttqObj->delete();
            }
        }

        return $ttqs;
   }


    /**
     * The actual question-to-test association code  (new TstTestQuestion)
     * @param $rb_rid_key
     * @param $rb_rule_qids
     */
    public function addQuestionFromRuleToTest($rb_rid_key, $rb_rule_qids)
    {
        $rb_rid_key_parts = explode("_", $rb_rid_key);
        $rb_rid = array_pop($rb_rid_key_parts); // rb_rid_key is of form rb_rid_%u

        TstTestQuestion::CreateTestQuestionsForRbRuleId($rb_rule_qids, $rb_rid, $this->id);
    }


    /**
     * @param $quiz_id null|int
     * @param $rb_rule_id null|int
     * @throws \yii\db\Exception
     */
    public static function DeleteTestQuestionsFromRBRule($quiz_id, $rb_rule_id)
    {
        $sql = <<<SQLSTR
            DELETE FROM tst_test_question 
            WHERE test_id = :test_id 
            AND rb_rule_id = :rb_rule_id;
SQLSTR;

        Yii::$app->db->createCommand($sql)
            ->bindValue(':test_id', $quiz_id)
            ->bindValue(':rb_rule_id', $rb_rule_id)
            ->execute();

    }

    /**
     * @param $quiz_id null|int
     * @param $rb_rule_id null|int
     */
    public static function DisconnectTestQuestionsFromRBRule($quiz_id, $rb_rule_id)
    {
        $sql = <<<SQLSTR
            UPDATE tst_test_question
             SET rb_rule_id = NULL
            WHERE test_id = :test_id 
            AND rb_rule_id = :rb_rule_id;
SQLSTR;

        Yii::$app->db->createCommand($sql)
            ->bindValue(':test_id', $quiz_id)
            ->bindValue(':rb_rule_id', $rb_rule_id)
            ->execute();

    }

    /**
     * @param $rb_rule_id int
     * @return int|mixed
     */
    public static function GetQuizIdFromRbRuleId($rb_rule_id)
    {
        $sql = <<<SQLSTR
            select t.id from lpr_test t where t.rb_id = (
                select rb_id from tst_test_rule where id = :rb_rule_id
            )
            LIMIT 0,1;
SQLSTR;

        /** @var int|mixed $quiz_id */
        $quiz_id = Yii::$app->db->createCommand($sql)
            ->bindValue(':rb_rule_id', $rb_rule_id)
            ->queryScalar();

        return intval($quiz_id);
    }



    /**
     * @param $rb_rule_id int
     * @return LprTest
     */
    public static function GetQuizFromRbRuleId($rb_rule_id)
    {
        $sql = <<<SQLSTR
            SELECT t.* 
            FROM lpr_test t 
            WHERE t.rb_id = (
                SELECT rb_id 
                FROM tst_test_rule 
                WHERE id = :rb_rule_id
            )
            LIMIT 0,1;
SQLSTR;

        /** @var LprTest $quiz_model */
        $quiz_model = LprTest::findBySql($sql, [':rb_rule_id' => $rb_rule_id])->one();

        return $quiz_model;
    }


    public function getSummaryAvailableNoOfQsForRbRule($rb_rule_id)
    {

    }


    /**
     * @param $rb_rule array
     * @return mixed
     */
    public function getQuickStatsNoOfQsForRbRule($rb_rule)
    {

        $sql = <<<SQLSTR
            SELECT
              ct.id AS ctag_id,
              ct.label AS ctag_label,
              q.difficulty AS q_difficulty,
              COUNT(q.id) AS no_of_qs
            FROM lpr_question q
              JOIN tst_ctag_question q2ct ON q2ct.question_id = q.id
              JOIN lpr_ctag ct ON ct.id = q2ct.category_id
            WHERE ct.id = :categ_id
                AND q.difficulty = :diffic_id
                AND q.id NOT IN (
                    SELECT tq.question_id AS qid
                    FROM tst_test_question tq
                    WHERE tq.test_id = :test_id
                      -- AND tq.rb_rule_id IS NULL
                )
            GROUP BY ct.id;
SQLSTR;

        /** @var array $rb_rule_stats */
        $rb_rule_stats = $this->findBySql($sql, [
            ':categ_id' => $rb_rule[TstTestRule::COL_CTAG_ID],
            ':diffic_id' => $rb_rule[TstTestRule::COL_DIFFICULTY],
            ':test_id' => $this->id,
        ])->asArray()->one();



        if(!isset($rb_rule_stats))
        {
            $rb_rule_stats = [
                'ctag_id' => intval($rb_rule[TstTestRule::COL_CTAG_ID]),
                'ctag_label' => '',
                'q_difficulty' => intval($rb_rule[LprQuestion::COL_DIFFICULTY]),
                'no_of_qs' => 0
            ];
        }

        return $rb_rule_stats;
    }


    /**
     * @param $rb_rule_id int
     * @return array
     */
    public function selectQuestionsForRbRule($rb_rule_id)
    {
        /** @var TstTestRule $rb_rule */
        $rb_rule = TstTestRule::find()->where([ TstTestRule::COL_ID => $rb_rule_id])->one();
        if(empty($rb_rule)){ return []; };

        $sql = <<<SQL_STR
            SELECT
              q.id AS qid
            FROM lpr_question q
              JOIN tst_ctag_question q2ct ON q2ct.question_id = q.id
              JOIN lpr_ctag ct ON ct.id = q2ct.category_id
            WHERE ct.id = :categ_id
                AND q.difficulty = :diffic_id
                AND q.id NOT IN (
                    SELECT tq.question_id AS qid 
                    FROM tst_test_question tq 
                    WHERE tq.test_id = :test_id
                      -- AND tq.rb_rule_id IS NULL 
                )
SQL_STR;

        $sql = $sql . sprintf("\n limit 0,%s; ", $rb_rule->q_count);

        $qids = $this->findBySql($sql, [
            ':categ_id' => $rb_rule->ctag_id,
            ':diffic_id' => $rb_rule->difficulty,
            ':test_id' => $this->id,
        ])->asArray()->all();

        $new_qids = [];

        foreach ($qids as $qid) {
            $new_qids[] = $qid['qid'];
        }

        return $new_qids;
    }


    public function addQuestionsToQuiz($rb_rule_id)
    {
        /** @var array $qids */
        $qids = $this->selectQuestionsForRbRule($rb_rule_id);

        if(empty($qids)){
            return sprintf('No questions added to quizfrom rule: %s!', $rb_rule_id); }

        TstTestQuestion::CreateTestQuestionsForRbRuleId($qids, $rb_rule_id, $this->id);

        return '';
    }


    /**
     * @param array $rb_rule_ids
     * @deprecated
     */
    public function clearQuestionsBasedOnRuleBookRules_old($rb_rule_ids = [])
    {
        $sql = <<<SQLSTR
            DELETE FROM tst_test_question ttq 
            WHERE ttq.test_id = :test_id 
            AND ttq.rb_rule_id IS NOT NULL;
SQLSTR;

        Yii::$app->db->createCommand($sql)
            ->bindValue(':test_id', $this->id)
            ->execute();
    }



} // end class