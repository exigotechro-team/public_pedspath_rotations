<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tst_ctag_test".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $test_id
 *
 * @property LprCtag $category
 * @property LprTest $test
 */
class TstCtagTest extends \yii\db\ActiveRecord
{

    const COL_ID = 'id';
    const COL_CATEGORY_ID = 'category_id';
    const COL_TEST_ID = 'test_id';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tst_ctag_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'test_id'], 'required'],
            [['id', 'category_id', 'test_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'test_id' => 'Test ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(LprCtag::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(LprTest::className(), ['id' => 'test_id']);
    }
}
