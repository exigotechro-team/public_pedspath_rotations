<?php

namespace app\commands;

use app\models\rbac\rules\AdminTraineeRule;
use app\models\rbac\rules\AdminTrainerRule;
use app\models\rbac\rules\ManageExamRule;
use app\models\rbac\rules\ManageOrganizationRule;
use Yii;
use yii\console\Controller;
use app\models\rbac\permissions\RbacPermission;
use app\models\rbac\roles\RbacRole;

class RbacAdminController extends Controller
{
    /**
     * ./yii rbac-admin/init
     * @param integer $admin_uid SuperAdmin ID
     * @return bool
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionInit($admin_uid = null)
    {

        if (!$this->confirm("Are you sure? It will re-create permissions tree.")) {
            return self::EXIT_CODE_NORMAL;
        }

        $auth = Yii::$app->authManager;
        $auth->removeAll();

        // USER
        // -------------------------------------------------------------------------------
        $userRole = $auth->createRole(RbacRole::USER);
        $userRole->description = RbacRole::USER;
        $auth->add($userRole);

        $beUserPermission = $auth->createPermission(RbacPermission::BE_USER);
        $beUserPermission->description = RbacPermission::BE_USER;
        $auth->add($beUserPermission);
        $auth->addChild($userRole, $beUserPermission);


        // add the manageOwnExamRule -----------------------------------------------------------------------------------
        $manageOwnExamRule = new ManageExamRule();
        $auth->add($manageOwnExamRule);

        $manageOwnExamPermission = $auth->createPermission(RbacPermission::MANAGE_OWN_EXAM);
        $manageOwnExamPermission->description = sprintf("Desc: %s",RbacPermission::MANAGE_OWN_EXAM);
        $manageOwnExamPermission->ruleName = $manageOwnExamRule->name;
        $auth->add($manageOwnExamPermission);
        $auth->addChild($userRole, $manageOwnExamPermission);

        $manageExamPermission = $auth->createPermission(RbacPermission::MANAGE_EXAM);
        $manageExamPermission->description = sprintf("Desc: %s",RbacPermission::MANAGE_EXAM);
        $auth->add($manageExamPermission);
        $auth->addChild($manageOwnExamPermission, $manageExamPermission);


        // ADMIN
        // -------------------------------------------------------------------------------
        $adminRole = $auth->createRole(RbacRole::ADMIN);
        $adminRole->description = RbacRole::ADMIN;
        $auth->add($adminRole);

        $beAdminPermission = $auth->createPermission(RbacPermission::BE_ADMIN);
        $beAdminPermission->description = RbacPermission::BE_ADMIN;
        $auth->add($beAdminPermission);
        $auth->addChild($adminRole, $beAdminPermission);

        // TRAINER
        // -------------------------------------------------------------------------------
        $trainerRole = $auth->createRole(RbacRole::TRAINER);
        $trainerRole->description = RbacRole::TRAINER;
        $auth->add($trainerRole);

        $beTrainerPermission = $auth->createPermission(RbacPermission::BE_TRAINER);
        $beTrainerPermission->description = RbacPermission::BE_TRAINER;
        $auth->add($beTrainerPermission);
        $auth->addChild($trainerRole, $beTrainerPermission);

        $canGiveTestsPermission = $auth->createPermission(RbacPermission::ADMINISTER_TESTS);
        $canGiveTestsPermission->description = RbacPermission::ADMINISTER_TESTS;
        $auth->add($canGiveTestsPermission);
        $auth->addChild($trainerRole, $canGiveTestsPermission);


        // add the adminTraineeRule -----------------------------------------------------------------------------------
        $adminTraineeRule = new AdminTraineeRule();
        $auth->add($adminTraineeRule);

        $adminOwnTraineesPermission = $auth->createPermission(RbacPermission::ADMIN_OWN_TRAINEES);
        $adminOwnTraineesPermission->description = sprintf("Desc: %s",RbacPermission::ADMIN_OWN_TRAINEES);
        $adminOwnTraineesPermission->ruleName = $adminTraineeRule->name;
        $auth->add($adminOwnTraineesPermission);
        $auth->addChild($trainerRole, $adminOwnTraineesPermission);

        $adminTraineesPermission = $auth->createPermission(RbacPermission::ADMIN_TRAINEES);
        $adminTraineesPermission->description = RbacPermission::ADMIN_TRAINEES;
        $auth->add($adminTraineesPermission);
        $auth->addChild($adminOwnTraineesPermission, $adminTraineesPermission);


        // TRAINEE
        // -------------------------------------------------------------------------------
        $traineeRole = $auth->createRole(RbacRole::TRAINEE);
        $traineeRole->description = RbacRole::TRAINEE;
        $auth->add($traineeRole);

        $beTraineePermission = $auth->createPermission(RbacPermission::BE_TRAINEE);
        $beTraineePermission->description = RbacPermission::BE_TRAINEE;
        $auth->add($beTraineePermission);
        $auth->addChild($traineeRole, $beTraineePermission);

        $canTakeTestsPermission = $auth->createPermission(RbacPermission::TAKE_TESTS);
        $canTakeTestsPermission->description = RbacPermission::TAKE_TESTS;
        $auth->add($canTakeTestsPermission);
        $auth->addChild($traineeRole, $canTakeTestsPermission);

        // ATTENDING
        // -------------------------------------------------------------------------------
        $attendingRole = $auth->createRole(RbacRole::ATTENDING);
        $attendingRole->description = RbacRole::ATTENDING;
        $auth->add($attendingRole);

        $beAttendingPermission = $auth->createPermission(RbacPermission::BE_ATTENDING);
        $beAttendingPermission->description = RbacPermission::BE_ATTENDING;
        $auth->add($beAttendingPermission);

        $auth->addChild($attendingRole, $beAttendingPermission);

        // FELLOW
        // -------------------------------------------------------------------------------
        $fellowRole = $auth->createRole(RbacRole::FELLOW);
        $fellowRole->description = RbacRole::FELLOW;
        $auth->add($fellowRole);

        $beFellowPermission = $auth->createPermission(RbacPermission::BE_FELLOW);
        $beFellowPermission->description = RbacPermission::BE_FELLOW;
        $auth->add($beFellowPermission);

        $auth->addChild($fellowRole, $beFellowPermission);

        // RESIDENT
        // -------------------------------------------------------------------------------
        $residentRole = $auth->createRole(RbacRole::RESIDENT);
        $residentRole->description = RbacRole::RESIDENT;
        $auth->add($residentRole);

        $beResidentPermission = $auth->createPermission(RbacPermission::BE_RESIDENT);
        $beResidentPermission->description = RbacPermission::BE_RESIDENT;
        $auth->add($beResidentPermission);

        $auth->addChild($residentRole, $beResidentPermission);

        // PROGRAM_COORDINATOR
        // -------------------------------------------------------------------------------
        $progAdminRole = $auth->createRole(RbacRole::PROGRAM_COORDINATOR);
        $progAdminRole->description = RbacRole::PROGRAM_COORDINATOR;
        $auth->add($progAdminRole);

        $beProgAdminPermission = $auth->createPermission(RbacPermission::BE_PROGRAM_ADMIN);
        $beProgAdminPermission->description = RbacPermission::BE_PROGRAM_ADMIN;
        $auth->add($beProgAdminPermission);
        $auth->addChild($progAdminRole, $beProgAdminPermission);


        // add the adminTrainerRule -----------------------------------------------------------------------------------
        $adminTrainerRule = new AdminTrainerRule();
        $auth->add($adminTrainerRule);

        $adminOwnTrainersPermission = $auth->createPermission(RbacPermission::ADMIN_OWN_TRAINERS);
        $adminOwnTrainersPermission->description = sprintf("Desc: %s",RbacPermission::ADMIN_OWN_TRAINERS);
        $adminOwnTrainersPermission->ruleName = $adminTrainerRule->name;
        $auth->add($adminOwnTrainersPermission);
        $auth->addChild($progAdminRole, $adminOwnTrainersPermission);

        $adminTrainersPermission = $auth->createPermission(RbacPermission::ADMIN_TRAINERS);
        $adminTrainersPermission->description = RbacPermission::ADMIN_TRAINERS;
        $auth->add($adminTrainersPermission);
        $auth->addChild($adminOwnTrainersPermission, $adminTrainersPermission);


        // add the manageOwnOrganizationRule ---------------------------------------------------------------------------
        $manageOwnOrganizationRule = new ManageOrganizationRule;
        $auth->add($manageOwnOrganizationRule);

        $manageOwnOrganizationPermission = $auth->createPermission(RbacPermission::MANAGE_OWN_ORGANIZATION);
        $manageOwnOrganizationPermission->description = RbacPermission::MANAGE_OWN_ORGANIZATION;
        $manageOwnOrganizationPermission->ruleName = $manageOwnOrganizationRule->name;
        $auth->add($manageOwnOrganizationPermission);
        $auth->addChild($progAdminRole, $manageOwnOrganizationPermission);

        $manageOrganizationPermission = $auth->createPermission(RbacPermission::MANAGE_ORGANIZATION);
        $manageOrganizationPermission->description = RbacPermission::MANAGE_ORGANIZATION;
        $auth->add($manageOrganizationPermission);
        $auth->addChild($manageOwnOrganizationPermission, $manageOrganizationPermission);



        // -------------------------------------------------------------------------------

        // build thre TRAINER branch, under USER at top
        $auth->addChild($trainerRole, $userRole);
        $auth->addChild($attendingRole, $trainerRole);
        $auth->addChild($progAdminRole, $attendingRole);
        $auth->addChild($adminRole, $progAdminRole);

        $auth->addChild($adminRole, $manageOrganizationPermission);
        $auth->addChild($adminRole, $manageExamPermission);
        $auth->addChild($adminRole, $adminTrainersPermission);
        $auth->addChild($adminRole, $adminTraineesPermission);

        // build thre TRAINEE branch, under USER at top
        $auth->addChild($traineeRole, $userRole);
        $auth->addChild($residentRole, $traineeRole);
        $auth->addChild($fellowRole, $traineeRole);


        // Assign roles to users. 1 is assumed to be the first user created and hence the super admin

        if ($admin_uid !== null) {
            $auth->assign($adminRole, $admin_uid);
        }else{
            $auth->assign($adminRole, 1);
        }

        $auth->assign($progAdminRole, 48);
        $auth->assign($progAdminRole, 49);
        $auth->assign($progAdminRole, 50);
        $auth->assign($progAdminRole, 51);
        $auth->assign($progAdminRole, 52);

        echo "Ok\n";

        return true;

    }

} // end class