<?php

namespace app\commands;

use app\models\db\ext\UserGroupsRepository;
use app\models\rbac\roles\RbacRole;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use yii\base\InvalidConfigException;
use yii\console\Controller;


class UserMgmtController extends Controller
{

    /**
     *
     * @example ./yii user-mgmt/update-trainee-user 48 60 "Jimmy Johns"
     *
     * @param int $trainer_id
     * @param int $trainee_id
     * @param string $user_full_name
     */
    public function actionUpdateTraineeUser($trainer_id, $trainee_id, $user_full_name)
    {
        /** @var User $user */
        $user = User::findOne(['id' => $trainee_id]);

        if(empty($user)){
            echo sprintf("Trainee: %s does NOT exist! \n", $trainee_id);
            exit;
        }

        echo sprintf(">>> Processing %s (id: %s)...", $user->username, $user->id);
        echo sprintf("\t [ Full_Name: %s , User_ID: %s]\n", $user_full_name, $trainee_id);

        if(intval($user->id) != intval($trainee_id)){
            echo "No match!"; exit; }

        if (true) {
            UserGroupsRepository::AddTraineeToTrainer($trainer_id, $user->id);

            $auth = \Yii::$app->authManager;
            $userRole = $auth->getRole(RbacRole::TRAINEE);
            try {
                $auth->assign($userRole, $user->id);
            } catch (\Exception $e) {
            }

            $profile = $user->profile;

            if ($profile == null) {
                try {
                    $profile = \Yii::createObject(Profile::className());
                } catch (InvalidConfigException $e) {
                }
                $profile->link('user', $user);
            }

            $profile->name = $user_full_name;
            $profile->save();
        } // end if ! true
    }


} // end class