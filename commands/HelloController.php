<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\db\ext\UserGroupsRepository;
use app\models\LprCtag;
use app\models\LprMediaContent;
use app\models\LprQuestion;
use app\models\LprTest;
use app\models\TstTestRule;
use DateTime;
use DateTimeZone;
use dektrium\user\models\Profile;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        Yii::$app->redis->set('mykey', 'some value');
        echo Yii::$app->redis->get('mykey') . "\n";

        Yii::$app->session->set('mykey3', 'some value3');
        echo Yii::$app->session->get('mykey3') . "\n";

        Yii::$app->cache->set('mykey2', 'some value2');
        echo Yii::$app->cache->get('mykey2') . "\n";

        echo $message . "\n";
    }


    /**
     * @example ./yii hello/index2
     */
    public function actionIndex2()
    {
        echo Profile::className() ."\n";
        echo Profile::class ."\n";
    }

    /**
     * @param string $parentCtag
     */
    public function actionListCtags($parentCtag = 'PPM')
    {
        $ctags = LprCtag::GetAllCtagsForParentWithTag($parentCtag, true);
        print_r(VarDumper::dumpAsString(['pTag' => $parentCtag , 'ctags' => $ctags])."\n");
    }

    public function actionDeleteMediaItem($id)
    {
        /** @var LprMediaContent $model */
        $model = LprMediaContent::findOne([LprMediaContent::COL_ID => $id]);

        if(!empty($model))
        {
            $baseAppPath = Yii::$app->basePath;
            $fullMediaDirPath = FileHelper::normalizePath(Yii::getAlias('@app') . '/web');

            $fileUri = join('/', [$fullMediaDirPath , $model->media_uri]);
            $fileThumbUri = join('/', [$fullMediaDirPath , $model->media_thumb_uri]);

            foreach([$fileUri, $fileThumbUri] as $fileName)
            {
                if(file_exists($fileName)){
                    print_r(VarDumper::dumpAsString(['msg' => $fileName . ' EXISTS!!!']));
                    unlink($fileName);
                }
            }

            if(true){
                print_r("\n<pre>\n" . VarDumper::dumpAsString([
                        'fileUri' => $fileUri,
                        'fileThumbUri' => $fileThumbUri,
                        'baseAppPath' => $baseAppPath,
                        'fullMediaDirPath' => $fullMediaDirPath,
                        'model' => $model->toArray(),
                    ]) . "\n</pre>\n");
                exit;
            }

            $model->unlinkAll('ctags', true);

            $model->delete();
        } // end fi model
    } // end action

    public function actionLoadAnswersForQuestion($qid)
    {
        /** @var LprQuestion $model */
        $model = LprQuestion::findOne([LprMediaContent::COL_ID => $qid]);

        if(empty($model)){
            print_r(sprintf("no question with qid:%s \n", $qid)); exit; }

        $answers = [];
        $answers_file = '/tmp/my_file_answers';

        if(file_exists($answers_file))
        {   $file_content = trim(file_get_contents($answers_file));
            $answers = explode(PHP_EOL, $file_content);
        }

        $dtzChicago = new DateTimeZone('America/Chicago');
        $dtzUTC = new DateTimeZone('UTC');
        $dtUTC = new DateTime("now", $dtzUTC);
        $dtOffset = $dtzChicago->getOffset($dtUTC);

        if(true){ print_r(VarDumper::dumpAsString([
            'model' => $model->toArray(),
            'answers' => $answers,
            'offset Chicago - UTC' => $dtOffset,
        ])); exit; } // end if true


    }

    /**
     * @param string $localTzStr
     *
     * usage:
     *  ./yii hello/convert-date-time-to-timezones America/Chicago
     *  ./yii hello/convert-date-time-to-timezones America/New_York
     *  ./yii hello/convert-date-time-to-timezones America/Los_Angeles
     */
    public function actionConvertDateTimeToTimezones($localTzStr = 'America/Chicago')
    {
        $triggerOn = "now";

        $localTz = new DateTimeZone($localTzStr);
        $utcTz = new DateTimeZone('UTC');

        $nowLocalDt = new DateTime($triggerOn, $localTz );
        $localDtStr =  $nowLocalDt->format('Y-m-d H:i:s');

        $nowLocalDt->setTimeZone($utcTz);
        $utcDtStr =  $nowLocalDt->format('Y-m-d H:i:s');

        $dtOffset = $localTz->getOffset($nowLocalDt)/3600;

        if(true){ print_r(VarDumper::dumpAsString([
            'localDtStr' => $localDtStr,
            'utcDtStr' => $utcDtStr,
            'offset Local - UTC' => $dtOffset,
        ])); exit; } // end if true
    }


    public function actionBuildSppImageFetcher($year = null)
    {
        $data_conf = [
            '07' => [
                '01' => 8, '02' => 8, '03' => 8, '04' => 8, '05' => 8,
                '06' => 8, '07' => 8, '08' => 8, '09' => 8, '10' => 8,
                '11' => 8, '12' => 8, '13' => 8, '14' => 8, '15' => 8,
            ],

            '08' => [
                '01' => 4, '02' => 8, '03' => 7, '04' => 4, '05' => 5,
                '06' => 3, '07' => 4, '08' => 5, '09' => 4, '10' => 4,
                '11' => 6, '12' => 4, '13' => 3, '14' => 4, '15' => 3,
//                'v1' => 5,
            ],

            '09' => [
                '01' => 5, '02' => 3, '03' => 3, '04' => 4, '05' => 3,
                '06' => 4, '07' => 4, '08' => 4, '09' => 3, '10' => 4,
                '11' => 4, '12' => 4, '13' => 4, '14' => 4, '15' => 4,
//                'v1' => 5,
            ],

            '10' => [
                '01' => 4, '02' => 4, '03' => 4, '04' => 4, '05' => 4,
                '06' => 2, '07' => 4, '08' => 4, '09' => 4, '10' => 4,
                '11' => 4, '12' => 4, '13' => 4, '14' => 4, '15' => 3,
                '16' => 4, '17' => 3, '18' => 4,
            ],

            '11' => [
                '01' => 4, '02' => 4, '03' => 4, '04' => 4, '05' => 5,
                '06' => 3, '07' => 4, '08' => 4, '09' => 5, '10' => 4,
                '11' => 4, '12' => 3, '13' => 4, '14' => 3, '15' => 3,
                '16' => 4, '17' => 4, '18' => 3,
            ],

            '12' => [
                '01' => 4, '02' => 8, '03' => 4, '04' => 4, '05' => 4,
                '06' => 4, '07' => 4, '08' => 4, '09' => 4, '10' => 4,
                '11' => 4, '12' => 5, '13' => 5, '14' => 3, '15' => 4,
                '16' => 4, '17' => 4, '18' => 3,
            ],

            '13' => [
                '01' => 4, '02' => 4, '03' => 4, '04' => 5, '05' => 4,
                '06' => 4, '07' => 4, '08' => 5, '09' => 5, '10' => 4,
                '11' => 4, '12' => 3, '13' => 6, '14' => 4, '15' => 5,
                '16' => 4, '17' => 3, '18' => 5,
            ],

            '14' => [
                '01' => 4, '02' => 4, '03' => 4, '04' => 4, '05' => 5,
                '06' => 4, '07' => 4, '08' => 4, '09' => 5, '10' => 5,
                '11' => 4, '12' => 3, '13' => 4, '14' => 4, '15' => 4,
                '16' => 4, '17' => 4, '18' => 4,
            ],

            '15' => [
                '01' => 4, '02' => 6, '03' => 6, '04' => 4, '05' => 5,
                '06' => 5, '07' => 6, '08' => 4, '09' => 6, '10' => 3,
                '11' => 6, '12' => 4, '13' => 4, '14' => 4, '15' => 4,
                '16' => 8, '17' => 4, '18' => 6,
            ],
        ];

        $url_tmpl = "wget http://sso.spponline.org/slidesurvey/images/%s%s%s.jpg -O %s/yr20%s_q%s_img%s.jpg";

        if(!empty($year) && array_key_exists($year, $data_conf))
        {
            $yr_data = $data_conf[$year];
            $this->_processYearData($yr_data, $url_tmpl, $year);
        }
        else
        {
            foreach ($data_conf as $yr => $yr_data)
            {
                $this->_processYearData($yr_data, $url_tmpl, $yr);
            }
        }
    }

    private function _processYearData($yr_data, $url_tmpl, $yr)
    {
        foreach($yr_data as $qs => $no_of_qs){
            for($i=1; $i<=$no_of_qs; $i++){

                $q_cnt = str_pad($i, 2, '0', STR_PAD_LEFT);
                print_r(sprintf($url_tmpl, $yr, $qs, $q_cnt, $qs, $yr, $qs, $q_cnt) . "\n");

            }
        }
    }

    public function actionGetRbRuleStats($rb_rule_id)
    {
        /** @var TstTestRule $rb_rule */
        $rb_rule = TstTestRule::find()->where([TstTestRule::COL_ID => intval($rb_rule_id)])->one();

        /** @var LprTest $quiz */
        $quiz = LprTest::GetQuizFromRbRuleId($rb_rule_id);

        /** @var int|string|array $quiz_id */
        $quiz_id = LprTest::GetQuizIdFromRbRuleId($rb_rule_id);

       /** @var array $rb_rule_stats */
        $rb_rule_stats = $quiz->getQuickStatsNoOfQsForRbRule($rb_rule->toArray());

        /** @var array $matching_qs */
        $matching_qs = $quiz->selectQuestionsForRbRule($rb_rule_id);

        if (true) {
            print_r("\n" . VarDumper::dumpAsString([
                    'rb_rule_id' => intval($rb_rule_id),
                    'rb_rule' => $rb_rule->toArray(),
                    'quiz' => $quiz->toArray(),
                    'quiz_id' => $quiz_id,
                    'rb_rule_stats ' => $rb_rule_stats,
                    'matching_qs ' => $matching_qs,
                    'count(matching_qs)' => count($matching_qs),
                ]) . "\n");
            exit;
        }


    }



    public function actionTest()
    {
        UserGroupsRepository::SwitchParentUserForGroup(39, 2);
    }


    /**
     * ./yii hello/test-redis
     *
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionTestRedis($message = 'hello world')
    {
        Yii::$app->redis->set('mykey', 'some value');
        echo Yii::$app->redis->get('mykey') . "\n";

        Yii::$app->session->set('mykey3', 'some value3');
        echo Yii::$app->session->get('mykey3') . "\n";

        Yii::$app->cache->set('mykey2', 'some value2');
        echo Yii::$app->cache->get('mykey2') . "\n";

        echo $message . "\n";
    }




} // end class