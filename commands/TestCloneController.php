<?php

namespace app\commands;

use app\models\db\ext\LprQuestion;
use app\models\db\ext\LprTest;
use app\models\db\ext\RuleBook;
use app\models\db\ext\TstTestRule;
use DusanKasan\Knapsack\Collection;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\console\widgets\Table;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\VarDumper;


class TestCloneController extends Controller
{

    /**
     * ./yii test-clone/clone 11 58
     * @param integer $tid
     * @param integer $new_owner_id
     */
    public function actionClone($tid, $new_owner_id)
    {
        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid]);

        if (empty($lprTest)) {
            $message = sprintf("No Such Test: %s\n", $tid);
            $message = $this->ansiFormat($message, Console::FG_YELLOW);
            $this->stdout($message . "\n", Console::BOLD);
            return;
        }

        /** @var LprTest $lprTestClone */
        $lprTestClone = $lprTest->clone($new_owner_id);

        if (true) {
            $message = sprintf("\n%s\n", VarDumper::dumpAsString([
                'old_test' => $lprTest->toArray(),
                'new_test' => $lprTestClone->toArray(),
            ]));

            $message = $this->ansiFormat($message, Console::FG_GREEN);
            $this->stdout($message . "\n", Console::BOLD);
        }
    }


    /**
     * ./yii test-clone/delete 13
     * @param integer $tid
     */
    public function actionDelete($tid)
    {
        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid]);

        if (empty($lprTest)) {
            $message = sprintf("No Such Test: %s\n", $tid);
            $message = $this->ansiFormat($message, Console::FG_YELLOW);
            $this->stdout($message . "\n", Console::BOLD);
            return;
        }

        if (true) {
            $message = sprintf("\n%s\n", VarDumper::dumpAsString([
                'model' => $lprTest->toArray(),
            ]));
            $message = $this->ansiFormat($message, Console::FG_GREEN);
            $this->stdout($message . "\n", Console::BOLD);
        }

        try {
            $lprTest->delete();
        } catch (StaleObjectException $e) {
        } catch (\Exception $e) {
        } catch (\Throwable $e) {
        }
    }


    /**
     * ./yii test-clone/lock 16 true|false
     * @param integer $tid
     * @param bool $lock
     */
    public function actionLock($tid, $lock)
    {
        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid]);

        if (empty($lprTest)) {
            $message = sprintf("No Such Test: %s\n", $tid);
            $message = $this->ansiFormat($message, Console::FG_YELLOW);
            $this->stdout($message . "\n", Console::BOLD);
            return;
        }

        $lock = (trim($lock) === 'true') ? true: false;

        $lprTest->lockLprTest($lock);

        if (true) {
            $message = sprintf("\n%s\n", VarDumper::dumpAsString([
                'model' => $lprTest->toArray(),
                'isLocked' => $lprTest->isLocked(),
            ]));
            $message = $this->ansiFormat($message, Console::FG_GREEN);
            $this->stdout($message . "\n", Console::BOLD);
        }
    }

    /**
     * ./yii test-clone/list
     */
    public function actionList()
    {
        /** @var [] $lprTestsAQ */
        $lprTestsAQ = LprTest::find()->select(['id', 'title'])->asArray()->all();

        $res = [];

        Collection::from($lprTestsAQ)
            ->each(function ($i) use (&$res) {
                $res[] = [$i['id'], $i['title']];
            })->toArray();

        try {
            $message = Table::widget([
                'headers' => ['id', 'title'],
                'rows' => $res,
            ]);
        } catch (\Exception $e) {
        }

        $message = $this->ansiFormat($message, Console::FG_YELLOW, Console::BOLD);
        $this->stdout($message . "\n");
    }

    /**
     * ./yii test-clone/info 16
     * @param integer $tid
     */
    public function actionInfo($tid)
    {
        /** @var array $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid])->toArray();

        if (empty($lprTest)) {
            $message = sprintf("No Such Test: %s\n", $tid);
            $message = $this->ansiFormat($message, Console::FG_YELLOW, Console::BOLD);
            $this->stdout($message . "\n");
            return;
        }

        $res = [];

        Collection::from(array_keys( $lprTest))
            ->each(function ($i) use (&$res, &$lprTest) {
                $res[] = [$i, $lprTest[$i]];
            })->toArray();

        $message = Table::widget([
            'headers' => ['field', 'value'],
            'rows' => $res,
        ]);

        $message = $this->ansiFormat($message, Console::FG_YELLOW, Console::BOLD);
        $this->stdout($message . "\n");
    }


    /**
     * ./yii test-clone/questions 16
     * @param integer $tid
     */
    public function actionQuestions($tid)
    {
        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid]);

        if (empty($lprTest)) {
            $message = sprintf("No Such Test: %s\n", $tid);
            $message = $this->ansiFormat($message, Console::FG_YELLOW, Console::BOLD);
            $this->stdout($message . "\n");
            return;
        }

        $qsAQ = $lprTest->getQuestions();
        $qs = $qsAQ->asArray()->all();

        $res = [];

        Collection::from($qs)
            ->each(function ($i) use (&$res) {
                $res[] = [$i['id'], $i['title']];
            })->toArray();

        $message = Table::widget([
            'headers' => ['id', 'title'],
            'rows' => $res,
        ]);

        $this->_print_to_console( $message );

        $this->_print_to_console(
            sprintf("\n%s\n", VarDumper::dumpAsString([
                'model' => $lprTest->toArray(),
                'isLocked' => $lprTest->isLocked(),
//                'questions' => $qs,
            ])),
            true
        );
    }


    /**
     * ./yii test-clone/rb-clone 1 58
     * @param integer $rb_id
     * @param int $new_owner_id
     * @return int
     */
    public function actionRbClone($rb_id, $new_owner_id)
    {
        /** @var RuleBook $ruleBook */
        $ruleBook = RuleBook::findOne(['id' => $rb_id]);

        if (empty($ruleBook)) {
            $message = sprintf("No Such ruleBook: %s\n", $rb_id);
            $this->_print_to_console( $message);
            return ExitCode::DATAERR;
        }

        /** @var RuleBook $ruleBook_new */
        $ruleBook_new = $ruleBook->clone($new_owner_id);

        $this->_print_to_console(
            sprintf("\n%s\n", VarDumper::dumpAsString([
                'ruleBook' => $ruleBook->toArray(),
                'ruleBook_new' => $ruleBook_new->toArray(),
            ]))
        );

        return ExitCode::OK;
    }


    /**
     * ./yii test-clone/rb-delete 1
     *
     * @param int $rb_id
     * @return int
     */
    public function actionRbDelete($rb_id)
    {
        /** @var RuleBook $ruleBook */
        $ruleBook = RuleBook::findOne(['id' => $rb_id]);

        if (empty($ruleBook)) {
            $message = sprintf("No Such ruleBook: %s\n", $rb_id);
            $this->_print_to_console( $message);
            return ExitCode::DATAERR;
        }

        $ruleBook->delete();

        return ExitCode::OK;
    }


    /**
     * ./yii test-clone/clone-test-questions 5 29
     * @param int $tid
     * @param int $tid_new
     * @param int $owner_id_new
     * @return int
     */
    public function actionCloneTestQuestions($tid, $tid_new, $owner_id_new)
    {
        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid]);

        if (empty($lprTest)) {
            $message = sprintf("No Such lprTest: %s\n", $tid);
            $this->_print_to_console( $message);
            return ExitCode::DATAERR;
        }

        $lprTest->cloneQuestions($tid_new, $owner_id_new);

        if (!true) {
            $this->_print_to_console(
                sprintf("\n%s\n", VarDumper::dumpAsString([
                    'lprTest' => $lprTest->toArray(),
                ]))
            );
        }

        return ExitCode::OK;
    }



    private function _print_to_console($msg, $doPrint = true)
    {
        if($doPrint){
            $msg = $this->ansiFormat($msg, Console::FG_CYAN, Console::BOLD);
            $this->stdout($msg . "\n");
        }

    }


    /**
     * ./yii test-clone/delete-qs
     */
    public function actionDeleteQs()
    {
        /** @var ActiveQuery $qs */
        $qsAQ = LprQuestion::find()->where([LprQuestion::COL_OWNER_ID => 58]);

        /** @var LprQuestion $q */
        foreach($qsAQ->all() as $q){
            $q->delete();
        }

        if (true) {
            $message = sprintf("\n%s\n", VarDumper::dumpAsString([
                'qs' => $qsAQ->asArray()->all(),
            ]));
            $message = $this->ansiFormat($message, Console::FG_GREEN);
            $this->stdout($message . "\n", Console::BOLD);
        }

    }



} // end class