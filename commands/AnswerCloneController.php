<?php

namespace app\commands;

use app\models\db\ext\LprTestTemplate;
use app\models\db\ext\TstAnswer;
use app\models\db\ext\LprQuestion;
use app\models\db\ext\TstMediaAnswer;
use app\models\LprTest;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\VarDumper;


class AnswerCloneController extends Controller
{

    /**
     * @param int $a_id
     * @param int $q_id
     * @internal param string $message
     *
     * ./yii answer-clone/clone-answer 3388 11
     */
    public function actionCloneAnswer($a_id, $q_id)
    {
        /** @var TstAnswer $answer_source */
        $answer_source = TstAnswer::findOne(['id' => $a_id]);

        /** @var TstAnswer $answer_target */
        $answer_target = $answer_source->clone($q_id);

        if (!true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'answer_source' => $answer_source->toArray(),
                    'answer_target' => $answer_target->toArray(),
                ]) . "</pre>\n";
            exit;
        }

        $message = sprintf("New Answer Id: %s\n\n", $answer_target->id);
        $message = $this->ansiFormat($message, Console::FG_YELLOW);
        $this->stdout($message . "\n", Console::BOLD);
    }


    /**
     * @param int $a_id
     *
     * ./yii answer-clone/delete-answer 3388
     */
    public function actionDeleteAnswer($a_id){

        /** @var TstAnswer $answer */
        $answer = TstAnswer::findOne(['id' => $a_id]);

        if(!empty($answer)){
            $answer->delete();
        }
    }


    /**
     * @param int $q_id
     * @internal param string $message
     *
     * ./yii answer-clone/clone-question 11
     */
    public function actionCloneQuestion($q_id)
    {
        /** @var LprQuestion $question_source */
        $question_source = LprQuestion::findOne(['id' => $q_id]);

        /** @var LprQuestion $question_target */
        $question_target = $question_source->clone();

        if (true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'question_source' => $question_source->toArray(),
                    'question_target' => $question_target->toArray(),
                ]) . "</pre>\n";
        }

        $message = sprintf("New Question Id: %s\n\n", $question_target->id);
        $message = $this->ansiFormat($message, Console::FG_YELLOW);
        $this->stdout($message . "\n", Console::BOLD);
    }


    /**
     * @param int $q_id
     * @internal param string $message
     *
     * ./yii answer-clone/delete-question 684
     */
    public function actionDeleteQuestion($q_id)
    {
        /** @var LprQuestion $question */
        $question = LprQuestion::findOne(['id' => $q_id]);

        if (!empty($question)) {
            if (true) {
                print "\n<pre>" . VarDumper::dumpAsString([
                        'question_source' => $question->toArray(),
                    ]) . "</pre>\n";
            }

            $question->delete();

            $message = sprintf("Question To Delete: %s\n\n", $question->id);
        } else {
            $message = sprintf("Question Not Found: %s\n\n", $q_id);
        }

        $message = $this->ansiFormat($message, Console::FG_YELLOW);
        $this->stdout($message . "\n", Console::BOLD);
    }

    public function actionIndex(){

        $q_id = 11;     // question to clone

        /** @var LprQuestion $question_source */
        $question_source = LprQuestion::findOne(['id' => $q_id]);

        $selectedCtagIds = ArrayHelper::getColumn($question_source->getTstCtagQuestions()
            ->select(['id'])
            ->asArray()->all(), 'id');

        $selectedCtags = $question_source->getTstCtagQuestions()
            ->select(['id'])
            ->asArray()->all();

        $question_source->clearCtags($selectedCtagIds);


        if (true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'selectedCtags' => $selectedCtags,
                    'question_source' => $question_source->toArray(),
                    'selectedCtagIds' => $selectedCtagIds,
                ]) . "</pre>\n";
            exit;
        }


    }


    /**
     * @param int $qid
     *
     * ./yii answer-clone/unlink-media-items-from-question 691
     */
    public function actionUnlinkMediaItemsFromQuestion($qid)
    {
        /** @var LprQuestion $question */
        $question = LprQuestion::findOne(['id' => $qid]);

        if (!empty($question)) {
            $question->unlinkAll('mediaItems', true);
        }
    }

    /**
     * @param int $qid
     * @param int $tid
     * @param bool $unlink
     *
     * ./yii answer-clone/link-unlink-question-to-test 693 11
     * ./yii answer-clone/link-unlink-question-to-test 693 11 true
     */
    public function actionLinkUnlinkQuestionToTest($qid, $tid, $unlink=false)
    {
        /** @var LprQuestion $question */
        $question = LprQuestion::findOne(['id' => $qid]);

        /** @var LprTest $question */
        $test = LprTest::findOne(['id' => $tid]);

        if (!empty($question) && !empty($test)) {
            if (!$unlink) {
//            $test->link('questions', $question);
                $question->link('tests', $test);
            } else {

                if (true) {
//                    $test->unlinkAll('questions', true);
                    $question->unlinkAll('tests', true);
                }

                if (!true) {
//                    $test->unlink('questions', $question, true);
                    $question->unlink('tests', $test,true);
                }
            }
        }
    }


    /**
     * @param int $tid
     *
     * ./yii answer-clone/quick-run 691
     */
    public function actionQuickRun($tid)
    {
        /** @var LprTest $lprTest */
        $lprTest = LprTest::findOne(['id' => $tid]);

        if (empty($lprTest)) {
            $message = sprintf("No Such Test: %s\n", $tid);
            $message = $this->ansiFormat($message, Console::FG_YELLOW);
            $this->stdout($message . "\n", Console::BOLD);
            return; }


        $rb_rule_ids = [];
        $rb_rule_ids[] = 7;
        $rb_rule_ids[] = 8;

        /** @var \yii\db\ActiveQuery $ttqs */
        $ttqs = $lprTest->getTestQuestionsFromRuleBookRules($rb_rule_ids);

        $rule_qids = $lprTest->getAvailableQuestionsForRuleBook($rb_rule_ids);

        if (true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'ttqs' => $ttqs->asArray()->all(),
                    'ttqs_count' => $ttqs->count(),
                    'tid' => $tid,
                    'rule_qids' => $rule_qids,
//                    'isTestAlreadyDelivered' => $lprTest->isTestAlreadyDelivered(),
                ]) . "</pre>\n";
            exit;
        }


    }



} // end class