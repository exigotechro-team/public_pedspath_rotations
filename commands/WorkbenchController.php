<?php

namespace app\commands;

use app\models\db\ext\TraineeUser;
use app\models\db\ext\TrainerUser;
use app\models\db\ext\UserGroups;
use app\models\db\ext\UserGroupsRepository;
use app\models\db\UserProfile;
use app\models\form\TestQuestionPreviewForm;
use app\models\form\TraineeProfileEntryForm;
use app\models\rbac\roles\RbacRole;
use app\models\TstTestQuestion;
use app\models\TstTestUser;
use app\ro\exigotech\lpr\GmUtils;
use app\ro\exigotech\lpr\HashUtils;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use DusanKasan\Knapsack\Collection;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;


class WorkbenchController extends Controller
{
    /**
     * @example ./yii workbench/test-hash
     */

    public function actionTestHash()
    {
        $qp = new TestQuestionPreviewForm();
        $qp->t_id = 9;
        $qp->q_id = 123;
        $qp->setControlHash();

        echo $qp->display_timestamp . "\n";
        echo $qp->data_hash . "\n";
        if($qp->verifyControlHash()){
            echo "valid\n";
        }


        $qp->t_id = 10;
        if(!$qp->verifyControlHash()){
            echo "not valid\n";
        }
        echo $qp->data_hash . "\n";


    }

    /**
     * @example ./yii workbench/test-user-groups 48
     */

    public function actionTestUserGroups($tr_id=48){

        /** @var TrainerUser $trainer */
        $trainer = TrainerUser::find()->where(['id'=>$tr_id])->one();

        /** @var ActiveQuery|null $trainees_query */
        $trainees_query = null; // $trainer->getTrainees();

        /** @var TraineeUser $trainee */
        $trainee = $trainer->getTrainee(63);

        /** @var ActiveQuery $entries */
        $entries = $trainee->getUserProps();

        $entries_q = Collection::from($trainee->getUserProps()->asArray()->all())
            ->map(function ($value) {
                return TraineeProfileEntryForm::CreateFromUserProfile($value)->toArray();
            })->toArray();


        if (true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'trainer' => $trainer->toArray(),
//                    'trainees' => $trainees_query->asArray()->all(),
//                    'trainees_count' => $trainees_query->count(),
                    'trainee' => $trainee->toArray(),
                    'entries' =>  $entries->asArray()->all(),
                    'entries_q' => $entries_q,
                ]) . "</pre>\n";
            exit;
        }
    }


    /**
     *
     * @example ./yii workbench/load-csv-data
     *
     */
    public function actionLoadCsvData(){
        $data = GmUtils::LoadCsvData('residents_list.csv', 'name,te_pgy,te_inst');
//        $data = GmUtils::LoadCsvData('residents_list.csv', '');

        /** @var array $entry */
        foreach($data as $entry){
            $name = $entry['name'];

            /** @var Profile $user_profile */
            $user_profile = Profile::find()->where(['name' => $name])->one();

            if(!empty($user_profile)){
                echo sprintf("%s, %s\n", $name, $user_profile->user_id);

                /** @var UserProfile $user_prop */
                $user_prop = new UserProfile();
                $user_prop->owner_type = 'TRAINEE';
                $user_prop->owner_id = $user_profile->user_id;
                $user_prop->prop_name = 'te_pgy';
                $user_prop->prop_val = $entry['te_pgy'];
                $user_prop->save();

                /** @var UserProfile $user_prop2 */
                $user_prop2 = new UserProfile();
                $user_prop2->owner_type = 'TRAINEE';
                $user_prop2->owner_id = $user_profile->user_id;
                $user_prop2->prop_name = 'te_inst';
                $user_prop2->prop_val = $entry['te_inst'];
                $user_prop2->save();

                /** @var UserProfile $user_prop3 */
                $user_prop3 = new UserProfile();
                $user_prop3->owner_type = 'TRAINEE';
                $user_prop3->owner_id = $user_profile->user_id;
                $user_prop3->prop_name = 'te_type';
                $user_prop3->prop_val = 'RESIDENT';
                $user_prop3->save();

            }else{
                echo sprintf("=> %s, %s\n", $name, 'missing');
                continue;
            }
        }



        if (!true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'data' => $data,
                ]) . "</pre>\n";
            exit;
        }


    }


    /**
     *
     * @example ./yii workbench/load-csv-exam-data
     *
     */
    public function actionLoadCsvExamData(){
        $data = GmUtils::LoadCsvData('residents_list_exams.csv', 'name,pre_test,post_test');

        $test_ids = [
            'pre_test' => [
                0, 5, 8
            ],
            'post_test' => [
                0, 9, 10, 11
            ],
        ];


        $cntr = 0;

        /** @var array $entry */
        foreach($data as $entry){
            $name = $entry['name'];

            /** @var Profile $user_profile */
            $user_profile = Profile::find()->where(['name' => $name])->one();

            if(!empty($user_profile)){
                 // echo sprintf("%s, %s\n", $name, $user_profile->user_id);
                echo sprintf("%s: %s, %s:\n", $cntr, $name, $user_profile->user_id, $entry['pre_test']);


                $pre_test_key = trim($entry['pre_test']);
                $pre_test = $test_ids['pre_test'];

                $post_test_key = trim($entry['post_test']);
                $post_test = $test_ids['post_test'];

                if($pre_test_key != '-'){

                    $test_id = $pre_test[$pre_test_key];

                    echo sprintf("\tpre: %s -> %s\n", $pre_test_key, $test_id);

                    if (!true) {
                        $tstTestUser =  new TstTestUser($test_id, $user_profile->user_id);
                        $tstTestUser->save();
                    }

                }else{
                    echo sprintf("\tpre: %s missing\n", $pre_test_key);
                }

                if($post_test_key != '-'){

                    $test_id = $post_test[$post_test_key];

                    echo sprintf("\tpost: %s -> %s\n", $post_test_key, $test_id);

                    if (!true) {
                        $tstTestUser =  new TstTestUser($test_id, $user_profile->user_id);
                        $tstTestUser->save();
                    }

                }else{
                    echo sprintf("\tpost: %s missing\n", $post_test_key);
                }


            }else{
                echo sprintf("=> %s, %s\n", $name, 'missing');
                continue;
            }

            $cntr += 1;
        }

        if (!true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'data' => $data,
                ]) . "</pre>\n";
            exit;
        }


    }


    /**
     *
     * @example ./yii workbench/test-remaining-time
     * @param int $ttu_id
     */
    public function actionTestRemainingTime($ttu_id = 96){

        /** @var TstTestUser $ttu */
        $ttu = TstTestUser::find()->where(['id' => $ttu_id])->one();

        if (true) {
            print "\n<pre>" . VarDumper::dumpAsString([
                    'ttu' => $ttu->toArray(),
                    'getTestTimeRemaining' => $ttu->getTestTimeRemaining(),
                    'start_date' => $ttu->getStartDate(),
                    'end_date' => $ttu->getEndDate(),
                ]) . "</pre>\n";
            exit;
        }


    }

    /**
     *
     * @example ./yii workbench/test-get-next-question
     * @param int $ttu_id
     */
    public function actionTestGetNextQuestion($ttu_id = 117)
    {
        /** @var TstTestUser $ttu */
        $ttu = TstTestUser::find()->where(['id' => $ttu_id])->one();

        /** @var TstTestQuestion $ttq */
        $ttq = $ttu->getNextQuestion();

        /** @var bool $isLast */
        $isLast = $ttu->isLastQuestion($ttq->id);

        /** @var bool $totals */
        $totals = $ttu->getTotalsQuestionsVsAnswers();

        if (true) {
            print "\n<pre>" . VarDumper::dumpAsString([
//                    'ttuModel' => $ttu->toArray(),
                    'ttqModel' => $ttq->toArray(),
                    'isLast' => $isLast,
                    'totals' => $totals,
                ]) . "</pre>\n";
//            exit;
        }


    }

} // end class