<?php

namespace app\commands;

use app\models\pojos\UserTestProperties;
use app\models\TstTestUser;
use app\ro\exigotech\dal\SymSerializer;
use yii\console\Controller;
use yii\helpers\VarDumper;

class SymSerializeController extends Controller
{
    /**
     * ./yii sym-serialize/index
     */
    public function actionIndex()
    {
        echo "\nOk\n";

//        $this->testUserTestProps();
        $this->experiment2();

//        $this->experiment3();

    }

    private function testUserTestProps()
    {
        $sst = SymSerializer::GetInstance();

        $userTestProps = new UserTestProperties();
        $userTestProps->setCanJump(true);
        $userTestPropsJson = $sst->serialize($userTestProps);
        echo $userTestPropsJson . "\n";


        /** @var UserTestProperties $userTestProps2 */
        $userTestProps2 = $sst->deserialize($userTestPropsJson, UserTestProperties::class, 'json');
        $userTestProps2->setCanJump(true);
        $userTestProps2->setCanGoPrevious(true);
        $userTestProps2->setCanGoNext(true);
        $userTestProps2->setCanShowDebug(true);
        echo $userTestProps2->getCanGoPrevious() . "\n";
        echo $userTestProps2->getCanShowDebug() . "\n";

        $tmpJson = $sst->serialize($userTestProps2);
        echo $tmpJson . "\n";

        echo "/** ======================================== */\n";
    }

    private function experiment2()
    {
        $tstUsrProps = new TstTestUser();

        /** @var TstTestUser $tstUsrProps2 */
        $tstUsrProps2 = TstTestUser::find()->where(['id' => 117])->one();
        echo $tstUsrProps2->usr_tst_properties->getCanShowDebug() . "\n";
        $tstUsrProps2->usr_tst_properties->setCanShowDebug(true);
        $tstUsrProps2->usr_tst_properties->setCanChangeAnswer(true);
        $tstUsrProps2->save();
        echo $tstUsrProps2->usr_tst_properties->getCanShowDebug() . "\n";

        if (!true) {
            print "\n" . VarDumper::dumpAsString([
                    'tstUsrProps2' => $tstUsrProps2->toArray(),
                ]) . "\n";
            exit;
        }

    }

    private function experiment3()
    {
        /** @var TstTestUser[] $ttus */
        $ttus = TstTestUser::find()->all();

        /** @var TstTestUser $ttu */
        foreach ($ttus as $ttu){

            $ttu->save();

//            echo sprintf("UserId: %s, TTU_ID: %s", $ttu->user_id , $ttu->id ) . "\n";

        }


    }


} // end class