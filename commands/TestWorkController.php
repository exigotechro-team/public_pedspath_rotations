<?php

namespace app\commands;

use app\models\db\ext\LprQuestion;
use app\models\db\ext\LprTest;
use app\models\db\ext\RuleBook;
use app\models\db\ext\TstTestRule;
use app\models\db\search\TstTestUserSearch;
use app\models\TstTestUser;
use app\ro\exigotech\lpr\GmUtils;
use DusanKasan\Knapsack\Collection;
use Faker\Provider\DateTime;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\console\widgets\Table;
use yii\db\ActiveQuery;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\VarDumper;


class TestWorkController extends Controller
{

    /**
     *
     * @example ./yii test-work/taking-exam 56 12
     *
     * @param int $trainee_id
     * @param int $utid
     */
    public function actionTakingExam($trainee_id = 56, $utid = 12)
    {
        $tuSearch = new TstTestUserSearch();
        /** @var TstTestUser $trainee_test */
        $trainee_test = $tuSearch->getAssignedTestsToTraineeQuery($trainee_id, $utid)->one();

        $lpr_test = LprTest::findOne(['id' => $trainee_test->test_id]);

        $curr_time = time();
        $start_time = $curr_time-1;
        $end_time = $curr_time + intval( $lpr_test->duration_sec );

        $tz = new \DateTimeZone('UTC');
        $curr_Date = new \DateTime(gmdate("Y-m-d\TH:i:s\Z", $curr_time), $tz);
        $start_Date = new \DateTime(gmdate("Y-m-d\TH:i:s\Z", $start_time), $tz);
        $end_Date = new \DateTime(gmdate("Y-m-d\TH:i:s\Z", $end_time), $tz);


        $start2_Date = new \DateTime($trainee_test->started_on, $tz);
        $end2_Date = new \DateTime($trainee_test->ended_on, $tz);

        $answ = $trainee_test->getExamCanBeginStatus(true);

        if (true) {
            print "\n" . VarDumper::dumpAsString([
//                    'model' => $trainee_test->toArray(),
//                    'lpr_test' => $lpr_test->toArray(),
                    'isAllowedToTakeExam' => $answ,
                    'curr_time' => $curr_time,
                    'curr_date' => $curr_Date->format('Y-m-d H:i:s'),
                    'start_time' => $start_time,
                    'start_date' => $start_Date->format('Y-m-d H:i:s'),
                    'end_time' => $end_time,
                    'end_date' => $end_Date->format('Y-m-d H:i:s'),
                    'start2_Date' => $start2_Date->format('Y-m-d H:i:s'),
                    'end2_Date' => $end2_Date->format('Y-m-d H:i:s'),
                ]) . "\n";
        }
    }


    /**
     *
     * @example ./yii test-work/reset-user-test 56 12
     *
     * @param int $trainee_id
     * @param int $utid
     */
    public function actionResetUserTest($trainee_id = 56, $utid = 12)
    {
        $tuSearch = new TstTestUserSearch();
        /** @var TstTestUser $trainee_test */
        $trainee_test = $tuSearch->getAssignedTestsToTraineeQuery($trainee_id, $utid)->one();

        $trainee_test->started_on = '0000-00-00 00:00:00';
        $trainee_test->ended_on = '0000-00-00 00:00:00';
        $trainee_test->save();

    }


    /**
     *
     * @example ./yii test-work/format-seconds 7200 1
     *
     * @param int $seconds
     * @param int $pieces
     */
    public function actionFormatSeconds($seconds = 7200, $pieces = 0)
    {
        echo VarDumper::dumpAsString(GmUtils::formatTimeRange($seconds, $pieces) ). "\n";
    }

} // end class