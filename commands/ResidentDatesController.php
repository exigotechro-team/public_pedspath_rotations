<?php

namespace app\commands;

use app\models\db\ext\TraineeUser;
use app\models\TstUserAnswer;
use DateTime;
use DateTimeZone;
use DusanKasan\Knapsack\Collection;
use yii\console\Controller;
use yii\db\Exception;
use yii\helpers\VarDumper;

/**
 *
 * @property array $residents
 */
class ResidentDatesController extends Controller
{

    public $tz_utc;
    public $tz_chicago;



    /**
     * @example  ./yii resident-dates/index
     */
    public function actionIndex()
    {
        echo "Ok\n";

        $residents = $this->getResidents();

        $user_ids = Collection::from($residents)
            ->map(function ($value) {

                echo "Retrieving user: " . $value['user_id'] . "\n";

                /** @var TraineeUser $u */
                $u = TraineeUser::find()->where(['id' => $value['user_id']])->one();

                if(/*false && */$u->id == 99){
                    echo "Found user: " . $u->id . "\n\n";
                    $ttu_ids = $this->getTstTestUserIds($u->id);

                    $ttu_ids[0]['resid_date'] = $value['t1_date'];
                    $this->_processAnswerDates($ttu_ids[0]);

                    $ttu_ids[1]['resid_date'] = $value['t2_date'];
                    $this->_processAnswerDates($ttu_ids[1]);
                }

                return $u;
            })->toArray();

        if (!true) {
            print "\n" . VarDumper::dumpAsString([
                    'user_ids' => $user_ids,
                ]) . "\n";
            exit;
        }

    }

    /**
     * @param int $ttu_id
     * @return TstUserAnswer[]
     */
    private function correctAnswerDates($ttu_id)
    {
        /** @var TstUserAnswer[] $tuas */
        $tuas = TstUserAnswer::find()
            ->where(['ttu_id' => $ttu_id])
            ->orderBy(['answer_time' => SORT_ASC])
            /*->asArray()*/
            ->all();

        return $tuas;
    }

    /**
     * @param int $user_id
     * @return array
     */
    private function getTstTestUserIds($user_id){

        $sql = <<<SQL_STR
            select
              ttu.user_id,
              tua.ttu_id,
              count(tua.id) as 'no_of_answers'
            from tst_test_user ttu
              join tst_user_answer tua on ttu.id = tua.ttu_id
            where ttu.user_id = :user_id
            group by tua.ttu_id
            order by count(tua.id) asc;
SQL_STR;

        $res = [];

        try {
            $res = \Yii::$app->db->createCommand($sql)
                ->bindValue(':user_id', $user_id)
                ->queryAll();
        } catch (Exception $e) {
            echo sprintf("<pre>%s</pre>", $e->getMessage());
            echo sprintf("<pre>%s</pre>", $e->getTraceAsString());
        }

        return $res;
    }

    private function getResidents(){

        return [
            ['user_id' => 72, 'name' => 'Andres Madrigal', 't1_date' => '2015-11-16', 't2_date' => '2016-01-08'],
            ['user_id' => 73, 'name' => 'Derek Baumgarten', 't1_date' => '2016-02-01', 't2_date' => '2016-02-28'],
            ['user_id' => 74, 'name' => 'Michael Eckhardt', 't1_date' => '2016-03-01', 't2_date' => '2016-03-31'],
            ['user_id' => 75, 'name' => 'Audrey Deeken', 't1_date' => '2016-03-07', 't2_date' => '2016-04-30'],
            ['user_id' => 76, 'name' => 'Zach Michalicek', 't1_date' => '2016-04-01', 't2_date' => '2016-04-30'],
            ['user_id' => 77, 'name' => 'John Biemer', 't1_date' => '2016-05-01', 't2_date' => '2016-05-31'],
            ['user_id' => 78, 'name' => 'Tim Tan', 't1_date' => '2016-05-02', 't2_date' => '2016-06-24'],
            ['user_id' => 79, 'name' => 'Brannan Griffin', 't1_date' => '2016-06-27', 't2_date' => '2016-08-19'],
            ['user_id' => 80, 'name' => 'Nicolas Lopez', 't1_date' => '2016-08-01', 't2_date' => '2016-08-31'],
            ['user_id' => 81, 'name' => 'Melanie Hakar', 't1_date' => '2016-08-22', 't2_date' => '2016-10-14'],
            ['user_id' => 82, 'name' => 'Myra Khan', 't1_date' => '2016-10-01', 't2_date' => '2016-10-31'],
            ['user_id' => 83, 'name' => 'Ryan Jones', 't1_date' => '2016-10-17', 't2_date' => '2016-12-09'],
            ['user_id' => 84, 'name' => 'Garrison Pease', 't1_date' => '2016-12-01', 't2_date' => '2016-12-31'],
            ['user_id' => 86, 'name' => 'Reza Eshraghi', 't1_date' => '2017-01-01', 't2_date' => '2017-01-31'],
            ['user_id' => 87, 'name' => 'Brandon Umphress', 't1_date' => '2017-02-07', 't2_date' => '2017-03-15'],
            ['user_id' => 88, 'name' => 'Monica Aldulescu', 't1_date' => '2017-03-01', 't2_date' => '2017-03-31'],
            ['user_id' => 89, 'name' => 'Xiao Huang', 't1_date' => '2017-04-01', 't2_date' => '2017-04-30'],
            ['user_id' => 90, 'name' => 'Sharda Thakral', 't1_date' => '2017-05-01', 't2_date' => '2017-05-31'],
            ['user_id' => 91, 'name' => 'Nina Rahimi', 't1_date' => '2017-05-01', 't2_date' => '2017-06-23'],
            ['user_id' => 92, 'name' => 'Bogdan Isaila', 't1_date' => '2017-06-05', 't2_date' => '2017-06-30'],
            ['user_id' => 93, 'name' => 'David Waters', 't1_date' => '2017-07-01', 't2_date' => '2017-08-15'],
            ['user_id' => 94, 'name' => 'Meredith Reynolds', 't1_date' => '2017-08-01', 't2_date' => '2017-08-31'],
            ['user_id' => 95, 'name' => 'Claire Sorensen', 't1_date' => '2017-08-31', 't2_date' => '2017-10-07'],
            ['user_id' => 96, 'name' => 'Farres Obeidin', 't1_date' => '2017-11-13', 't2_date' => '2018-01-05'],
            ['user_id' => 97, 'name' => 'Aadil Ahmed', 't1_date' => '2017-12-01', 't2_date' => '2017-12-31'],
            ['user_id' => 98, 'name' => 'Saman Ahmadian', 't1_date' => '2018-01-01', 't2_date' => '2018-01-31'],
            ['user_id' => 99, 'name' => 'Indu Aragwal', 't1_date' => '2018-02-01', 't2_date' => '2018-02-28'],
            ['user_id' => 101, 'name' => 'Neelima Valluru', 't1_date' => '2017-09-05', 't2_date' => '2017-09-29'],
        ];
    }

    public function beforeAction($action)
    {
        $this->tz_utc       = new \DateTimeZone('UTC');
        $this->tz_chicago   = new \DateTimeZone('America/Chicago');

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    /**
     * @param array $ttu_data
     */
    private function _processAnswerDates($ttu_data)
    {
        $ttu_id = $ttu_data['ttu_id'];

        /** @var TstUserAnswer[] $tuas */
        $tuas = $this->correctAnswerDates($ttu_id);


        /** @var TstUserAnswer $tua */
        foreach ($tuas as $tua) {

            if (true || $tua['answer_id'] == 39) {

                /** @var DateTime $atime */
                $atime = $this->_spliceDateTimesForAnswer($tua->answer_time, $ttu_data['resid_date']);

                $tua->answer_time = $atime->format('Y-m-d H:i:s');
                $tua->save();

                if (!true) {
                    print "\n" . VarDumper::dumpAsString([

                            'ttu_data' => $ttu_data,
                            'tua' => $tua->toArray(),
                            'atime' => $atime->format('Y-m-d H:i:s'),
                            'atime_tstmp' => $atime->getTimestamp(),
                        ]) . "\n";

                }
            }
        }
    }

    /**
     * @param string $answer_time_txt
     * @param string $resid_date_txt
     * @return DateTime
     */
    private function _spliceDateTimesForAnswer($answer_time_txt, $resid_date_txt)
    {
        $atime_resid_chicago = new \DateTime($resid_date_txt, $this->tz_chicago);
        $atime_p1 = $atime_resid_chicago->format('Y-m-d');

        $atime_mine_chicago = (new \DateTime($answer_time_txt, $this->tz_utc))->setTimezone($this->tz_chicago);
        $atime_p2 = $atime_mine_chicago->format('H:i:s');

        $atime_txt = join(" ", [$atime_p1, $atime_p2]);

        $atime = new DateTime($atime_txt, $this->tz_chicago);

        return $atime;
    }


} // end class