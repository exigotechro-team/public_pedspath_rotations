<?php

namespace app\ro\exigotech\dal;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SymSerializer
{
    private $encoders;
    private $normalizers;
    private $serializer;

    private static $_sym_serializer;

    /**
     * SymSerTest constructor.
     */
    public function __construct()
    {
        $this->encoders = array(new JsonEncoder());
        $this->normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
    }

    public static function GetInstance()
    {
        if(empty(self::$_sym_serializer)){
            self::$_sym_serializer = new SymSerializer();
        }

        return self::$_sym_serializer;
    }


    public function serialize($user_test_properties)
    {
        $jsonContent = $this->serializer->serialize(
            $user_test_properties,
            'json',
            [
                'json_encode_options' => JSON_PRETTY_PRINT,
            ]);

        return $jsonContent;
    }

    public function deserialize($inJsonStr, $class, $string)
    {
        $exam_grades = $this->serializer->deserialize($inJsonStr, $class, $string);
        return $exam_grades;
    }
}