<?php

namespace app\ro\exigotech\lpr;


use yii\helpers\FileHelper;
use yii\helpers\VarDumper;

class GmUtils
{
    // const CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const CHARS = '23456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';

    public static function generateRandomString($length = 5)
    {
        $charsLength = strlen(GmUtils::CHARS);
        $randomString = '';

        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= substr(GmUtils::CHARS, rand(0, $charsLength - 1), 1);
        }
        return $randomString;
    }


    /**
     * @param int $seconds
     * @param int $return_pieces
     * @return string|array
     */
    public static function formatTimeRange($seconds = 0, $return_pieces = 0)
    {
        $hours = floor($seconds / 3600);
        $mins = floor($seconds / 60 % 60);
        $secs = floor($seconds % 60);

        $pieces = [
            'h' => $hours,
            'm' => $mins,
            's' => $secs,
        ];

        $timeFormat = null;

        if($mins == 0 && $secs == 0){
            $timeFormat = sprintf(($hours == 1) ? '%2d hour' : '%2d hours', $hours);

        }elseif ($secs == 0){
            $timeFormat = sprintf('%2d h : %02d m', $hours, $mins);
        }else{
            $timeFormat = sprintf('%2d:%02d:%02d', $hours, $mins, $secs);
        }

        return (intval($return_pieces) == 1) ? $pieces : $timeFormat;
    }

    /**
     * @return string
     */
    public static function getTimestamp()
    {
        $tz = new \DateTimeZone('UTC');
        $end_Date = new \DateTime(gmdate("Y-m-d\TH:i:s\Z", time()), $tz);
        return $end_Date->format('Y-m-d H:i:s');
    }


    /**
     * @param string $csvFileName
     * @param string $row_fields_str
     * @return array
     */
    public static function LoadCsvData($csvFileName, $row_fields_str = ""){

        $fullDocsDir = FileHelper::normalizePath(\Yii::getAlias('@app') . '');
        $fileUri = join('/', [$fullDocsDir , $csvFileName]);

        $row_fields = [];

        if(!empty($row_fields_str)){
            $row_fields = preg_split("/,/", $row_fields_str);
        }

        $row = 1;

        $residents_raw = [];

        if (($handle = fopen($fileUri, "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $num = count($data);
                $row++;
                $up_raw = [];

                for ($c = 0; $c < $num; $c++) {
                    if(!empty($row_fields)){
                        $up_raw[$row_fields[$c]] = $data[$c];
                    }else{
                        $up_raw[] = $data[$c];
                    }
                }

                $residents_raw[] = $up_raw;
            }
            fclose($handle);
        }

        return $residents_raw;
    }


    public static function GetCurrentDatetime()
    {
        $tz = new \DateTimeZone('UTC');
        $curr_Date = new \DateTime(gmdate("Y-m-d\TH:i:s\Z", time()), $tz);
        return $curr_Date->format('Y-m-d H:i:s');
    }


}