<?php

namespace app\ro\exigotech\lpr;


use yii\helpers\VarDumper;

class HashUtils
{

    const LprSecret = '...';

    /**
     * @param array $inputs
     * @return string
     */
    public static function GetHash($inputs)
    {
        if(is_array( $inputs)){
            $inputs[] = self::LprSecret;
        }

        $inStr = join("/", $inputs);
        $strHash = md5(sprintf("%s/%s", md5($inStr) , self::LprSecret)) ;

        return $strHash;
    }

    /**
     * @param array|string $inputs
     * @param string $control_hash
     * @return bool
     */
    public static function VerifyHash($inputs, $control_hash)
    {
        $hash_inputs = null;

        if(is_array($inputs)){
            $hash_inputs = self::GetHash($inputs);
        }else{
            $hash_inputs = $inputs;
        }

        return ($hash_inputs === $control_hash) ? true : false;
    }



}